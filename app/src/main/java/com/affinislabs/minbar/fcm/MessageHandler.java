package com.affinislabs.minbar.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.affinislabs.minbar.R;

import java.util.Map;
import java.util.Random;

/**
 * minbar-v2
 * Michael Obi
 * 16/04/2018, 10:03 AM
 */
abstract class MessageHandler {

    private static final int notificationRequestCode = 7583;
    private static final String NOTIFICATION_CHANNEL_ID = "MINBAR_ID";
    private final Context context;
    private NotificationManager notificationManager;


    MessageHandler(Context context) {
        this.context = context;
        notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        createNotificationChannels();
    }

    public abstract void execute(Map<String, String> notificationData);

    void createNotification(String title, String message, Intent intent) {

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher)
                .setAutoCancel(true)
                .setSound(defaultSoundUri);

        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.mipmap
                .ic_launcher);
        notificationBuilder.setLargeIcon(largeIcon);
        notificationBuilder.setContentTitle(title);
        notificationBuilder.setContentText(message);
        notificationBuilder.setWhen(0);
        notificationBuilder.setTicker(title);

        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent notificationPendingIntent = PendingIntent.getActivity(context,
                notificationRequestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notificationBuilder.setContentIntent(notificationPendingIntent);
        Random rnd = new Random();
        int notificationId = 100000 + rnd.nextInt(900000);
        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    protected Context getContext() {
        return context;
    }


    private void createNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                    context.getString(R.string.minbar_notifications),
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setDescription("Notification category for private messages");

            notificationManager.createNotificationChannel(notificationChannel);
        }
    }
}
