package com.affinislabs.minbar.fcm;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;

import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.MinbarDataRepository;
import com.affinislabs.minbar.ui.answers.AnswersActivity;
import com.affinislabs.minbar.utils.Constants;

import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * minbar-v2
 * Michael Obi
 * 16/04/2018, 10:07 AM
 */
final class NewQuestionHandler extends MessageHandler {
    private static final String TAG = "NewQuestionHandler";
    private final MinbarDataRepository repository;

    public NewQuestionHandler(Context context, MinbarDataRepository repository) {
        super(context);
        this.repository = repository;
    }

    @SuppressLint("CheckResult")
    @Override
    public void execute(Map<String, String> data) {
        String questionId = data.get("question-id");
        if (questionId == null) {
            return;
        }

        repository.getQuestionsUserAnswer()
                .flatMapObservable(Observable::fromIterable)
                .filter(question -> question.getId().equals(questionId))
                .take(1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(question -> {
                    Intent intent = new Intent(getContext(), AnswersActivity.class);
                    intent.putExtra(Constants.QUESTION, question);
                    createNotification(getContext().getString(R.string.new_question),
                            data.get("notification-message"), intent);
                });

    }
}
