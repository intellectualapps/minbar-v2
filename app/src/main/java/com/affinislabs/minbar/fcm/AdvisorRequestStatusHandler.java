package com.affinislabs.minbar.fcm;

import android.content.Context;
import android.content.Intent;

import com.affinislabs.minbar.R;
import com.affinislabs.minbar.ui.main.MainActivity;
import com.affinislabs.minbar.utils.Constants;

import java.util.Map;

/**
 * minbar
 * Michael Obi
 * 04/05/2018, 12:32 PM
 */
final class AdvisorRequestStatusHandler extends MessageHandler {
    AdvisorRequestStatusHandler(Context context) {
        super(context);
    }

    @Override
    public void execute(Map<String, String> data) {
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.setAction(Constants.NEW_ADVISOR_ACTION);
        createNotification(getContext().getString(R.string.advisor_request_response), data.get("notification-message"), intent);
    }
}
