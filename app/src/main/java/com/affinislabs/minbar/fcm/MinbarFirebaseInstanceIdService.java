package com.affinislabs.minbar.fcm;

import android.annotation.SuppressLint;
import android.util.Log;

import com.affinislabs.minbar.MinbarApplication;
import com.affinislabs.minbar.data.MinbarDataRepository;
import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.domain.auth.AuthManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import javax.inject.Inject;

import io.reactivex.schedulers.Schedulers;

/**
 * minbar-v2
 * Michael Obi
 * 09/04/2018, 4:44 PM
 */
public class MinbarFirebaseInstanceIdService extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseInstanceService";

    @Inject
    MinbarDataRepository repository;

    @Inject
    AuthManager authManager;

    @Override
    public void onCreate() {
        super.onCreate();
        ((MinbarApplication) getApplication())
                .getAppComponent()
                .getFcmComponent()
                .inject(this);
    }

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    @SuppressLint("CheckResult")
    public void sendRegistrationToServer(String fcmToken) {
        User user = authManager.getUser();
        if (user == null) {
            Log.d(TAG, "sendRegistrationToServer: No user logged In");
            return;
        }
        if (fcmToken != null) {
            repository.updateFcmToken(user.getUsername(), fcmToken, MinbarApplication.getDeviceUuid())
                    .subscribeOn(Schedulers.io())
                    .subscribe(() -> Log.d(TAG, "Firebase Token updated"));
        }

    }
}
