package com.affinislabs.minbar.fcm;

import com.affinislabs.minbar.MinbarApplication;
import com.affinislabs.minbar.data.MinbarDataRepository;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

/**
 * minbar-v2
 * Michael Obi
 * 09/04/2018, 4:42 PM
 */
public class MinbarFirebaseMessagingService extends FirebaseMessagingService {
    Map<String, MessageHandler> messageHandlers = new HashMap<String, MessageHandler>();

    @Inject
    MinbarDataRepository repository;

    @Override
    public void onCreate() {
        super.onCreate();
        ((MinbarApplication) getApplication())
                .getAppComponent()
                .getFcmComponent()
                .inject(this);
        messageHandlers.put("new-question", new NewQuestionHandler(this, repository));
        messageHandlers.put("new-advisor", new NewAdvisorHandler(this));
        messageHandlers.put("advisor-request-status-update", new AdvisorRequestStatusHandler(this));
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String messageType = remoteMessage.getData().get("notification-type");
        MessageHandler messageHandler = messageHandlers.get(messageType);
        if (messageHandler != null) {
            messageHandler.execute(remoteMessage.getData());
        }
    }
}
