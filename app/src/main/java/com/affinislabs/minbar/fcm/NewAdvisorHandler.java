package com.affinislabs.minbar.fcm;

import android.content.Context;
import android.content.Intent;

import com.affinislabs.minbar.R;
import com.affinislabs.minbar.ui.main.MainActivity;
import com.affinislabs.minbar.utils.Constants;

import java.util.Map;

/**
 * minbar
 * Michael Obi
 * 21/04/2018, 10:51 PM
 */
final class NewAdvisorHandler extends MessageHandler {
    NewAdvisorHandler(Context context) {
        super(context);
    }

    @Override
    public void execute(Map<String, String> data) {
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.setAction(Constants.NEW_ADVISOR_ACTION);
        createNotification(getContext().getString(R.string.new_advisor_request),
                data.get("notification-message"), intent);
    }
}
