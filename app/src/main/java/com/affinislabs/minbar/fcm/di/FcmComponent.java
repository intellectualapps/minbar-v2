package com.affinislabs.minbar.fcm.di;

import com.affinislabs.minbar.fcm.MinbarFirebaseInstanceIdService;
import com.affinislabs.minbar.fcm.MinbarFirebaseMessagingService;

import dagger.Subcomponent;

/**
 * minbar-v2
 * Michael Obi
 * 09/04/2018, 9:17 PM
 */

@Subcomponent
public interface FcmComponent {
    void inject(MinbarFirebaseInstanceIdService instanceIdService);
    void inject(MinbarFirebaseMessagingService firebaseMessagingService);
}
