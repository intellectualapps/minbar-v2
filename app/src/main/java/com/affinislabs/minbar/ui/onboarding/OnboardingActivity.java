package com.affinislabs.minbar.ui.onboarding;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.models.OnBoardingContent;
import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.slide.FragmentSlide;

import java.util.ArrayList;

/**
 * minbar-v2
 * Michael Obi
 * 26/01/2018, 8:10 PM
 */

public class OnboardingActivity extends IntroActivity {

    public static final String PREF_HAS_COMPLETED_ONBOARDING = "HAS_COMPLETED_ONBOARDING";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setFullscreen(false);

        for (OnBoardingContent onBoardingContent : getOnBoardingContents()) {
            Fragment onBoardingChildFragment = OnBoardingChildFragment.newInstance(onBoardingContent);
            addSlide(new FragmentSlide.Builder()
                    .background(R.color.colorWhite)
                    .backgroundDark(R.color.colorPrimary)
                    .fragment(onBoardingChildFragment)
                    .build());
        }

        setButtonBackVisible(true);
        setButtonBackFunction(BUTTON_BACK_FUNCTION_BACK);
        setButtonNextVisible(true);
        setButtonNextFunction(BUTTON_NEXT_FUNCTION_NEXT_FINISH);
        setButtonCtaVisible(false);

        setPageScrollDuration(500);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            setImmersive(true);
        }
    }

    @NonNull
    private ArrayList<OnBoardingContent> getOnBoardingContents() {
        ArrayList<OnBoardingContent> onBoardingContentArrayList = new ArrayList<>();

        onBoardingContentArrayList.add(new OnBoardingContent(getString(R.string
                .onboarding_title_one), getString(R.string.onboarding_content_one), R.drawable
                .ic_onboarding1));
        onBoardingContentArrayList.add(new OnBoardingContent(getString(R.string
                .onboarding_title_two), getString(R.string.onboarding_content_two), R.drawable.ic_onboarding2));
        onBoardingContentArrayList.add(new OnBoardingContent(getString(R.string
                .onboarding_title_three), getString(R.string.onboarding_content_three), R.drawable.ic_onboarding3));
        onBoardingContentArrayList.add(new OnBoardingContent(getString(R.string
                .onboarding_title_four), getString(R.string.onboarding_content_four), R.drawable.ic_onboarding4));
        return onBoardingContentArrayList;
    }
}
