package com.affinislabs.minbar.ui.questions;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.models.Question;
import com.affinislabs.minbar.databinding.QuestionsFragmentBinding;
import com.affinislabs.minbar.ui.answers.AnswersActivity;
import com.affinislabs.minbar.ui.common.ViewPagerAdapter;
import com.affinislabs.minbar.ui.questions.myquestions.MyQuestionsFragment;
import com.affinislabs.minbar.ui.questions.questionstoanswer.QuestionsToAnswerFragment;
import com.affinislabs.minbar.utils.Constants;
import com.affinislabs.minbar.utils.FontUtils;

/**
 * Minbar
 * Michael Obi
 * 05 January 2018, 10:58 PM
 */

public class QuestionsFragment extends Fragment {

    private QuestionsFragmentBinding binding;

    public QuestionsFragment() {
    }

    public static QuestionsFragment newInstance() {
        return new QuestionsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_questions, container, false);
        if (getActivity() != null) {
            getActivity().setTitle(getString(R.string.questions_title));
        }
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViewPager();
    }

    private void setupViewPager() {
        ViewPagerAdapter questionPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
        questionPagerAdapter.addFragment(MyQuestionsFragment.newInstance(), getString(R.string.you_asked_label));
        questionPagerAdapter.addFragment(QuestionsToAnswerFragment.newInstance(), getString(R.string.you_answer_label));
        binding.viewPager.setAdapter(questionPagerAdapter);
        binding.viewPager.setCurrentItem(0);

        binding.tabs.setTabGravity(TabLayout.GRAVITY_CENTER);
        binding.tabs.setTabMode(TabLayout.MODE_FIXED);
        binding.tabs.setupWithViewPager(binding.viewPager);

        ViewGroup vg = (ViewGroup) binding.tabs.getChildAt(0);
        Typeface typeface = FontUtils.selectTypeface(getContext(), FontUtils.STYLE_BOLD);

        for (int i = 0; i < binding.tabs.getTabCount(); i++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(i);

            int tabChildCount = vgTab.getChildCount();

            for (int j = 0; j < tabChildCount; j++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(typeface);
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        for (Fragment fragment : getChildFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}