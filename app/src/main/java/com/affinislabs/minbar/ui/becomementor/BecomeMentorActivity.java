package com.affinislabs.minbar.ui.becomementor;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.affinislabs.minbar.MinbarApplication;
import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.models.Category;
import com.affinislabs.minbar.databinding.BecomeMentorActivityBinding;
import com.affinislabs.minbar.di.SharedPref;
import com.affinislabs.minbar.ui.main.MainActivity;
import com.affinislabs.minbar.ui.common.GridSpacingItemDecoration;
import com.affinislabs.minbar.ui.onboarding.OnboardingActivity;
import com.affinislabs.minbar.utils.Constants;
import com.affinislabs.minbar.utils.ToastUtil;

import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import static com.affinislabs.minbar.ui.onboarding.OnboardingActivity.PREF_HAS_COMPLETED_ONBOARDING;

/**
 * minbar-v2
 * Michael Obi
 * 18 January 2018, 4:53 PM
 */

public class BecomeMentorActivity extends AppCompatActivity implements BecomeMentorView,
        MentorCategoriesAdapter.CategoryItemClickListener, CompoundButton.OnCheckedChangeListener {


    @Inject
    BecomeMentorPresenter presenter;
    @Inject
    @SharedPref(value = SharedPref.META)
    SharedPreferences sharedPreferences;

    private BecomeMentorActivityBinding binding;
    private MentorCategoriesAdapter adapter;
    private ProgressDialog progressDialog;

    private HashMap<String, Boolean> mentorCategorySelected = new HashMap<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MinbarApplication) getApplication())
                .getAppComponent()
                .becomeMentorComponent()
                .inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_become_mentor);
        binding.toolbarLayout.toolbarTitle.setText(getString(R.string.be_a_mentor));
        setSupportActionBar(binding.toolbarLayout.toolbar);

        presenter.attachView(this);
        int spanCount = 2;
        int spacing = 30;
        binding.recyclerviewMentorCategories.addItemDecoration(new GridSpacingItemDecoration(spanCount,
                spacing, false));
        binding.recyclerviewMentorCategories.setLayoutManager(new GridLayoutManager(this, spanCount));
        adapter = new MentorCategoriesAdapter(this);
        binding.recyclerviewMentorCategories.setAdapter(adapter);
        binding.beAMentorSwitch.setOnCheckedChangeListener(this);
    }

    @SuppressLint("ApplySharedPref")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_CODE_INTRO) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(PREF_HAS_COMPLETED_ONBOARDING, true);
            editor.commit();
            navigateToNextPage();
        }
    }

    @Override
    public void showCategoryLoadingProgress() {
        binding.beAMentorSwitch.setEnabled(false);
        binding.nextButton.setEnabled(false);
        binding.categoryLoadingProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideCategoryLoadingProgress() {
        binding.beAMentorSwitch.setEnabled(true);
        binding.nextButton.setEnabled(true);
        binding.categoryLoadingProgress.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this, R.style.AppCompatAlertDialogStyle);
        }

        progressDialog.setMessage(getString(R.string.saving_your_mentorship_categories));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog == null) return;
        progressDialog.dismiss();
    }

    @Override
    public void showError(String errorMessage) {
        binding.beAMentorSwitch.setChecked(false);
        ToastUtil.showToast(this, errorMessage, Toast.LENGTH_LONG);
    }

    @Override
    public void showCategories(List<Category> categories) {
        binding.recyclerviewMentorCategories.setVisibility(View.VISIBLE);
        mentorCategorySelected.clear();
        for (Category category : categories) {
            mentorCategorySelected.put(category.getId(), false);
        }
        adapter.setMentorCategoriesList(categories);
    }

    @Override
    public void navigateToNextPage() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(Constants.KEY_HAS_SEEN_BECOME_MENTOR_SCREEN, true);
        editor.apply();

        boolean hasCompletedOnboarding = sharedPreferences.getBoolean
                (PREF_HAS_COMPLETED_ONBOARDING, false);
        if (!hasCompletedOnboarding) {
            Intent i = new Intent(this, OnboardingActivity.class);
            startActivityForResult(i, Constants.REQUEST_CODE_INTRO);
        } else {
            Intent i = new Intent(this, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }
    }

    @Override
    public void showCategoryLoadingError() {
        Snackbar snackbar = Snackbar.make(binding.container,
                R.string.category_loading_error,
                Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.retry, v -> {
            snackbar.dismiss();
            presenter.loadMentorCategories();
        });
        snackbar.show();
    }

    @Override
    public void onCategoryItemClicked(String categoryId, boolean isSelected) {
        mentorCategorySelected.put(categoryId, isSelected);
    }

    public void onNextButtonClicked(View v) {
        if (binding.beAMentorSwitch.isChecked()) {
            if (!mentorCategorySelected.containsValue(true)) {
                ToastUtil.showToast(this, getString(R.string.please_select_a_category), Toast.LENGTH_SHORT);
            }

            presenter.saveMentorshipCategories(mentorCategorySelected);
        }
        navigateToNextPage();
    }


    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            presenter.loadMentorCategories();
            return;
        }

        binding.recyclerviewMentorCategories.setVisibility(View.GONE);
    }

    public void onMentorSwitchLabelClick(View view) {
        if (binding.beAMentorSwitch.isEnabled()) {
            binding.beAMentorSwitch.toggle();
        }
    }
}
