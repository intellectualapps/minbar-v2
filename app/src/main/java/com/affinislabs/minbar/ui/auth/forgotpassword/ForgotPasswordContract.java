package com.affinislabs.minbar.ui.auth.forgotpassword;

import com.affinislabs.minbar.ui.common.base.RemoteView;

/**
 * Created by felixunlimited on 1/30/18.
 */

public interface ForgotPasswordContract {
    interface View extends RemoteView {
        void navigateToLoginPage();

        void showPasswordResetFailMessage (String errorMessage);

        void showPasswordResetSuccessMessage(String successMessage);
    }

}
