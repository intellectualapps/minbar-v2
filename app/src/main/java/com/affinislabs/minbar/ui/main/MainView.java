package com.affinislabs.minbar.ui.main;

import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.ui.common.base.View;

/**
 * minbar
 * Michael Obi
 * 24/04/2018, 10:30 PM
 */
public interface MainView extends View {
    void navigateToLogin();

    void showProfileData(User user);

    void showOpenQuestionCount(Integer count);
}
