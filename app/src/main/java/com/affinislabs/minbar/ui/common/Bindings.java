package com.affinislabs.minbar.ui.common;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.minbar.utils.DateUtils;
import com.affinislabs.minbar.utils.ImageLoader;

import java.util.Date;

/**
 * minbar-v2
 * Michael Obi
 * 06/03/2018, 6:58 PM
 */

public class Bindings {
    @BindingAdapter({"date"})
    public static void bindDate(TextView textView, Date date) {
        if (date != null) {
            textView.setText(DateUtils.getRelativeDateText(date));
        }
    }


    @BindingAdapter({"imageUrl", "error"})
    public static void loadImage(ImageView imageView, String url, Drawable error) {
        ImageLoader.loadImage(imageView, url, error);
    }
}
