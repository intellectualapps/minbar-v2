package com.affinislabs.minbar.ui.threads.di;

import com.affinislabs.minbar.di.ScreenScoped;
import com.affinislabs.minbar.ui.threads.ThreadActivity;

import dagger.Subcomponent;

/**
 * minbar-v2
 * Michael Obi
 * 12/03/2018, 7:17 AM
 */

@ScreenScoped
@Subcomponent
public interface ThreadsComponent {
    void inject(ThreadActivity activity);
}
