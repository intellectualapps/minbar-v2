package com.affinislabs.minbar.ui.askquestion;


import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.affinislabs.minbar.MinbarApplication;
import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.models.Category;
import com.affinislabs.minbar.data.models.Question;
import com.affinislabs.minbar.databinding.AskQuestionFragmentBinding;
import com.affinislabs.minbar.ui.becomementor.MentorCategoriesAdapter;
import com.affinislabs.minbar.ui.common.GridSpacingItemDecoration;
import com.affinislabs.minbar.ui.common.base.BaseFragment;
import com.affinislabs.minbar.ui.selectmentors.SelectMentorsFragment;
import com.affinislabs.minbar.utils.ToastUtil;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.processors.FlowableProcessor;
import io.reactivex.processors.PublishProcessor;

/**
 * minbar-v2
 * Michael Obi
 * 25/01/2018, 12:16 AM
 */

public class AskQuestionFragment extends BaseFragment implements AskQuestionView, MentorCategoriesAdapter
        .CategoryItemClickListener, View.OnClickListener {
    Flowable<CharSequence> questionChangeObservable;
    FlowableProcessor<Set<String>> selectedCategoriesChangeObservable = PublishProcessor.create();
    @Inject
    AskQuestionPresenter presenter;
    private AskQuestionFragmentBinding binding;
    private MentorCategoriesAdapter adapter;
    private ProgressDialog progressDialog;
    private Set<String> selectedCategories = new HashSet<>();

    public AskQuestionFragment() {
    }

    public static AskQuestionFragment newInstance() {
        return new AskQuestionFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MinbarApplication) getActivity().getApplication())
                .getAppComponent()
                .askQuestionComponent()
                .inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        presenter.attachView(this);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ask_question, container, false);
        binding.recyclerviewMentorCategories.addItemDecoration(new GridSpacingItemDecoration(2, 30, false));
        binding.recyclerviewMentorCategories.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        binding.recyclerviewMentorCategories.setNestedScrollingEnabled(false);
        binding.nextButton.setOnClickListener(this);
        adapter = new MentorCategoriesAdapter(this);
        binding.recyclerviewMentorCategories.setAdapter(adapter);
        getActivity().setTitle(getString(R.string.ask_a_question_title));

        validateForm();

        return binding.getRoot();
    }


    private void validateForm() {
        questionChangeObservable = RxTextView.textChanges(binding.editTextQuestion)
                .toFlowable(BackpressureStrategy.LATEST);
        Flowable.combineLatest(questionChangeObservable, selectedCategoriesChangeObservable,
                (question, selectedCategories) -> {
                    boolean questionValid = question.length() > 0;
                    boolean categoriesValid = selectedCategories.size() > 0;
                    return questionValid && categoriesValid;
                })
                .subscribe(formValid -> binding.nextButton.setEnabled(formValid));
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.loadMentorCategories();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
    }

    @Override
    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
        }

        progressDialog.setMessage(getString(R.string.ask_question_progress_label));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog == null) return;
        progressDialog.dismiss();
    }

    @Override
    public void showError(String errorMessage) {
        ToastUtil.showToast(requireActivity(), errorMessage, Toast.LENGTH_SHORT);
    }

    @Override
    public void showCategories(List<Category> categories) {
        binding.recyclerviewMentorCategories.setVisibility(View.VISIBLE);
        adapter.setMentorCategoriesList(categories);
    }

    @Override
    public void showCategoryLoadingProgress() {
        binding.categoryLoadingProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideCategoryLoadingProgress() {
        binding.categoryLoadingErrorContainer.setVisibility(View.GONE);
        binding.categoryLoadingProgress.setVisibility(View.GONE);
    }

    @Override
    public void navigateToNextPage(Question question) {
        Fragment fragment = SelectMentorsFragment.newInstance(question);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.replace(R.id.drawer_layout, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void showCategoryLoadingError() {
        binding.categoryLoadingErrorContainer.setVisibility(View.VISIBLE);
        binding.categoryLoadingRetryBtn.setOnClickListener(v -> {
            binding.categoryLoadingErrorContainer.setVisibility(View.GONE);
            presenter.loadMentorCategories();
        });
    }

    @Override
    public void onCategoryItemClicked(String categoryId, boolean isSelected) {
        if (isSelected) {
            selectedCategories.add(categoryId);
        } else {
            selectedCategories.remove(categoryId);
        }
        selectedCategoriesChangeObservable.onNext(selectedCategories);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.next_button) {
            presenter.saveQuestion(binding.editTextQuestion.getText().toString(), selectedCategories);
        }
    }
}
