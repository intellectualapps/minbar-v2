package com.affinislabs.minbar.ui.auth.forgotpassword;

import android.content.Context;

import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.MinbarDataRepository;
import com.affinislabs.minbar.ui.common.base.BasePresenter;

import java.util.Random;

import javax.inject.Inject;

/**
 * Created by felixunlimited on 1/30/18.
 */

class ForgotPasswordPresenter extends BasePresenter<ForgotPasswordContract.View> {
    private MinbarDataRepository repository;

    @Inject
    ForgotPasswordPresenter(MinbarDataRepository repository) {
        this.repository = repository;
    }

    void resetPassword(Context context, String usernameOrEmail) {
        getView().showProgress();
        if (new Random().nextBoolean()) {
            getView().hideProgress();
            getView().showPasswordResetSuccessMessage(context.getString(R.string.password_reset_sent));
            getView().navigateToLoginPage();
        } else {
            getView().hideProgress();
            getView().showPasswordResetFailMessage(context.getString(R.string.password_reset_not_sent));
        }
    }
}
