package com.affinislabs.minbar.ui.questions.questionstoanswer;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.affinislabs.minbar.MinbarApplication;
import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.models.Question;
import com.affinislabs.minbar.databinding.QuestionsToAnswerFragmentBinding;
import com.affinislabs.minbar.ui.answers.AnswersActivity;
import com.affinislabs.minbar.ui.common.SwipeToDeleteCallback;
import com.affinislabs.minbar.ui.questions.QuestionsAdapter;
import com.affinislabs.minbar.utils.Constants;

import java.util.List;

import javax.inject.Inject;

import static com.affinislabs.minbar.utils.Constants.ANSWERS_ACTIVITY_REQUEST_CODE;

/**
 * minbar-v2
 * Michael Obi
 * 05/04/2018, 9:55 PM
 */
public class QuestionsToAnswerFragment extends Fragment implements QuestionsToAnswerView,
        QuestionsAdapter.QuestionItemClickListener {
    @Inject
    QuestionsToAnswerPresenter presenter;

    QuestionsToAnswerFragmentBinding binding;
    QuestionsAdapter questionsAdapter;
    List<Question> questions;

    public static QuestionsToAnswerFragment newInstance() {
        return new QuestionsToAnswerFragment();
    }

    public void setupQuestionsList() {
        questionsAdapter = new QuestionsAdapter(this);
        binding.questionsToAnswer.setEmptyView(binding.noQuestions);
        binding.questionsToAnswer.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.questionsToAnswer.addItemDecoration(new DividerItemDecoration(requireActivity(),
                DividerItemDecoration.VERTICAL));
        binding.questionsToAnswer.setAdapter(questionsAdapter);
        SwipeToDeleteCallback swipeToDeleteCallback = new SwipeToDeleteCallback(getActivity()) {
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                Question question = questions.get(viewHolder.getAdapterPosition());
                presenter.deleteMentorQuestion(question.getId());
                questionsAdapter.removeItemAt(viewHolder.getAdapterPosition());
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(swipeToDeleteCallback);
        itemTouchHelper.attachToRecyclerView(binding.questionsToAnswer);
    }

    @Override
    public void showProgress() {
        binding.questionsLoadingProgress.setVisibility(View.VISIBLE);
        binding.questionsToAnswer.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        binding.questionsLoadingProgress.setVisibility(View.GONE);
    }

    @Override
    public void showError(String errorMessage) {
        Toast.makeText(requireActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showQuestionsToAnswer(List<Question> questionsToAnswer) {
        this.questions = questionsToAnswer;
        binding.questionsToAnswer.setVisibility(View.VISIBLE);
        questionsAdapter.setItems(questionsToAnswer);
    }

    @Override
    public void showDeclinedQuestion() {
        Toast.makeText(getActivity(), R.string.question_declined, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onQuestionItemClicked(Question question) {
        Intent i = new Intent(getActivity(), AnswersActivity.class);
        i.putExtra("question", question);
        startActivityForResult(i, Constants.ANSWERS_ACTIVITY_REQUEST_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == ANSWERS_ACTIVITY_REQUEST_CODE) {
            if (intent != null && intent.getData() != null) {
                String returnedResult = intent.getData().toString();
                if (returnedResult.equals(Constants.ACTIVITY_RESULT_DONE_WITH_ANSWER)) {
                    presenter.loadQuestionsToAnswer();
                }
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MinbarApplication) requireActivity().getApplication())
                .getAppComponent()
                .questionsComponent()
                .inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_questions_to_answer,
                container, false);
        presenter.attachView(this);
        setupQuestionsList();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.loadQuestionsToAnswer();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }


}
