package com.affinislabs.minbar.ui.threads;

import com.affinislabs.minbar.data.models.Answer;
import com.affinislabs.minbar.data.models.AnswerReply;
import com.affinislabs.minbar.ui.common.base.RemoteView;

import java.util.List;

/**
 * minbar-v2
 * Michael Obi
 * 09/02/2018, 4:13 AM
 */

interface ThreadView extends RemoteView {

    void showAnswerReplies(List<AnswerReply> answerReplies);

    void sendingReplyProgress();

    void finishedSendingReply();

    void showInitialAnswer(Answer answer);

    void showSaveAnswerError();

    void showMarkAsDoneProgress();

    void hideMarkAsDoneProgress();

    void doneWithAnswer();

    void showDoneButton();

    void hideRatingBar();

    void showRatingBar();

    void hideInviteAdvisorButton();

    void showInviteAdvisorButton();

    void showInviteAdvisorProgress();

    void hideInviteAdvisorProgress();

    void inviteAdvisorSuccess();
}
