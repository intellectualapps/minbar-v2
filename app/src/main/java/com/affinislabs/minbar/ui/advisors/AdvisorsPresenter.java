package com.affinislabs.minbar.ui.advisors;

import android.view.View;

import com.affinislabs.minbar.data.MinbarDataRepository;
import com.affinislabs.minbar.ui.common.base.BasePresenter;
import com.affinislabs.minbar.utils.Constants;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created
 by felixunlimited
 on 1/29/18.
 */

class AdvisorsPresenter extends BasePresenter<AdvisorsView> {
    private MinbarDataRepository repository;

    @Inject
    AdvisorsPresenter(MinbarDataRepository repository) {
        this.repository = repository;
    }

    void loadAdvisors() {
        getView().showProgress();
        addDisposableSubscription(repository.getAdvisors()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(advisors -> {
                    getView().hideProgress();
                    getView().showAdvisors(advisors);
                }, error -> {
                    getView().hideProgress();
                    getView().showError(error.getMessage());
                }));
        addDisposableSubscription(repository.getMentees()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mentees -> getView().showMentees(mentees), error -> getView().showError(error.getMessage())));
    }

    void decideMenteeRequest(View advisorDecisionView, String decision, String username) {
        if (decision.equals(Constants.ACCEPT)) {
            repository.acceptMenteeRequest(username, accepted -> getView().handleAcceptAdvisorDecision(advisorDecisionView,accepted));
        } else if (decision.equals(Constants.DECLINE)) {
            repository.declineMenteeRequest(username, declined -> getView().handleDeclineAdvisorDecision(advisorDecisionView,declined));
        }
    }
}
