package com.affinislabs.minbar.ui.profileview;

import com.affinislabs.minbar.data.models.Question;
import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.ui.common.base.RemoteView;

import java.util.List;

/**
 * Created
 * by User
 * on 2/23/2018.
 */

interface ProfileView extends RemoteView {
    void showSecondPartyPublicProfile(User user);

    void showConversations(List<Question> questionsUserAsked);

    void finishInvitingAdvisor();
}