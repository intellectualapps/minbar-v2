package com.affinislabs.minbar.ui.questions;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.models.Question;
import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.ui.profileview.ProfileViewActivity;
import com.affinislabs.minbar.utils.Constants;
import com.affinislabs.minbar.utils.DateUtils;
import com.affinislabs.minbar.utils.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created
 * by felixunlimited
 * on 1/25/18.
 */

public class QuestionsAdapter extends RecyclerView.Adapter<QuestionsAdapter.ViewHolder> {

    private List<Question> questions = new ArrayList<>();
    private QuestionItemClickListener questionItemClickListener;

    public QuestionsAdapter(QuestionItemClickListener questionItemClickListener) {
        this.questionItemClickListener = questionItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_question,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        final Context context = viewHolder.itemView.getContext();
        final Question question = questions.get(position);
        User writer = question.getWriter();
        if (writer != null) {
            viewHolder.userFullName.setText(writer.getFullName());
            ImageLoader.loadImage(viewHolder.userProfilePhoto, writer.getPhotoUrl(), R.drawable.ic_user);
            if (writer.getUsername() != null) {
                viewHolder.userFullName.setOnClickListener(view -> {
                    Intent profileViewIntent = new Intent(context, ProfileViewActivity.class);
                    profileViewIntent.putExtra(Constants.SECOND_PARTY_USERNAME, question.getAsker());
                    context.startActivity(profileViewIntent);
                });
                viewHolder.userProfilePhoto.setOnClickListener(view -> {
                    Intent profileViewIntent = new Intent(context, ProfileViewActivity.class);
                    profileViewIntent.putExtra(Constants.SECOND_PARTY_USERNAME, question.getAsker());
                    context.startActivity(profileViewIntent);
                });
            }
        }
        if (question.getDateAsked() != null) {
            viewHolder.questionDateView.setText(DateUtils.getRelativeDate(question.getDateAsked()
                    .getTime()));
        }

        viewHolder.questionBodyView.setText(question.getBody());
        viewHolder.itemView.setOnClickListener(view -> questionItemClickListener.onQuestionItemClicked(question));
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    public void setItems(List<Question> items) {
        this.questions = items;
        notifyDataSetChanged();
    }

    public void removeItemAt(int position) {
        questions.remove(position);
        notifyItemRemoved(position);
    }

    public interface QuestionItemClickListener {
        void onQuestionItemClicked(Question question);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        TextView userFullName, questionDateView, questionBodyView;
        ImageView userProfilePhoto, unreadQuestionsIndicator;

        ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            userFullName = itemView.findViewById(R.id.user_full_name);
            userProfilePhoto = itemView.findViewById(R.id.user_profile_photo);
            questionDateView = itemView.findViewById(R.id.question_date_view);
            questionBodyView = itemView.findViewById(R.id.question_body_view);
            unreadQuestionsIndicator = itemView.findViewById(R.id.unread_question_indicator);
        }
    }
}
