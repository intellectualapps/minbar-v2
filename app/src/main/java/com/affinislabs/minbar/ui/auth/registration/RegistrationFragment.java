package com.affinislabs.minbar.ui.auth.registration;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.affinislabs.minbar.MinbarApplication;
import com.affinislabs.minbar.R;
import com.affinislabs.minbar.databinding.RegistrationFragmentBinding;
import com.affinislabs.minbar.ui.auth.AuthActivity;
import com.affinislabs.minbar.ui.common.base.BaseFragment;
import com.affinislabs.minbar.ui.common.base.ViewPagerFragmentLoadedListener;
import com.affinislabs.minbar.utils.ToastUtil;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

import static android.text.TextUtils.isEmpty;

/**
 * Minbar
 * Michael Obi
 * 06 January 2018, 1:02 AM
 */

public class RegistrationFragment extends BaseFragment implements RegistrationContract.View, View.OnClickListener {
    private static final String TAG = "RegistrationFragment";
    RegistrationFragmentBinding binding;
    @Inject
    RegistrationPresenter presenter;
    Subject<Boolean> formValidObservable = PublishSubject.create();
    private Flowable<CharSequence> usernameChangeObservable;
    private Flowable<CharSequence> passwordChangeObservable;
    private Flowable<CharSequence> confirmPasswordChangeObservable;
    private boolean passwordValid = false;
    private boolean usernameValid = false;

    public RegistrationFragment() {
    }

    public static RegistrationFragment newInstance(ViewPagerFragmentLoadedListener fragmentLoadedListener) {
        RegistrationFragment fragment = new RegistrationFragment();
        fragment.fragmentLoadedListener = fragmentLoadedListener;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MinbarApplication) getActivity().getApplication())
                .getAppComponent()
                .authenticationComponent()
                .inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_registration, container,
                false);
        presenter.attachView(this);
        binding.registerButton.setOnClickListener(this);

        usernameChangeObservable = RxTextView.textChanges(binding.username).skip(1)
                .toFlowable(BackpressureStrategy.LATEST);
        passwordChangeObservable = RxTextView.textChanges(binding.password).skip(1)
                .toFlowable(BackpressureStrategy.LATEST);
        confirmPasswordChangeObservable = RxTextView.textChanges(binding.confirmPassword).skip(1)
                .toFlowable(BackpressureStrategy.LATEST);
        checkFormValidity();
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
    }

    private void checkFormValidity() {
        validateUsername();
        validatePassword();
        formValidObservable.subscribe(formIsValid -> binding.registerButton.setEnabled(formIsValid));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register_button:
                handleRegisterButtonClick();
                break;
            default:
        }
    }

    @Override
    public void showProgress() {
        showLoadingDialog(getString(R.string.registration_progress_label));
    }

    @Override
    public void hideProgress() {
        hideLoadingDialog();
    }

    @Override
    public void showError(String errorMessage) {
        ToastUtil.showToast(getActivity(), errorMessage, Toast.LENGTH_LONG);
    }

    @Override
    public void navigateToMainPage() {
        ((AuthActivity) getActivity()).navigateToNextPage();
    }

    @Override
    public void handleUsernameValidity(boolean isValid) {
        if (!isValid) {
            binding.usernameInputLayout.setError(getString(R.string.username_unavailable_error_message));
        } else {
            binding.usernameInputLayout.setError(null);
        }
        usernameValid = isValid;
        formValidObservable.onNext(isValid && passwordValid);
    }

    private void handleRegisterButtonClick() {
        String username = binding.username.getText().toString();
        String email = binding.emailAddress.getText().toString();
        String password = binding.password.getText().toString();
        presenter.onRegisterButtonClicked(username, email, password);
    }

    private void validateUsername() {
        usernameChangeObservable.debounce(500, TimeUnit.MILLISECONDS)
                .filter(text -> !isEmpty(text.toString()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(text -> presenter.validateUsername(text.toString()));
    }

    private void validatePassword() {
        Flowable.combineLatest(passwordChangeObservable, confirmPasswordChangeObservable,
                (newPassword, newConfirmPassword) -> {
                    boolean passwordLengthValid = !isEmpty(newPassword) && newPassword.length() >= 8;
                    if (!passwordLengthValid) {
                        binding.passwordInputLayout.setError(getString(R.string.password_length_error));
                    } else {
                        binding.passwordInputLayout.setError(null);
                    }

                    boolean passwordConfirmationValid = newPassword.toString().equals(newConfirmPassword.toString());
                    if (!passwordConfirmationValid) {
                        binding.confirmPasswordInputLayout.setError(getString(R.string.confirm_password_match_error));
                    } else {
                        binding.confirmPasswordInputLayout.setError(null);
                    }

                    return passwordLengthValid && passwordConfirmationValid;
                })
                .subscribe(passwordValid -> {
                    RegistrationFragment.this.passwordValid = passwordValid;
                    formValidObservable.onNext(passwordValid && usernameValid);
                });
    }
}
