package com.affinislabs.minbar.ui.profileedit.di;

import com.affinislabs.minbar.di.ScreenScoped;
import com.affinislabs.minbar.ui.profileedit.ProfileEditFragment;

import dagger.Subcomponent;

/**
 * minbar-v2
 * Michael Obi
 * 12/03/2018, 6:57 AM
 */

@ScreenScoped
@Subcomponent
public interface ProfileEditComponent {
    void inject(ProfileEditFragment activity);
}