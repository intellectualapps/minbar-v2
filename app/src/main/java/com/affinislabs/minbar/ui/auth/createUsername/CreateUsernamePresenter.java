package com.affinislabs.minbar.ui.auth.createUsername;

import android.util.Log;

import com.affinislabs.minbar.data.MinbarDataRepository;
import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.domain.auth.AuthManager;
import com.affinislabs.minbar.ui.common.base.BasePresenter;
import com.affinislabs.minbar.utils.Constants;

import org.json.JSONObject;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * minbar-v2
 * Michael Obi
 * 10/03/2018, 9:45 AM
 */

class CreateUsernamePresenter extends BasePresenter<CreateUsernameView> {

    private static final String TAG = "CreateUsernamePresenter";
    private final AuthManager authManager;
    private final MinbarDataRepository repository;

    @Inject
    CreateUsernamePresenter(AuthManager authManager, MinbarDataRepository repository) {
        this.authManager = authManager;
        this.repository = repository;
    }

    void validateUsername(String username) {
        addDisposableSubscription(authManager.checkUsernameValidity(username)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(status -> getView().handleUsernameValidity(status),
                        throwable -> getView().showError(throwable.getMessage())));
    }

    void saveUsername(String username, String email, String tempKey) {
        getView().showProgress();
        addDisposableSubscription(repository.createUsername(username, email, tempKey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                    repository.saveAuthToken(user.getAuthToken());
                    getView().saveUserProfile();
                }, e -> {
                    getView().showError(e.getMessage());
                    Log.e(TAG, e.getMessage());
                }));
    }

    void saveProfile(String username, @AuthManager.AuthMethod String authMethod,
                     JSONObject profileData) {
        getView().showProgress();
        Single<User> updateProfileObservable;
        switch (authMethod) {
            case Constants.AUTH_FACEBOOK:
                updateProfileObservable = authManager.updateProfileFromFacebook(username, profileData);
                break;
            case Constants.AUTH_LINKEDIN:
                updateProfileObservable = authManager.updateProfileFromLinkedIn(username, profileData);
                break;
            default:
                return;
        }

        addDisposableSubscription(updateProfileObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                    repository.saveUserData(user);
                    authManager.updateFcmToken(user.getUsername());
                    getView().navigateToMain();
                }, throwable -> {
                    Log.e(TAG, throwable.getMessage());
                    getView().showUpdateProfileError();
                }));
    }
}
