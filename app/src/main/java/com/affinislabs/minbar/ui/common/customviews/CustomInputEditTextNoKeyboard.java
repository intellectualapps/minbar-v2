package com.affinislabs.minbar.ui.common.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;
import android.view.inputmethod.InputMethodManager;

import com.affinislabs.minbar.R;
import com.affinislabs.minbar.utils.FontUtils;


public class CustomInputEditTextNoKeyboard extends TextInputEditText {
    private static final String DEFAULT_SCHEMA = "xmlns:android=\"http://schemas.android.com/apk/res/android\"";
    private int textStyle = 0;

    public CustomInputEditTextNoKeyboard(Context context) {
        this(context, null);
    }

    public CustomInputEditTextNoKeyboard(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomInputEditTextNoKeyboard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            init(context, attrs, defStyleAttr);
        }
    }

    @Override
    public boolean onCheckIsTextEditor() {
        hideKeyboard();
        return super.onCheckIsTextEditor();
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        super.onSelectionChanged(selStart, selEnd);
        hideKeyboard();
    }

    private void hideKeyboard(){
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindowToken(), 0);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {

        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomEditText);

            textStyle = 0;
            if (a.hasValue(R.styleable.CustomEditText_textStyle)) {
                textStyle = a.getInt(R.styleable.CustomEditText_textStyle, 0);
            } else {
                //use default schema
                textStyle = attrs.getAttributeIntValue(DEFAULT_SCHEMA, "textStyle", 0);
            }

            a.recycle();

            applyCustomFont(context, textStyle);
        }

    }

    private void applyCustomFont(Context context, int textStyle) {
        Typeface typeface = FontUtils.selectTypeface(context, textStyle);

        setTypeface(typeface);
    }

    @Override
    public void setInputType(int type) {
        super.setInputType(type);

        if (getContext() != null) {
            applyCustomFont(getContext(), textStyle);
        }
    }
}
