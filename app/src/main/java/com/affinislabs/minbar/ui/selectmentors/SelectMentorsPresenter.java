package com.affinislabs.minbar.ui.selectmentors;

import com.affinislabs.minbar.data.MinbarDataRepository;
import com.affinislabs.minbar.data.models.Question;
import com.affinislabs.minbar.ui.common.base.BasePresenter;

import java.util.Set;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by felixunlimited on 1/29/18.
 */

class SelectMentorsPresenter extends BasePresenter<SelectMentorsView> {
    private static final String TAG = "SelectMentorsPresenter";
    private MinbarDataRepository repository;

    @Inject
    SelectMentorsPresenter(MinbarDataRepository repository) {
        this.repository = repository;
    }

    void loadMentors(Question question) {
        getView().showMentorsLoadingProgress();
        addDisposableSubscription(repository.getMentorsForCategories(question)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(relevantMentors -> {
                    getView().hideMentorsLoadingProgress();
                    getView().showMentors(relevantMentors);
                }, error -> {
                    getView().hideMentorsLoadingProgress();
                    getView().showError(error.getMessage());
                }));
    }

    void saveSelectedMentors(Question question, Set<String> selectedMentorIds) {
        getView().showProgress();
        addDisposableSubscription(repository.saveSelectedQuestionMentors
                (question.getId(), selectedMentorIds)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    getView().hideProgress();
                    getView().finishAskQuestionProcess();
                }, error -> {
                    getView().hideProgress();
                    getView().showError(error.getMessage());
                }));
    }
}
