package com.affinislabs.minbar.ui.profileview.di;

import com.affinislabs.minbar.di.ScreenScoped;
import com.affinislabs.minbar.ui.profileview.ProfileViewActivity;

import dagger.Subcomponent;

/**
 * minbar-v2
 * Michael Obi
 * 12/03/2018, 6:57 AM
 */

@ScreenScoped
@Subcomponent
public interface ProfileViewComponent {
    void inject(ProfileViewActivity activity);
}