package com.affinislabs.minbar.ui.becomementor;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.models.Category;
import com.affinislabs.minbar.ui.common.customviews.CheckableButton;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * minbar-v2
 * Michael Obi
 * 23/01/2018, 10:09 AM
 */

public class MentorCategoriesAdapter extends RecyclerView.Adapter<MentorCategoriesAdapter.ViewHolder> {

    private List<Category> mentorCategoriesList = new ArrayList<>();
    private CategoryItemClickListener categoryItemClickListener;
    private Set<String> selectedCategories = new HashSet<>();

    public MentorCategoriesAdapter(CategoryItemClickListener categoryItemClickListener) {
        this.categoryItemClickListener = categoryItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_category,
                parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Category category = mentorCategoriesList.get(position);
        holder.btnMentorCategory.setText(category.getName());
        holder.btnMentorCategory.setOnClickListener(btn -> categoryItemClickListener
                .onCategoryItemClicked(category.getId(), btn.isSelected()));
        if (selectedCategories.contains(category.getId())) {
            holder.btnMentorCategory.setChecked(true);
        }
    }

    @Override
    public int getItemCount() {
        return mentorCategoriesList.size();
    }

    public void setMentorCategoriesList(List<Category> mentorCategoriesList) {
        this.mentorCategoriesList = mentorCategoriesList;
        notifyDataSetChanged();
    }

    public void setMentorCategoriesList(List<Category> mentorCategoriesList, Set<String> selectedCategories) {
        this.selectedCategories = selectedCategories;
        setMentorCategoriesList(mentorCategoriesList);
    }

    public interface CategoryItemClickListener {
        void onCategoryItemClicked(String categoryId, boolean isSelected);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        CheckableButton btnMentorCategory;

        public ViewHolder(View itemView) {
            super(itemView);
            btnMentorCategory = itemView.findViewById(R.id.btn_mentor_category);
        }
    }
}
