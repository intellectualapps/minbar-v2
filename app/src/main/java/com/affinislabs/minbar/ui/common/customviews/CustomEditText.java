package com.affinislabs.minbar.ui.common.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.affinislabs.minbar.R;
import com.affinislabs.minbar.utils.FontUtils;


public class CustomEditText extends AppCompatEditText {
    private static final String ANDROID_SCHEMA = "xmlns:android=\"http://schemas.android.com/apk/res/android\"";

    public CustomEditText(Context context) {
        this(context, null);
    }


    public CustomEditText(Context context, AttributeSet attrs) {
        this(context, attrs, android.support.v7.appcompat.R.attr.editTextStyle);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomEditText,
                    defStyle, 0);

            int textStyle;
            if (a.hasValue(R.styleable.CustomEditText_textStyle)) {
                textStyle = a.getInt(R.styleable.CustomEditText_textStyle, 0);
            } else {
                //use default schema
                textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", 0);
            }

            a.recycle();

            Typeface typeface = FontUtils.selectTypeface(context, textStyle);
            setTypeface(typeface);
        }
    }
}
