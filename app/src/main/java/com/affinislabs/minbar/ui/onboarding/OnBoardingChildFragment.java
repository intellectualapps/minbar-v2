package com.affinislabs.minbar.ui.onboarding;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.models.OnBoardingContent;

public class OnBoardingChildFragment extends Fragment {

    private OnBoardingContent onBoardingContent;
    private ImageView contentIconView;
    private TextView contentHeadingView, contentDescriptionView;

    public OnBoardingChildFragment() {
    }

    public static OnBoardingChildFragment newInstance(OnBoardingContent onBoardingContent) {
        OnBoardingChildFragment fragment = new OnBoardingChildFragment();
        fragment.setOnBoardingContent(onBoardingContent);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_on_boarding_child, container, false);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        contentHeadingView = view.findViewById(R.id.content_heading_view);
        contentDescriptionView = view.findViewById(R.id.content_description_view);
        contentIconView = view.findViewById(R.id.content_icon_view);
        setData(onBoardingContent);
    }

    public OnBoardingContent getOnBoardingContent() {
        return onBoardingContent;
    }

    public void setOnBoardingContent(OnBoardingContent onBoardingContent) {
        this.onBoardingContent = onBoardingContent;
    }

    private void setData(OnBoardingContent onBoardingContent) {
        contentHeadingView.setText(onBoardingContent.getHeading());
        contentDescriptionView.setText(onBoardingContent.getDescription());
        contentIconView.setImageResource(onBoardingContent.getIconResource());
    }
}
