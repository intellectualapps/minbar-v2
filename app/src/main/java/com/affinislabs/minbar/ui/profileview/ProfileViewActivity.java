package com.affinislabs.minbar.ui.profileview;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.affinislabs.minbar.MinbarApplication;
import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.models.Question;
import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.databinding.ProfileViewActivityBinding;
import com.affinislabs.minbar.domain.auth.AuthManager;
import com.affinislabs.minbar.ui.answers.AnswersActivity;
import com.affinislabs.minbar.ui.questions.QuestionsAdapter;
import com.affinislabs.minbar.utils.Constants;
import com.affinislabs.minbar.utils.ImageLoader;
import com.affinislabs.minbar.utils.ToastUtil;

import java.util.List;

import javax.inject.Inject;

public class ProfileViewActivity extends AppCompatActivity implements ProfileView, QuestionsAdapter.QuestionItemClickListener, View.OnClickListener {

    ProfileViewActivityBinding binding;
    String secondPartyUsername;
    @Inject
    AuthManager authManager;
    @Inject
    ProfileViewPresenter presenter;
    private QuestionsAdapter conversationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((MinbarApplication) getApplication())
                .getAppComponent()
                .profileViewComponent()
                .inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile_view);
        User loggedInUser = authManager.getUser();
        secondPartyUsername = getIntent().getStringExtra(Constants.SECOND_PARTY_USERNAME);

        presenter.attachView(this);

        setSupportActionBar(binding.toolbarLayout.toolbar);
        binding.toolbarLayout.toolbar.setNavigationIcon(getResources().getDrawable(R.drawable
                .ic_action_back));
        binding.toolbarLayout.toolbar.setNavigationOnClickListener(view -> finish());
        binding.inviteAsAdvisor.setOnClickListener(this);

        if (loggedInUser.getUsername().equals(secondPartyUsername)) {
            populateProfile(loggedInUser);
        } else {
            presenter.loadProfile(secondPartyUsername);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle("");
        binding.toolbarLayout.toolbarTitle.setText(getString(R.string.about_user, title));
    }

    public void showPublicProfile() {

    }

    public void showMentorProfile() {
        binding.shortBioContainer.setVisibility(View.VISIBLE);
        binding.inviteAsAdvisor.setVisibility(View.VISIBLE);
        presenter.loadConversations();
    }

    public void showPAdvisorProfile() {
        binding.shortBioContainer.setVisibility(View.VISIBLE);
        binding.askNewQuestion.setVisibility(View.VISIBLE);
        binding.removeAsAdvisor.setVisibility(View.VISIBLE);
        presenter.loadConversations();
    }

    public void populateProfile(User user) {
        binding.fullname.setText(user.getFullName());
        binding.username.setText(String.format("@%s", user.getUsername()));
        binding.longBio.setText(user.getLongBio());
        ImageLoader.loadImage(binding.profilePhoto, user.getPhotoUrl(), R.drawable.ic_user);
        this.setTitle(user.getFullName());
        if (user.getFacebookProfile() != null && !user.getFacebookProfile().isEmpty()) {
            binding.facebookBtn.setVisibility(View.VISIBLE);
            binding.facebookBtn.setOnClickListener(v -> startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(user.getFacebookProfile()))));
        }

        if (user.getLinkedinProfile() != null && !user.getLinkedinProfile().isEmpty()) {
            binding.linkedinBtn.setVisibility(View.VISIBLE);
            binding.linkedinBtn.setOnClickListener(v -> startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(user.getLinkedinProfile()))));
        }
    }

    public void setUpAdapter(List<Question> questions) {
        conversationAdapter = new QuestionsAdapter(this);
        conversationAdapter.setItems(questions);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.recyclerviewConversations.setAdapter(conversationAdapter);
        binding.recyclerviewConversations.setLayoutManager(linearLayoutManager);
        binding.recyclerviewConversations.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));
    }

    @Override
    public void showProgress() {
        binding.recyclerviewConversations.setVisibility(View.GONE);
        binding.conversationsLoadingProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        binding.conversationsLoadingProgress.setVisibility(View.GONE);
    }

    @Override
    public void showError(String errorMessage) {
        Snackbar snackbar = Snackbar.make(binding.container,
                R.string.answers_loading_error,
                Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.retry, v -> {
            snackbar.dismiss();
            presenter.loadProfile(secondPartyUsername);
        });
        snackbar.show();
    }

    @Override
    public void showSecondPartyPublicProfile(User user) {
        populateProfile(user);
        switch (user.getRelationshipToCaller()) {
            case "ADVISOR":
                showPAdvisorProfile();
                break;
            case "MENTOR":
                showMentorProfile();
                break;
            case "NO_CONNECTION":
                showPublicProfile();
                break;
            default:
                showPublicProfile();
        }
    }

    @Override
    public void showConversations(List<Question> questions) {
        hideProgress();
        if (questions != null) {
            setUpAdapter(questions);
        } else {
            ToastUtil.showToast(ProfileViewActivity.this, getString(R.string.null_response), Toast.LENGTH_LONG);
        }
    }

    @Override
    public void finishInvitingAdvisor() {
        binding.inviteAsAdvisor.setText(Constants.INVITATION_SENT);
        binding.inviteAsAdvisor.setEnabled(false);
    }

    @Override
    public void onQuestionItemClicked(Question question) {
        Intent i = new Intent(ProfileViewActivity.this, AnswersActivity.class);
        i.putExtra(Constants.QUESTION, question);
        startActivity(i);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.invite_as_advisor:
                presenter.sendAdvisorInvitation(secondPartyUsername);
                break;
        }
    }
}
