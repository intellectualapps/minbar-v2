package com.affinislabs.minbar.ui.profileedit;

/**
 * minbar
 * Michael Obi
 * 24/04/2018, 11:30 PM
 */
public interface ProfileUpdatedListener {
    void onProfileUpdated();
}
