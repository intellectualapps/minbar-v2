package com.affinislabs.minbar.ui.auth.createUsername;

import com.affinislabs.minbar.ui.common.base.RemoteView;

import io.reactivex.functions.Consumer;

/**
 * minbar-v2
 * Michael Obi
 * 10/03/2018, 9:43 AM
 */

interface CreateUsernameView extends RemoteView {
    void handleUsernameValidity(boolean isValid);

    void saveUserProfile();

    void navigateToMain();

    void showUpdateProfileError();
}
