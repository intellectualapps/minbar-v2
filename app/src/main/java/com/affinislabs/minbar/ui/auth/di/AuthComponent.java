package com.affinislabs.minbar.ui.auth.di;

import com.affinislabs.minbar.di.ScreenScoped;
import com.affinislabs.minbar.ui.auth.AuthActivity;
import com.affinislabs.minbar.ui.auth.AuthFragment;
import com.affinislabs.minbar.ui.auth.createUsername.CreateUsernameFragment;
import com.affinislabs.minbar.ui.auth.forgotpassword.ForgotPasswordFragment;
import com.affinislabs.minbar.ui.auth.login.LoginFragment;
import com.affinislabs.minbar.ui.auth.registration.RegistrationFragment;

import dagger.Subcomponent;

/**
 * minbar-v2
 * Michael Obi
 * 11/03/2018, 11:23 PM
 */

@ScreenScoped
@Subcomponent
public interface AuthComponent {
    void inject(AuthActivity activity);

    void inject(LoginFragment fragment);

    void inject(ForgotPasswordFragment fragment);

    void inject(RegistrationFragment fragment);

    void inject(CreateUsernameFragment fragment);

    void inject(AuthFragment authFragment);
}
