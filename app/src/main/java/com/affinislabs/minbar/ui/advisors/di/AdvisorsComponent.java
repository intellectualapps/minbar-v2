package com.affinislabs.minbar.ui.advisors.di;

import com.affinislabs.minbar.di.ScreenScoped;
import com.affinislabs.minbar.ui.advisors.AdvisorsFragment;

import dagger.Subcomponent;

/**
 * minbar-v2
 * Michael Obi
 * 12/03/2018, 9:15 AM
 */

@Subcomponent
@ScreenScoped
public interface AdvisorsComponent {
    void inject(AdvisorsFragment fragment);
}
