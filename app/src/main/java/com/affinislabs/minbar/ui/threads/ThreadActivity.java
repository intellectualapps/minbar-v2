package com.affinislabs.minbar.ui.threads;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Toast;

import com.affinislabs.minbar.MinbarApplication;
import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.models.Answer;
import com.affinislabs.minbar.data.models.AnswerReply;
import com.affinislabs.minbar.data.models.Question;
import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.databinding.ThreadActivityBinding;
import com.affinislabs.minbar.domain.auth.AuthManager;
import com.affinislabs.minbar.utils.Constants;
import com.affinislabs.minbar.utils.DateUtils;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;


/**
 * minbar-v2
 * Michael Obi
 * 14/02/2018, 2:33 PM
 */

public class ThreadActivity extends AppCompatActivity implements ThreadView {
    private static final String TAG = "ThreadActivity";
    @Inject
    ThreadPresenter presenter;
    @Inject
    AuthManager authManager;
    ThreadActivityBinding binding;
    Answer answer;
    private ThreadAdapter repliesAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MinbarApplication) getApplication())
                .getAppComponent()
                .threadsComponent()
                .inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_thread);
        presenter.attachView(this);

        setSupportActionBar(binding.toolbarLayout.toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        answer = getIntent().getParcelableExtra(Constants.ANSWER);
        Question question;
        if (answer != null) {
            question = answer.getQuestion();
        } else {
            question = getIntent().getParcelableExtra("question");
        }

        binding.toolbarLayout.toolbar
                .setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        binding.toolbarLayout.toolbarTitle.setText(getString(R.string.users_answer, ""));
        binding.textViewQuestion.setText(question.getBody());

        Date questionDate = question.getDateAsked();
        if (questionDate != null) {
            binding.questionTime.setText(getString(R.string.question_ask_time,
                    DateUtils.getRelativeDateText(questionDate), DateUtils.getShortTime(questionDate)));
        }

        binding.editTextReply.setImeOptions(EditorInfo.IME_ACTION_SEND);
        binding.editTextReply.setRawInputType(InputType.TYPE_CLASS_TEXT);
        binding.editTextReply.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                String text = binding.editTextReply.getText().toString();
                if (!text.isEmpty()) {
                    if (answer != null) {
                        presenter.sendReply(text, answer, authManager.getUser());
                    } else {
                        presenter.sendInitialAnswer(text, question);
                    }
                }
            }
            return true;
        });

        repliesAdapter = new ThreadAdapter();
        binding.recyclerviewAnswerReplies.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));
        binding.recyclerviewAnswerReplies.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerviewAnswerReplies.setAdapter(repliesAdapter);

        if (answer != null) {
            showInitialAnswer(answer);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showError(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showAnswerReplies(List<AnswerReply> answerReplies) {
        binding.recyclerviewAnswerReplies.setVisibility(View.VISIBLE);
        repliesAdapter.setItems(answerReplies);
    }

    @Override
    public void sendingReplyProgress() {
        binding.editTextReply.setEnabled(false);
    }

    @Override
    public void finishedSendingReply() {
        binding.editTextReply.setText("");
        binding.editTextReply.setEnabled(true);
    }

    @Override
    public void showInitialAnswer(Answer answer) {
        this.answer = answer;
        binding.btnAdvisorInvite.setVisibility(View.VISIBLE);
        binding.btnAdvisorInvite.setOnClickListener(v -> {
            binding.btnAdvisorInvite.setEnabled(false);
            presenter.inviteAdvisor(answer.getMentorUsername());
        });

        binding.setAnswer(answer);
        presenter.loadReplies(answer);
        User user = authManager.getUser();
        if (answer.wasWrittenBy(user)) {
            binding.btnDone.setVisibility(View.VISIBLE);
            binding.btnAdvisorInvite.setVisibility(View.GONE);
            binding.btnDone.setText(R.string.done);
            binding.btnDone.setOnClickListener(v -> presenter.doneWithAnswer(answer));
        } else if (answer.getQuestion().wasWrittenBy(user)) {
            binding.btnDone.setVisibility(View.VISIBLE);
            binding.btnAdvisorInvite.setVisibility(View.VISIBLE);
            binding.btnDone.setText(R.string.i_am_happy_with_my_answer);
            binding.btnDone.setOnClickListener(v -> presenter.happyWithAnswer(answer));
        } else {
            binding.btnDone.setVisibility(View.GONE);
        }

        binding.rating.setOnRatingBarChangeListener((simpleRatingBar, rating, fromUser) -> {
            if (fromUser) {
                presenter.rateMentor(answer.getMentorUsername(), rating);
            }
        });
    }

    @Override
    public void showSaveAnswerError() {
        binding.editTextReply.setEnabled(true);
        binding.editTextReply.setError(getString(R.string.network_error));
    }

    @Override
    public void showMarkAsDoneProgress() {
        binding.btnDone.setVisibility(View.GONE);
        binding.doneProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideMarkAsDoneProgress() {
        binding.doneProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void doneWithAnswer() {
        hideMarkAsDoneProgress();
        Intent data = new Intent();
        data.setData(Uri.parse(Constants.ACTIVITY_RESULT_DONE_WITH_ANSWER));
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public void showDoneButton() {
        binding.btnDone.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRatingBar() {
        binding.ratingContainer.setVisibility(View.GONE);
    }

    @Override
    public void showRatingBar() {
        binding.ratingContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideInviteAdvisorButton() {
        binding.btnAdvisorInvite.setVisibility(View.GONE);
    }

    @Override
    public void showInviteAdvisorButton() {
        binding.btnAdvisorInvite.setVisibility(View.GONE);
        binding.btnAdvisorInvite.setEnabled(true);
    }

    @Override
    public void showInviteAdvisorProgress() {
        binding.inviteProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideInviteAdvisorProgress() {
        binding.inviteProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void inviteAdvisorSuccess() {
        Toast.makeText(this, R.string.invite_sent, Toast.LENGTH_SHORT).show();
    }
}
