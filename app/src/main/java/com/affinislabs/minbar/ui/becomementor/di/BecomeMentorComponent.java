package com.affinislabs.minbar.ui.becomementor.di;

import com.affinislabs.minbar.di.ScreenScoped;
import com.affinislabs.minbar.ui.becomementor.BecomeMentorActivity;

import dagger.Subcomponent;

/**
 * minbar-v2
 * Michael Obi
 * 11/03/2018, 4:56 PM
 */

@ScreenScoped
@Subcomponent
public interface BecomeMentorComponent {
    void inject(BecomeMentorActivity activity);
}