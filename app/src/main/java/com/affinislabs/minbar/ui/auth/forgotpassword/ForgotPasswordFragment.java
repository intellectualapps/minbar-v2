package com.affinislabs.minbar.ui.auth.forgotpassword;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.affinislabs.minbar.MinbarApplication;
import com.affinislabs.minbar.R;
import com.affinislabs.minbar.databinding.ForgotPasswordFragmentBinding;
import com.affinislabs.minbar.ui.common.base.BaseFragment;
import com.affinislabs.minbar.ui.common.customviews.CustomTextView;
import com.affinislabs.minbar.ui.meta.AboutFragment;
import com.affinislabs.minbar.utils.Constants;
import com.affinislabs.minbar.utils.ToastUtil;
import com.jakewharton.rxbinding2.widget.RxTextView;

import javax.inject.Inject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;

public class ForgotPasswordFragment extends BaseFragment implements View.OnClickListener, ForgotPasswordContract.View {
    @Inject
    ForgotPasswordPresenter presenter;
    private ForgotPasswordFragmentBinding binding;
    private Flowable<CharSequence> textEntryChangeObservable;
    private CustomTextView aboutAppButton, termsAppButton, privacyPolicyButton;

    public ForgotPasswordFragment() {

    }

    public static ForgotPasswordFragment newInstance() {
        return new ForgotPasswordFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MinbarApplication) getActivity().getApplication())
                .getAppComponent()
                .authenticationComponent()
                .inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forgot_password, container, false);
        presenter.attachView(this);

        aboutAppButton = binding.getRoot().findViewById(R.id.about_app_button);
        aboutAppButton.setOnClickListener(this);
        privacyPolicyButton = binding.getRoot().findViewById(R.id.privacy_policy_button);
        privacyPolicyButton.setOnClickListener(this);
        termsAppButton = binding.getRoot().findViewById(R.id.terms_app_button);
        termsAppButton.setOnClickListener(this);
        binding.passwordResetButton.setOnClickListener(this);

        textEntryChangeObservable = RxTextView.textChanges(binding.textEntry)
                .toFlowable(BackpressureStrategy.LATEST);

        textEntryChangeObservable.subscribe(text -> binding.passwordResetButton.setEnabled(text.length() > 0));
        return binding.getRoot();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.toolbar.setNavigationIcon(R.drawable.ic_action_back);
        binding.toolbar.setNavigationOnClickListener(view1 -> getActivity().getSupportFragmentManager().popBackStack());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.password_reset_button:
                String usernameOrEmail = binding.textEntry.getText().toString();
                presenter.resetPassword(getContext(), usernameOrEmail);
                break;
            case R.id.about_app_button:
                startMetaFragment(AboutFragment.newInstance());
                break;
            case R.id.privacy_policy_button:
                Intent privacyPolicyIntent = new Intent(Intent.ACTION_VIEW);
                privacyPolicyIntent.setData(Uri.parse(Constants.MA_PRIVACY_POLICY_URL));
                requireActivity().startActivityFromFragment(this, privacyPolicyIntent, 3000);
                break;
            case R.id.terms_app_button:
                Intent termsIntent = new Intent(Intent.ACTION_VIEW);
                termsIntent.setData(Uri.parse(Constants.MA_TERMS_URL));
                requireActivity().startActivityFromFragment(this, termsIntent, 3000);
                break;
            default:
        }
    }


    @Override
    public void showProgress() {
        showLoadingDialog(getString(R.string.forgot_password_progress_label));
    }

    @Override
    public void hideProgress() {
        hideLoadingDialog();
    }

    @Override
    public void showError(String errorMessage) {
    }

    @Override
    public void navigateToLoginPage() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void showPasswordResetFailMessage(String errorMessage) {
        ToastUtil.showToast(getActivity(), errorMessage, Toast.LENGTH_LONG);
    }

    @Override
    public void showPasswordResetSuccessMessage(String successMessage) {
        ToastUtil.showToast(getActivity(), successMessage, Toast.LENGTH_LONG);
    }
}
