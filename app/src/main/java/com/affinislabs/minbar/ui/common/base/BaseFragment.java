package com.affinislabs.minbar.ui.common.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.affinislabs.minbar.R;

/**
 * Minbar
 * Michael Obi
 * 05 January 2018, 11:52 PM
 */

public abstract class BaseFragment extends Fragment {
    protected ViewPagerFragmentLoadedListener fragmentLoadedListener;
    private ProgressDialog progressDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && fragmentLoadedListener != null) {
            fragmentLoadedListener.onViewPagerFragmentLoaded(this.getClass());
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void startFragment(Fragment frag) {
        if (getActivity() != null && frag != null) {
            FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getName());
            fragmentTransaction.addToBackStack(frag.getClass().getName());
            fragmentTransaction.commit();
        }
    }

    public void startMetaFragment(Fragment frag) {
        if (getActivity() != null && frag != null) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_down, R.anim.slide_in_down, R.anim.slide_out_up);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.replace(R.id.container, frag, frag.getClass().getName());
            fragmentTransaction.commit();
        }
    }

    protected void showLoadingDialog(String message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
        }

        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
    }

    protected void hideLoadingDialog() {
        if (progressDialog == null) return;
        progressDialog.dismiss();
    }
}
