package com.affinislabs.minbar.ui.profileedit;

import android.net.Uri;
import android.util.Log;

import com.affinislabs.minbar.data.MinbarDataRepository;
import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.ui.common.base.BasePresenter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Set;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * Created
 * by User
 * on 2/16/2018.
 */

class ProfilePresenter extends BasePresenter<ProfileView> {

    private static final String TAG = "ProfilePresenter";
    private MinbarDataRepository repository;

    @Inject
    ProfilePresenter(MinbarDataRepository repository) {
        this.repository = repository;
    }

    void saveProfile(String firstName, String lastName, String phoneNumber, String emailAddress,
                     String location, String longBio, Set<String> selectedCategoryIds) {
        getView().showProgress();
        Single<User> saveBioObservable = repository.saveProfile(firstName, lastName, phoneNumber,
                emailAddress, location, longBio, selectedCategoryIds);

        addDisposableSubscription(saveBioObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                    user.setMentorCategoryIds(selectedCategoryIds);
                    repository.saveUserProfile(user);
                    getView().showSaveSuccess();
                    getView().hideProgress();
                }, e -> {
                    getView().showError(e.getMessage());
                    getView().hideProgress();
                }));
    }

    void loadProfile() {
        User user = repository.getLoggedInUser();
        getView().showProfileData(user);
    }

    void loadMentorCategories() {
        getView().showCategoryLoadingProgress();
        Single<UserProfileCategories> getUserCategoriesObservable =
                Single.zip(repository.getAllCategories(), repository.loadLoggedInUser(),
                        (categories, user) -> new UserProfileCategories(categories, user.getMentorCategoryIds()));
        addDisposableSubscription(getUserCategoriesObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(userProfileCategories -> {
                            getView().hideCategoryLoadingProgress();
                            getView().showCategories(userProfileCategories.getAllCategories(),
                                    userProfileCategories.getUserCategoryIds());
                        },
                        error -> {
                            getView().hideCategoryLoadingProgress();
                            getView().showCategoryLoadingError();
                        }));
    }

    void connectToFacebook(JSONObject facebookProfileData) {
        String email = facebookProfileData.optString("email");
        String profileUrl = facebookProfileData.optString("link");
        String photoUrl = null;
        try {
            if (facebookProfileData.has("picture")) {
                photoUrl = facebookProfileData.getJSONObject("picture").getJSONObject("data").getString("url");
            }
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }
        getView().showProgress();
        addDisposableSubscription(repository.connectFacebookProfile(email, profileUrl, photoUrl)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                    repository.saveUserData(user);
                    getView().hideProgress();
                    getView().showFacebookConnectSuccess();
                }, throwable -> {
                    getView().hideProgress();
                    getView().showError(throwable.getMessage());
                }));
    }

    void disconnectFacebook() {
        getView().showProgress();
        addDisposableSubscription(repository.disconnectFacebook()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                    repository.saveUserData(user);
                    getView().hideProgress();
                    getView().showFacebookDisconnectSuccess();
                }, throwable -> {
                    getView().hideProgress();
                    getView().showError(throwable.getMessage());
                }));
    }

    public void uploadProfilePicture(Uri resultUri) {
        getView().showImageUploadProgress();
        addDisposableSubscription(repository.uploadProfilePicture(resultUri)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                            getView().showProfileData(user);
                            getView().showImageUploadSuccess();
                        },
                        e -> Log.e(TAG, e.getMessage(), e)));
    }
}
