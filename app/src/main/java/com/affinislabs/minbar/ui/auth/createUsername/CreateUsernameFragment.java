package com.affinislabs.minbar.ui.auth.createUsername;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.affinislabs.minbar.MinbarApplication;
import com.affinislabs.minbar.R;
import com.affinislabs.minbar.databinding.CreateUsernameBinding;
import com.affinislabs.minbar.ui.auth.AuthActivity;
import com.jakewharton.rxbinding2.widget.RxTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;


import static android.text.TextUtils.isEmpty;

/**
 * minbar-v2
 * Michael Obi
 * 10/03/2018, 9:43 AM
 */

public class CreateUsernameFragment extends Fragment implements CreateUsernameView {
    private static final String TAG = "CreateUsernameFragment";
    ProgressDialog progressDialog;
    String email, tempKey, authMethod;
    JSONObject profileData;
    @Inject
    CreateUsernamePresenter presenter;
    private Flowable<CharSequence> usernameChangeObservable;
    private Subject<Boolean> formValidObservable = PublishSubject.create();
    private CreateUsernameBinding binding;

    public CreateUsernameFragment() {
    }

    public static CreateUsernameFragment newInstance() {
        return new CreateUsernameFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MinbarApplication) getActivity().getApplication())
                .getAppComponent()
                .authenticationComponent()
                .inject(this);
        if (getArguments() != null) {
            email = getArguments().getString("email", "");
            tempKey = getArguments().getString("tempKey", "");
            authMethod = getArguments().getString("authMethod", "");
            String socialProfileData = getArguments().getString("socialProfileData", "");
            try {
                profileData = new JSONObject(socialProfileData);
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_create_username, container, false);
        presenter.attachView(this);
        binding.toolbar.toolbarTitle.setText(getString(R.string.create_username_title));
        binding.emailAddressInput.setText(email);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        formValidObservable.subscribe(formIsValid -> binding.createUsernameButton
                .setEnabled(formIsValid));
        usernameChangeObservable = RxTextView.textChanges(binding.usernameInput)
                .skip(1)
                .toFlowable(BackpressureStrategy.LATEST);
        usernameChangeObservable.debounce(1, TimeUnit.SECONDS)
                .filter(text -> !isEmpty(text.toString()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(text -> presenter.validateUsername(text.toString()));
        binding.createUsernameButton.setOnClickListener(v -> {
            String username = binding.usernameInput.getText().toString();
            presenter.saveUsername(username, email, tempKey);
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
    }

    @Override
    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
        }
        progressDialog.setMessage(getString(R.string.login_progress_label));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void showError(String errorMessage) {

    }

    @Override
    public void handleUsernameValidity(boolean isValid) {
        if (!isValid) {
            binding.usernameInputLayout.setError(getString(R.string
                    .username_unavailable_error_message));
        } else {
            binding.usernameInputLayout.setError(null);
        }
        formValidObservable.onNext(isValid);
    }

    @Override
    public void saveUserProfile() {
        String username = binding.usernameInput.getText().toString();
        presenter.saveProfile(username, authMethod, profileData);
    }


    @Override
    public void navigateToMain() {
        ((AuthActivity) getActivity()).navigateToNextPage();
    }

    @Override
    public void showUpdateProfileError() {
        showError(getString(R.string.update_profile_error));
    }
}
