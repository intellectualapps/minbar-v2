package com.affinislabs.minbar.ui.common.base;

import android.support.v4.app.Fragment;

/**
 * Minbar
 * Michael Obi
 * 06 January 2018, 1:32 AM
 */

public interface ViewPagerFragmentLoadedListener {
    <T extends Fragment> void onViewPagerFragmentLoaded(Class<T> clazz);
}
