package com.affinislabs.minbar.ui.auth;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.affinislabs.minbar.R;
import com.affinislabs.minbar.databinding.EmailAuthFragmentBinding;
import com.affinislabs.minbar.ui.auth.login.LoginFragment;
import com.affinislabs.minbar.ui.auth.registration.RegistrationFragment;
import com.affinislabs.minbar.ui.common.ViewPagerAdapter;
import com.affinislabs.minbar.ui.common.base.BaseFragment;
import com.affinislabs.minbar.ui.common.base.ViewPagerFragmentLoadedListener;
import com.affinislabs.minbar.ui.common.customviews.CustomTextView;
import com.affinislabs.minbar.ui.meta.AboutFragment;
import com.affinislabs.minbar.utils.Constants;
import com.affinislabs.minbar.utils.FontUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Minbar
 * Michael Obi
 * 05 January 2018, 10:58 PM
 */

public class EmailAuthFragment extends BaseFragment implements ViewPagerFragmentLoadedListener, View.OnClickListener {

    private EmailAuthFragmentBinding binding;
    private Map<String, String> fragmentTitleMap;
    private CustomTextView aboutAppButton, termsAppButton, privacyPolicyButton;

    public EmailAuthFragment() {
    }

    public static EmailAuthFragment newInstance() {
        return new EmailAuthFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentTitleMap = new HashMap<>();
        fragmentTitleMap.put(RegistrationFragment.class.getName(), "Create an account");
        fragmentTitleMap.put(LoginFragment.class.getName(), "Login to your account");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_email_auth, container, false);
        aboutAppButton = binding.getRoot().findViewById(R.id.about_app_button);
        aboutAppButton.setOnClickListener(this);
        privacyPolicyButton = binding.getRoot().findViewById(R.id.privacy_policy_button);
        privacyPolicyButton.setOnClickListener(this);
        termsAppButton = binding.getRoot().findViewById(R.id.terms_app_button);
        termsAppButton.setOnClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViewPager();
        setupTabLayout();
    }

    private void setupViewPager() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(RegistrationFragment.newInstance(this), getString(R.string.register_tab_label));
        adapter.addFragment(LoginFragment.newInstance(this), getString(R.string.login_tab_label));
        binding.viewPager.setAdapter(adapter);
        binding.tabs.setTabMode(TabLayout.MODE_SCROLLABLE);
        binding.tabs.setTabGravity(TabLayout.GRAVITY_CENTER);
        binding.tabs.setupWithViewPager(binding.viewPager);
    }

    private void setupTabLayout() {
        Typeface typeface = FontUtils.selectTypeface(getContext(), FontUtils.STYLE_BOLD);
        binding.tabs.setTabGravity(TabLayout.GRAVITY_CENTER);
        binding.tabs.setTabMode(TabLayout.MODE_FIXED);
        binding.tabs.removeAllTabs();
        ViewGroup viewGroup = (ViewGroup) binding.tabs.getChildAt(0);
        PagerAdapter pagerAdapter = binding.viewPager.getAdapter();
        for (int i = 0, count = pagerAdapter.getCount(); i < count; i++) {
            TabLayout.Tab tab = binding.tabs.newTab();
            binding.tabs.addTab(tab.setText(pagerAdapter.getPageTitle(i)));
            AppCompatTextView view = (AppCompatTextView) ((ViewGroup) viewGroup.getChildAt(i)).getChildAt(1);
            view.setTypeface(typeface);
        }
    }

    @Override
    public <T extends Fragment> void onViewPagerFragmentLoaded(Class<T> clazz) {
        String title = fragmentTitleMap.get(clazz.getName());
        binding.toolbarLayout.toolbarTitle.setText(title);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.email_auth_button:
                startFragment(EmailAuthFragment.newInstance());
                break;
            case R.id.about_app_button:
                startMetaFragment(AboutFragment.newInstance());
                break;
            case R.id.privacy_policy_button:
                Intent privacyPolicyIntent = new Intent(Intent.ACTION_VIEW);
                privacyPolicyIntent.setData(Uri.parse(Constants.MA_PRIVACY_POLICY_URL));
                requireActivity().startActivityFromFragment(this, privacyPolicyIntent, 3000);
                break;
            case R.id.terms_app_button:
                Intent termsIntent = new Intent(Intent.ACTION_VIEW);
                termsIntent.setData(Uri.parse(Constants.MA_TERMS_URL));
                requireActivity().startActivityFromFragment(this, termsIntent, 3000);
                break;
            default:
        }
    }
}
