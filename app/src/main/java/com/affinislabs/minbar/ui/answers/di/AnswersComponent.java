package com.affinislabs.minbar.ui.answers.di;

import com.affinislabs.minbar.di.ScreenScoped;
import com.affinislabs.minbar.ui.answers.AnswersActivity;

import dagger.Subcomponent;

/**
 * minbar-v2
 * Michael Obi
 * 12/03/2018, 8:58 AM
 */

@Subcomponent
@ScreenScoped
public interface AnswersComponent {
    void inject(AnswersActivity activity);
}
