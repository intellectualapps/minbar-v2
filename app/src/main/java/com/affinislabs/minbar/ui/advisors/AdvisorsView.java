package com.affinislabs.minbar.ui.advisors;

import android.view.View;

import com.affinislabs.minbar.data.models.Mentor;
import com.affinislabs.minbar.ui.common.base.RemoteView;

import java.util.List;

/**
 * Created
 by felixunlimited
 on 1/29/18.
 */

interface AdvisorsView extends RemoteView {
    void showAdvisors(List<Mentor> advisors);

    void showMentees(List<Mentor> mentees);

    void handleAcceptAdvisorDecision(View advisorDecisionView, boolean accept);

    void handleDeclineAdvisorDecision(View advisorDecisionView, boolean decline);
}
