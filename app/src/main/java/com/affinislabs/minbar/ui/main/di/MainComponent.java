package com.affinislabs.minbar.ui.main.di;

import com.affinislabs.minbar.di.ScreenScoped;
import com.affinislabs.minbar.ui.main.MainActivity;

import dagger.Subcomponent;

/**
 * minbar-v2
 * Michael Obi
 * 11/03/2018, 4:41 PM
 */

@ScreenScoped
@Subcomponent
public interface MainComponent {
    void inject(MainActivity activity);
}
