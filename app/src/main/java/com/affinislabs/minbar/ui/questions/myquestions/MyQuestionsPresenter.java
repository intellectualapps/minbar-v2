package com.affinislabs.minbar.ui.questions.myquestions;

import com.affinislabs.minbar.data.MinbarDataRepository;
import com.affinislabs.minbar.ui.common.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * minbar-v2
 * Michael Obi
 * 05/04/2018, 9:25 PM
 */
public class MyQuestionsPresenter extends BasePresenter<MyQuestionsView> {
    private final MinbarDataRepository repository;

    @Inject
    public MyQuestionsPresenter(MinbarDataRepository repository) {
        this.repository = repository;
    }

    public void loadMyQuestions() {
        getView().showProgress();
        addDisposableSubscription(repository.getQuestionsUserAsked()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(questionsAsked -> {
                    getView().hideProgress();
                    getView().showMyQuestions(questionsAsked);
                }, error -> {
                    getView().hideProgress();
                    getView().showError(error.getMessage());
                }));
    }

    public void deleteMyQuestion(String questionId) {
        addDisposableSubscription(repository.deleteMyQuestion(questionId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> getView().showQuestionDeleted(),
                        error -> getView().showError(error.getMessage())
                ));
    }
}
