package com.affinislabs.minbar.ui.auth.registration;

import com.affinislabs.minbar.ui.common.base.RemoteView;

/**
 * minbar-v2
 * Michael Obi
 * 11 January 2018, 5:14 PM
 */

public interface RegistrationContract {
    public interface View extends RemoteView {
        void navigateToMainPage();

        void handleUsernameValidity(boolean isValid);
    }
}
