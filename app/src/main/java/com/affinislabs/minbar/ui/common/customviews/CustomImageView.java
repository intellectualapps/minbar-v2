package com.affinislabs.minbar.ui.common.customviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

/**
 * Created by felixunlimited on 1/25/18.
 */

public class CustomImageView extends AppCompatImageView {

    Paint mImagePaint;
    public CustomImageView(Context context) {
        super(context);
    }

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CustomImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void init (Context context, AttributeSet attrs) {
        mImagePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mImagePaint.setStyle(Paint.Style.FILL);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawCircle(0,0, 100f, mImagePaint);
    }
}
