package com.affinislabs.minbar.ui.threads;

import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.affinislabs.minbar.data.models.AnswerReply;
import com.affinislabs.minbar.databinding.AnswerReplyListItemBinding;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * minbar-v2
 * Michael Obi
 * 20/02/2018, 11:39 AM
 */

class ThreadAdapter extends RecyclerView.Adapter<ThreadAdapter.ViewHolder> {

    private List<AnswerReply> answerReplies = new ArrayList<>();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        AnswerReplyListItemBinding binding = AnswerReplyListItemBinding.inflate(inflater, parent,
                false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(answerReplies.get(position));
    }

    @Override
    public int getItemCount() {
        return answerReplies.size();
    }

    public void setItems(List<AnswerReply> answerReplies) {
        this.answerReplies = answerReplies;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        AnswerReplyListItemBinding binding;

        public ViewHolder(AnswerReplyListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(final AnswerReply answerReply) {
            binding.setAnswerReply(answerReply);
            binding.executePendingBindings();
            String time = DateUtils.getRelativeTimeSpanString(answerReply.getTime(),
                    GregorianCalendar.getInstance().getTimeInMillis(), DateUtils.MINUTE_IN_MILLIS).toString();
            binding.questionTimeView.setText(time);
        }
    }
}
