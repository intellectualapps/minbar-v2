package com.affinislabs.minbar.ui.becomementor;

import com.affinislabs.minbar.data.MinbarDataRepository;
import com.affinislabs.minbar.ui.common.base.BasePresenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * minbar-v2
 * Michael Obi
 * 12 January 2018, 5:13 PM
 */

class BecomeMentorPresenter extends BasePresenter<BecomeMentorView> {

    private MinbarDataRepository repository;

    @Inject
    BecomeMentorPresenter(MinbarDataRepository repository) {
        this.repository = repository;
    }

    void loadMentorCategories() {
        getView().showCategoryLoadingProgress();

        addDisposableSubscription(repository.getAllCategories()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(categories -> {
                            getView().hideCategoryLoadingProgress();
                            getView().showCategories(categories);
                        },
                        error -> {
                            getView().hideCategoryLoadingProgress();
                            getView().showError(error.getMessage());
                        }));
    }

    void saveMentorshipCategories(HashMap<String, Boolean> mentorCategorySelected) {
        getView().showProgress();
        List<String> selectedIds = new ArrayList<>();
        for (Map.Entry<String, Boolean> entry : mentorCategorySelected.entrySet()) {
            String key = entry.getKey();
            Boolean value = entry.getValue();
            if (value) {
                selectedIds.add(key);
            }
        }

        addDisposableSubscription(repository.saveMentorCategories(selectedIds)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(categories -> {
                            getView().hideProgress();
                            getView().navigateToNextPage();
                        },
                        error -> {
                            getView().hideProgress();
                            getView().showError(error.getMessage());
                        }));
    }
}
