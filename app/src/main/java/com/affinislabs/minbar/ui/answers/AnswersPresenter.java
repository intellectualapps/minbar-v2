package com.affinislabs.minbar.ui.answers;

import android.util.Log;

import com.affinislabs.minbar.data.MinbarDataRepository;
import com.affinislabs.minbar.data.models.Question;
import com.affinislabs.minbar.ui.common.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * minbar-v2
 * Michael Obi
 * 25/01/2018, 12:15 AM
 */

public class AnswersPresenter extends BasePresenter<AnswersView> {
    private static final String TAG = "AnswersPresenter";
    private MinbarDataRepository repository;

    @Inject
    public AnswersPresenter(MinbarDataRepository repository) {
        this.repository = repository;
    }


    public void loadAnswers(Question question) {
        getView().showProgress();

        addDisposableSubscription(repository.getAnswersForQuestion(question)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(answers -> {
                            getView().hideProgress();
                            getView().showAnswers(answers);
                        },
                        error -> {
                            getView().hideProgress();
                            getView().showError(error.getMessage());
                        }));
    }

    public void markAsRead(String questionId) {
        addDisposableSubscription(repository.markQuestionAsRead(questionId)
                .subscribeOn(Schedulers.io())
                .subscribe(() -> Log.d(TAG, String.format("Question marked as read: %s", questionId)),
                        error -> Log.d(TAG, String.format("Question mark as read failed: %s", questionId))));
    }
}
