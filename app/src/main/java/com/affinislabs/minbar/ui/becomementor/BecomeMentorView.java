package com.affinislabs.minbar.ui.becomementor;

import com.affinislabs.minbar.data.models.Category;
import com.affinislabs.minbar.ui.common.base.RemoteView;

import java.util.List;

/**
 * minbar-v2
 * Michael Obi
 * 22/01/2018, 3:14 PM
 */

public interface BecomeMentorView extends RemoteView {

    void showCategories(List<Category> categories);

    void navigateToNextPage();

    void showCategoryLoadingProgress();

    void hideCategoryLoadingProgress();

    void showCategoryLoadingError();
}
