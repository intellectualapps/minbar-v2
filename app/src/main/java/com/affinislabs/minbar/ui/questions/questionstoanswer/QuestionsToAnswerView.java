package com.affinislabs.minbar.ui.questions.questionstoanswer;

import com.affinislabs.minbar.data.models.Question;
import com.affinislabs.minbar.ui.common.base.RemoteView;

import java.util.List;

/**
 * minbar-v2
 * Michael Obi
 * 05/04/2018, 9:25 PM
 */
public interface QuestionsToAnswerView extends RemoteView {

    void showQuestionsToAnswer(List<Question> questionsToAnswer);

    void showDeclinedQuestion();
}