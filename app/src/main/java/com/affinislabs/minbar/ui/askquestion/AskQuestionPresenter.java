package com.affinislabs.minbar.ui.askquestion;

import android.util.Log;

import com.affinislabs.minbar.data.MinbarDataRepository;
import com.affinislabs.minbar.ui.common.base.BasePresenter;

import java.util.Set;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * minbar-v2
 * Michael Obi
 * 25/01/2018, 12:15 AM
 */

public class AskQuestionPresenter extends BasePresenter<AskQuestionView> {
    private static final String TAG = "AskQuestionPresenter";
    private MinbarDataRepository repository;

    @Inject
    public AskQuestionPresenter(MinbarDataRepository repository) {
        this.repository = repository;
    }

    public void loadMentorCategories() {
        getView().showCategoryLoadingProgress();

        addDisposableSubscription(repository.getAllCategories()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(categories -> {
                            getView().hideCategoryLoadingProgress();
                            getView().showCategories(categories);
                        },
                        error -> {
                            getView().hideCategoryLoadingProgress();
                            getView().showCategoryLoadingError();
                        }));
    }

    public void saveQuestion(String questionBody, Set<String> categoryIds) {
        getView().showProgress();
        addDisposableSubscription(repository.saveQuestion(questionBody, categoryIds)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(question -> {
                            getView().hideProgress();
                            getView().navigateToNextPage(question);
                        },
                        error -> {
                            getView().hideProgress();
                            getView().showError(error.getMessage());
                        },
                        () -> Log.d(TAG, "Question saved")));
    }
}
