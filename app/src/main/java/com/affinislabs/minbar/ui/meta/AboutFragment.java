package com.affinislabs.minbar.ui.meta;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.affinislabs.minbar.R;
import com.affinislabs.minbar.databinding.AboutFragmentBinding;
import com.affinislabs.minbar.ui.common.base.BaseFragment;


public class AboutFragment extends BaseFragment {

    private AboutFragmentBinding binding;

    public static Fragment newInstance() {
        return new AboutFragment();
    }

    public AboutFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_about, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.aboutToolbar.setNavigationIcon(R.drawable.ic_action_expand);
        binding.aboutToolbar.setNavigationOnClickListener(view1 -> getActivity().getSupportFragmentManager().popBackStack());
    }
}