package com.affinislabs.minbar.ui.profileedit;

import com.affinislabs.minbar.data.models.Category;

import java.util.List;
import java.util.Set;

/**
 * minbar-v2
 * Michael Obi
 * 28/02/2018, 4:18 PM
 */


public class UserProfileCategories {

    private List<Category> allCategories;
    private Set<String> userCategoryIds;

    public UserProfileCategories(List<Category> allCategories, Set<String> userCategoryIds) {
        this.allCategories = allCategories;
        this.userCategoryIds = userCategoryIds;
    }

    public List<Category> getAllCategories() {
        return allCategories;
    }

    public void setAllCategories(List<Category> allCategories) {
        this.allCategories = allCategories;
    }

    public Set<String> getUserCategoryIds() {
        return userCategoryIds;
    }

    public void setUserCategoryIds(Set<String> userCategoryIds) {
        this.userCategoryIds = userCategoryIds;
    }
}
