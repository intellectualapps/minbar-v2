package com.affinislabs.minbar.ui.answers;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.models.Answer;
import com.affinislabs.minbar.databinding.AnswerListItemBinding;
import com.affinislabs.minbar.utils.ImageLoader;

import java.util.List;


/**
 * minbar-v2
 * Michael Obi
 * 08/02/2018, 7:13 AM
 */

public class AnswersAdapter extends RecyclerView.Adapter<AnswersAdapter.ViewHolder> {

    private List<Answer> answers;
    private final AnswerClickHandler answerClickHandler;

    public AnswersAdapter(AnswerClickHandler answerClickHandler) {
        this.answerClickHandler = answerClickHandler;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        AnswerListItemBinding answerListItemBinding = AnswerListItemBinding.inflate(layoutInflater,
                parent, false);
        return new ViewHolder(answerListItemBinding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Answer answer = answers.get(position);
        holder.bind(answer);
        holder.itemView.setOnClickListener(v -> answerClickHandler.onAnswerClicked(answer));
    }

    @Override
    public int getItemCount() {
        return answers.size();
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        AnswerListItemBinding binding;

        public ViewHolder(AnswerListItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(final Answer answer) {
            binding.setAnswer(answer);
            binding.executePendingBindings();
            ImageLoader.loadImage(binding.userProfilePhoto, answer.getPhotoUrl(), R.drawable.ic_user);
        }
    }

    interface AnswerClickHandler {
        void onAnswerClicked(Answer answer);
    }
}
