package com.affinislabs.minbar.ui.advisors;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.affinislabs.minbar.MinbarApplication;
import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.models.Mentor;
import com.affinislabs.minbar.databinding.AdvisorsFragmentBinding;
import com.affinislabs.minbar.ui.common.customviews.CustomTextView;
import com.affinislabs.minbar.ui.profileview.ProfileViewActivity;
import com.affinislabs.minbar.utils.Constants;
import com.affinislabs.minbar.utils.FontUtils;
import com.affinislabs.minbar.utils.ToastUtil;

import java.util.List;

import javax.inject.Inject;

/**
 * Minbar
 * Michael Obi
 * 05 January 2018, 10:58 PM
 */

public class AdvisorsFragment extends Fragment implements View.OnClickListener, AdvisorsAdapter.AdvisorItemClickListener, AdvisorsView, AdvisorsAdapter.AdvisorDecisionItemClickListener {
    @Inject
    AdvisorsPresenter presenter;

    private AdvisorsFragmentBinding binding;
    private AdvisorsAdapter advisorsAdapter, menteesAdapter;

    public AdvisorsFragment() {
    }

    public static AdvisorsFragment newInstance() {
        return new AdvisorsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MinbarApplication) getActivity().getApplication())
                .getAppComponent()
                .advisorsComponent()
                .inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        presenter.attachView(this);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_advisors, container, false);
        getActivity().setTitle(getString(R.string.advisors_label));
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViewPager();
        setupTabLayout();
        presenter.loadAdvisors();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
    }

    public void setUpMenteesAdapters(List<Mentor> mentees) {
        menteesAdapter = new AdvisorsAdapter(getContext(), Constants.MENTEES, this, this);
        LinearLayoutManager linearLayoutManagerAsked = new LinearLayoutManager(getContext());
        linearLayoutManagerAsked.setOrientation(LinearLayoutManager.VERTICAL);
        menteesAdapter.setItems(mentees);
        binding.menteesRecyclerView.setAdapter(menteesAdapter);
        binding.menteesRecyclerView.setLayoutManager(linearLayoutManagerAsked);
        binding.menteesRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        binding.menteesRecyclerView.setEmptyView(binding.emptyViewTheirs);
    }

    public void setUpAdvisorsAdapters(List<Mentor> advisors) {
        advisorsAdapter = new AdvisorsAdapter(getContext(), Constants.ADVISORS, this, this);
        LinearLayoutManager linearLayoutManagerAsked = new LinearLayoutManager(getContext());
        linearLayoutManagerAsked.setOrientation(LinearLayoutManager.VERTICAL);
        advisorsAdapter.setItems(advisors);
        binding.advisorsRecyclerView.setAdapter(advisorsAdapter);
        binding.advisorsRecyclerView.setLayoutManager(linearLayoutManagerAsked);
        binding.advisorsRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
        binding.advisorsRecyclerView.setEmptyView(binding.emptyViewYours);
    }

    private void setupViewPager() {
        AdvisorsPagerAdapter advisorPagerAdapter = new AdvisorsPagerAdapter();
        advisorPagerAdapter.addView(binding.theirsView);
        advisorPagerAdapter.addView(binding.yoursView);
        binding.viewPager.setAdapter(advisorPagerAdapter);
    }

    private void setupTabLayout() {
        Typeface typeface = FontUtils.selectTypeface(getContext(), FontUtils.STYLE_BOLD);
        binding.tabs.setTabGravity(TabLayout.GRAVITY_CENTER);
        binding.tabs.setTabMode(TabLayout.MODE_FIXED);
        binding.tabs.setupWithViewPager(binding.viewPager);
        binding.tabs.removeAllTabs();
        ViewGroup viewGroup = (ViewGroup) binding.tabs.getChildAt(0);

        TabLayout.Tab theirsTab = binding.tabs.newTab();
        binding.tabs.addTab(theirsTab.setCustomView(R.layout.theirs_custom_tab));
        CustomTextView theirsLabel = binding.getRoot().findViewById(R.id.theirs_tab);
        theirsLabel.setTypeface(typeface);

        TabLayout.Tab yoursTab = binding.tabs.newTab();
        binding.tabs.addTab(yoursTab.setText(R.string.advisors_tab_label));
        AppCompatTextView yoursView = (AppCompatTextView) ((ViewGroup) viewGroup.getChildAt(1)).getChildAt(1);
        yoursView.setTypeface(typeface);
        yoursView.setTextSize(getResources().getDimension(R.dimen.tab_text_size));

        binding.tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab == theirsTab) {
                    theirsLabel.setTextColor(getResources().getColor(R.color.colorBlack));
                    yoursView.setTextColor(getResources().getColor(R.color.colorDarkGray));
                } else {
                    theirsLabel.setTextColor(getResources().getColor(R.color.colorDarkGray));
                    yoursView.setTextColor(getResources().getColor(R.color.colorBlack));
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
        }
    }

    @Override
    public void showProgress() {
        binding.loadingProgress.setVisibility(View.VISIBLE);
        binding.viewPager.setVisibility(View.GONE);
    }

    @Override
    public void hideProgress() {
        binding.loadingProgress.setVisibility(View.GONE);
    }

    @Override
    public void showError(String errorMessage) {
        ToastUtil.showToast(getActivity(), errorMessage, Toast.LENGTH_LONG);
    }

    @Override
    public void showAdvisors(List<Mentor> advisors) {
        hideProgress();
        if (advisors != null) {
            binding.viewPager.setVisibility(View.VISIBLE);
            setUpAdvisorsAdapters(advisors);
            if (advisors.size() > 0) {
                binding.advisorsSearchView.setVisibility(View.VISIBLE);
            }
        } else {
            ToastUtil.showToast(getActivity(), Constants.NULL_RESPONSE, Toast.LENGTH_LONG);
        }
    }

    @Override
    public void showMentees(List<Mentor> mentees) {
        hideProgress();
        if (mentees != null) {
            binding.viewPager.setVisibility(View.VISIBLE);
            setUpMenteesAdapters(mentees);
            CustomTextView menteeRequestsCount = binding.getRoot().findViewById(R.id.mentee_requests_count);
            if (mentees.size() > 0) {
                menteeRequestsCount.setVisibility(View.VISIBLE);
                menteeRequestsCount.setText(String.valueOf(mentees.size()));
                binding.menteesSearchView.setVisibility(View.VISIBLE);
            } else {
                menteeRequestsCount.setVisibility(View.GONE);
            }
        } else {
            ToastUtil.showToast(getActivity(), Constants.NULL_RESPONSE, Toast.LENGTH_LONG);
        }
    }

    @Override
    public void handleAcceptAdvisorDecision(View advisorDecisionView, boolean acceptDecision) {
        if (acceptDecision) {
            advisorDecisionView.setVisibility(View.GONE);
        } else {
            ToastUtil.showToast(getActivity(), Constants.ACCEPT_ERROR, Toast.LENGTH_LONG);
        }
    }

    @Override
    public void handleDeclineAdvisorDecision(View advisorDecisionView, boolean declineDecision) {
        if (declineDecision) {
            advisorDecisionView.setVisibility(View.GONE);
        } else {
            ToastUtil.showToast(getActivity(), Constants.DECLINE_ERROR, Toast.LENGTH_LONG);
        }
    }

    @Override
    public void onAdvisorItemClicked(Mentor advisor) {
        Intent profileViewIntent = new Intent(getActivity(), ProfileViewActivity.class);
        profileViewIntent.putExtra(Constants.SECOND_PARTY_USERNAME, advisor.getUsername());
        startActivity(profileViewIntent);
    }

    @Override
    public void onAdvisorDecisionItemClicked(View advisorDecisionView, String decision, Mentor advisor) {
        presenter.decideMenteeRequest(advisorDecisionView, decision, advisor.getUsername());
    }
}
