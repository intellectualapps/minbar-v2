package com.affinislabs.minbar.ui.answers;

import com.affinislabs.minbar.data.models.Answer;
import com.affinislabs.minbar.ui.common.base.RemoteView;

import java.util.List;

/**
 * minbar-v2
 * Michael Obi
 * 09/02/2018, 4:13 AM
 */

interface AnswersView extends RemoteView {

    void showAnswers(List<Answer> answers);
}
