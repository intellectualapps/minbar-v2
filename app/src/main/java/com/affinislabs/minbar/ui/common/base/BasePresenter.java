package com.affinislabs.minbar.ui.common.base;

import android.support.annotation.NonNull;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * minbar-v2
 * Michael Obi
 * 16 January 2018, 5:02 PM
 */

public abstract class BasePresenter<V extends View> {

    private V view;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public final void attachView(@NonNull V view) {
        this.view = view;
    }

    public final void detachView() {
        compositeDisposable.clear();
        view = null;
    }

    protected final boolean isViewAttached() {
        return view != null;
    }

    protected V getView() {
        if (!isViewAttached()) {
            throw new IllegalStateException("View not attached to presenter. Call Presenter.attachView()");
        }

        return view;
    }

    protected void addDisposableSubscription(Disposable disposable) {
        this.compositeDisposable.add(disposable);
    }
}
