package com.affinislabs.minbar.ui.main;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.minbar.MinbarApplication;
import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.databinding.MainActivityBinding;
import com.affinislabs.minbar.ui.advisors.AdvisorsFragment;
import com.affinislabs.minbar.ui.askquestion.AskQuestionFragment;
import com.affinislabs.minbar.ui.auth.AuthActivity;
import com.affinislabs.minbar.ui.common.customviews.CustomTextView;
import com.affinislabs.minbar.ui.meta.AboutFragment;
import com.affinislabs.minbar.ui.profileedit.ProfileEditFragment;
import com.affinislabs.minbar.ui.profileedit.ProfileUpdatedListener;
import com.affinislabs.minbar.ui.questions.QuestionsFragment;
import com.affinislabs.minbar.utils.Constants;
import com.affinislabs.minbar.utils.CustomTabHelper;
import com.affinislabs.minbar.utils.ImageLoader;

import javax.inject.Inject;

/**
 * minbar-v2
 * Michael Obi
 * 14 January 2018, 8:36 AM
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener, MainView,
        ProfileUpdatedListener {
    @Inject
    MainPresenter presenter;

    private MainActivityBinding binding;
    private Toolbar toolbar;
    private ImageView mUserProfilePhoto, mVerifiedIcon;
    private TextView mFullNameView, mUsernameView;
    private TextView mPrivacyNav;
    private View mAskQuestionNav, mOpenQuestionsNav, mTermsNav, mAdvisorsNav, mLogoutNav, mHelpNav, mProfileNav, mAboutAppNav;
    private View userSidebarContainer;
    private String currentFragmentTag;
    private Fragment fragmentToSet = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MinbarApplication) getApplication())
                .getAppComponent()
                .mainComponent()
                .inject(this);
        presenter.attachView(this);
        presenter.checkLoginStatus();

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu_white);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, binding.drawerLayout,
                toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                InputMethodManager imm =
                        (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0);

            }
        };
        binding.drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        binding.drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (fragmentToSet != null) {
                    loadFragment(fragmentToSet, R.id.container);
                    fragmentToSet = null;
                }
            }
        });

        View customNavLayout = binding.navView.inflateHeaderView(R.layout.nav_sidebar_layout);
        userSidebarContainer = customNavLayout.findViewById(R.id.nav_sidebar_header_container);
        mFullNameView = customNavLayout.findViewById(R.id.fullname);
        mVerifiedIcon = customNavLayout.findViewById(R.id.verified_icon);
        mUsernameView = customNavLayout.findViewById(R.id.username);
        mUserProfilePhoto = customNavLayout.findViewById(R.id.profile_photo);

        mAskQuestionNav = customNavLayout.findViewById(R.id.nav_ask_question);
        mOpenQuestionsNav = customNavLayout.findViewById(R.id.nav_open_questions);
        mAdvisorsNav = customNavLayout.findViewById(R.id.nav_advisors);
        mProfileNav = customNavLayout.findViewById(R.id.nav_profile);
        mLogoutNav = customNavLayout.findViewById(R.id.nav_logout);

        mAboutAppNav = customNavLayout.findViewById(R.id.nav_about);
        mHelpNav = customNavLayout.findViewById(R.id.nav_help);
        mTermsNav = customNavLayout.findViewById(R.id.nav_terms);
        mPrivacyNav = customNavLayout.findViewById(R.id.nav_privacy);

        userSidebarContainer.setOnClickListener(this);

        mAskQuestionNav.setOnClickListener(MainActivity.this);
        mOpenQuestionsNav.setOnClickListener(MainActivity.this);
        mAdvisorsNav.setOnClickListener(MainActivity.this);
        mProfileNav.setOnClickListener(MainActivity.this);
        mLogoutNav.setOnClickListener(MainActivity.this);

        mAboutAppNav.setOnClickListener(MainActivity.this);
        mHelpNav.setOnClickListener(MainActivity.this);
        mTermsNav.setOnClickListener(MainActivity.this);
        mPrivacyNav.setOnClickListener(MainActivity.this);

        String action = getIntent().getAction();
        if (action != null && action.equals(Constants.NEW_ADVISOR_ACTION)) {
            loadFragment(AdvisorsFragment.newInstance(), R.id.container);
        } else {
            loadFragment(AskQuestionFragment.newInstance(), R.id.container);
        }

        presenter.loadProfile();
        presenter.loadOpenQuestionCount();
    }

    private void loadFragment(Fragment fragment, int containerId) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(containerId, fragment, fragment.getClass().getSimpleName());
        transaction.commit();
        fragmentManager.executePendingTransactions();
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle("");
        CustomTextView toolbarTitle = getToolbar().findViewById(R.id.toolbar_title);
        toolbarTitle.setText(title);
    }

    public Toolbar getToolbar() {
        if (toolbar == null) {
            toolbar = findViewById(R.id.toolbar);
        }
        return toolbar;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.nav_sidebar_header_container) {
            navigate(R.id.nav_profile);
        } else {
            navigate(view.getId());
        }
        binding.drawerLayout.closeDrawer(GravityCompat.START);
    }

    private void navigate(@IdRes int id) {
        switch (id) {
            case R.id.nav_ask_question:
                fragmentToSet = AskQuestionFragment.newInstance();
                break;
            case R.id.nav_open_questions:
                fragmentToSet = QuestionsFragment.newInstance();
                break;
            case R.id.nav_advisors:
                fragmentToSet = AdvisorsFragment.newInstance();
                break;
            case R.id.nav_profile:
                fragmentToSet = ProfileEditFragment.newInstance();
                break;
            case R.id.nav_logout:
                presenter.logout();
                break;
            case R.id.nav_about:
                fragmentToSet = AboutFragment.newInstance();
                break;
            case R.id.nav_terms:
                CustomTabHelper.openUrl(this, Constants.MA_TERMS_URL);
                break;
            case R.id.nav_privacy:
                CustomTabHelper.openUrl(this, Constants.MA_PRIVACY_POLICY_URL);
                break;
            default:
        }
    }

    public void navigateHome() {
        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        loadFragment(AskQuestionFragment.newInstance(), R.id.container);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (currentFragmentTag != null) {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(currentFragmentTag);
            if (fragment != null) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        currentFragmentTag = fragment.getClass().getSimpleName();
    }

    @Override
    public void navigateToLogin() {
        Intent i = new Intent(this, AuthActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }

    public void showProfileData(User user) {
        mUsernameView.setText(String.format("@%s", user.getUsername()));
        mFullNameView.setText(user.getFullName());
        ImageLoader.loadImage(mUserProfilePhoto, user.getPhotoUrl(), R.drawable.ic_user);
    }

    @Override
    public void showOpenQuestionCount(Integer count) {
        if (count > 0) {
            TextView tvOpenQuestionsCount = findViewById(R.id.open_questions_count);
            tvOpenQuestionsCount.setText(count.toString());
            tvOpenQuestionsCount.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onProfileUpdated() {
        presenter.loadProfile();
    }
}
