package com.affinislabs.minbar.ui.common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.affinislabs.minbar.R;


/**
 * minbar-v2
 * Michael Obi
 * 05/04/2018, 8:58 AM
 */
public abstract class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback {

    private int backgroundColor = Color.parseColor("#f44336");
    private Drawable deleteIcon;
    private Paint clearPaint;
    private ColorDrawable background = new ColorDrawable();


    public SwipeToDeleteCallback(Context context) {
        this(context, ItemTouchHelper.LEFT);
    }

    public SwipeToDeleteCallback(Context context, int swipeDirections) {
        super(0, swipeDirections);
        deleteIcon = ContextCompat.getDrawable(context, R.drawable.ic_delete);
        clearPaint = new Paint();
        clearPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        View itemView = viewHolder.itemView;
        int itemHeight = itemView.getBottom() - itemView.getTop();
        boolean isCanceled = dX == 0f && !isCurrentlyActive;
        if (isCanceled) {
            clearCanvas(c, ((float) itemView.getRight() + dX), ((float) itemView.getTop()),
                    ((float) itemView.getRight()), ((float) itemView.getBottom()));
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            return;
        }


        int deleteIconTop = itemView.getTop() + (itemHeight - deleteIcon.getIntrinsicHeight()) / 2;
        int deleteIconMargin = (itemHeight - deleteIcon.getIntrinsicHeight()) / 2;
        int deleteIconBottom = deleteIconTop + deleteIcon.getIntrinsicHeight();
        int deleteIconLeft;
        int deleteIconRight;

        background.setColor(backgroundColor);
        if (getSwipeDirs(recyclerView, viewHolder) == ItemTouchHelper.LEFT) {
            background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(),
                    itemView.getBottom());
            deleteIconLeft = itemView.getRight() - deleteIconMargin - deleteIcon.getIntrinsicWidth();
            deleteIconRight = itemView.getRight() - deleteIconMargin;
        } else {
            background.setBounds(itemView.getLeft(), itemView.getTop(), itemView.getRight() +
                    (int) dX, itemView.getBottom());
            deleteIconLeft = itemView.getLeft() + deleteIconMargin;
            deleteIconRight = itemView.getLeft() + deleteIconMargin + deleteIcon.getIntrinsicWidth();
        }

        background.draw(c);
        deleteIcon.setBounds(deleteIconLeft, deleteIconTop, deleteIconRight, deleteIconBottom);
        deleteIcon.draw(c);
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }

    private void clearCanvas(Canvas c, Float left, Float top, Float right, Float bottom) {
        c.drawRect(left, top, right, bottom, clearPaint);
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        return super.getMovementFlags(recyclerView, viewHolder);
    }
}
