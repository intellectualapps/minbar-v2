package com.affinislabs.minbar.ui.advisors;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.models.Mentor;
import com.affinislabs.minbar.ui.common.customviews.CustomButton;
import com.affinislabs.minbar.ui.common.customviews.CustomTextView;
import com.affinislabs.minbar.utils.Constants;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created
 by felixunlimited
 on 1/25/18.
 */

public class AdvisorsAdapter extends RecyclerView.Adapter<AdvisorsAdapter.ViewHolder> {

    private List<Mentor> mentors;
    private Context context;
    private AdvisorItemClickListener advisorItemClickListener;
    private AdvisorDecisionItemClickListener advisorDecisionItemClickListener;
    private String flag;

    AdvisorsAdapter(Context context, String flag, AdvisorItemClickListener advisorItemClickListener, AdvisorDecisionItemClickListener advisorDecisionItemClickListener) {
        this.context = context;
        this.advisorItemClickListener = advisorItemClickListener;
        this.advisorDecisionItemClickListener = advisorDecisionItemClickListener;
        this.flag = flag;
    }
    
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.advisor_item_layout,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final Mentor advisor = mentors.get(position);
        viewHolder.advisorFullName.setText(advisor.getFullName());
        viewHolder.advisorShortBio.setText(context.getString(R.string.user_short_bio_placeholder));
        if (flag.equals(Constants.ADVISORS) && advisor.getStatus().equals(Constants.NOT_CONNECTED)) {
            viewHolder.advisorShortBio.setVisibility(View.GONE);
            viewHolder.advisorPending.setVisibility(View.VISIBLE);
        } else if (flag.equals(Constants.MENTEES) && advisor.getStatus().equals(Constants.NOT_CONNECTED)) {
            viewHolder.advisorShortBio.setVisibility(View.GONE);
            viewHolder.advisorDecision.setVisibility(View.VISIBLE);
        }
        viewHolder.advisorFullName.setOnClickListener(view -> advisorItemClickListener.onAdvisorItemClicked(advisor));
        viewHolder.acceptAdvisorBtn.setOnClickListener(view -> advisorDecisionItemClickListener.onAdvisorDecisionItemClicked(viewHolder.advisorDecision,Constants.ACCEPT, advisor));
        viewHolder.declineAdvisorBtn.setOnClickListener(view -> advisorDecisionItemClickListener.onAdvisorDecisionItemClicked(viewHolder.advisorDecision,Constants.DECLINE, advisor));
    }

    @Override
    public int getItemCount() {
        return mentors.size();
    }

    void setItems(List<Mentor> items) {
        this.mentors = items;
        notifyDataSetChanged();
    }

    public interface AdvisorItemClickListener {
        void onAdvisorItemClicked(Mentor Advisor);
    }

    public interface AdvisorDecisionItemClickListener {
        void onAdvisorDecisionItemClicked(View advisorDecisionView, String decision, Mentor Advisor);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        View itemView, advisorDecision;
        CustomTextView advisorFullName, advisorShortBio, advisorPending;
        CircleImageView advisorProfilePic;
        ImageView advisorVerifiedIcon;
        CustomButton acceptAdvisorBtn, declineAdvisorBtn;

        ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            advisorFullName = itemView.findViewById(R.id.advisor_full_name);
            advisorProfilePic = itemView.findViewById(R.id.user_profile_photo);
            advisorDecision = itemView.findViewById(R.id.advisor_decision);
            advisorShortBio = itemView.findViewById(R.id.advisor_short_bio);
            advisorVerifiedIcon = itemView.findViewById(R.id.advisor_verified_icon);
            acceptAdvisorBtn = itemView.findViewById(R.id.accept_advisor);
            declineAdvisorBtn = itemView.findViewById(R.id.decline_advisor);
            advisorPending = itemView.findViewById(R.id.advisor_pending);
        }
    }
}
