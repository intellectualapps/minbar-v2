package com.affinislabs.minbar.ui.askquestion.di;

import com.affinislabs.minbar.di.ScreenScoped;
import com.affinislabs.minbar.ui.askquestion.AskQuestionFragment;

import dagger.Subcomponent;

/**
 * minbar-v2
 * Michael Obi
 * 11/03/2018, 4:44 PM
 */

@ScreenScoped
@Subcomponent
public interface AskQuestionComponent {
    void inject(AskQuestionFragment fragment);
}
