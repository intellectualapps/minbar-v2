package com.affinislabs.minbar.ui.threads;

import android.util.Log;

import com.affinislabs.minbar.data.MinbarDataRepository;
import com.affinislabs.minbar.data.models.Answer;
import com.affinislabs.minbar.data.models.Question;
import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.ui.common.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * minbar-v2
 * Michael Obi
 * 15/02/2018, 11:26 AM
 */

class ThreadPresenter extends BasePresenter<ThreadView> {
    private static final String TAG = "ThreadPresenter";
    private MinbarDataRepository dataRepository;

    @Inject
    ThreadPresenter(MinbarDataRepository dataRepository) {
        this.dataRepository = dataRepository;
    }

    void loadReplies(Answer answer) {
        getView().showProgress();
        addDisposableSubscription(dataRepository.observeAnswerThread(answer.getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(answerReplies -> getView().showAnswerReplies(answerReplies),
                        throwable -> Log.e(TAG, throwable.getMessage(), throwable)));
    }

    void sendReply(String text, Answer answerToReply, User author) {
        getView().sendingReplyProgress();
        addDisposableSubscription(dataRepository.addAnswerReply(answerToReply.getId(), text, author
                .getUsername()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> getView().finishedSendingReply()));
    }

    void sendInitialAnswer(String answerText, Question question) {
        getView().sendingReplyProgress();
        addDisposableSubscription(dataRepository.saveQuestionAnswer(answerText, question)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(answer -> {
                    getView().finishedSendingReply();
                    answer.setQuestion(question);
                    getView().showInitialAnswer(answer);
                }, throwable -> {
                    getView().showSaveAnswerError();
                }));
    }

    void doneWithAnswer(Answer answer) {
        getView().showMarkAsDoneProgress();
        addDisposableSubscription(dataRepository.doneWithAnswer(answer)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> getView().doneWithAnswer(),
                        throwable -> {
                            getView().showError(throwable.getMessage());
                            getView().hideMarkAsDoneProgress();
                            getView().showDoneButton();
                        }));
    }


    void happyWithAnswer(Answer answer) {
        getView().showMarkAsDoneProgress();
        addDisposableSubscription(dataRepository.happyWithAnswer(answer)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> getView().doneWithAnswer(),
                        throwable -> {
                            getView().showError(throwable.getMessage());
                            getView().hideMarkAsDoneProgress();
                            getView().showDoneButton();
                        }));
    }

    public void rateMentor(String username, float rating) {
        getView().hideRatingBar();
        addDisposableSubscription(dataRepository.rateMentor(username, rating)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> Log.i(TAG, "Finished Rating User"),
                        throwable -> {
                            getView().showError(throwable.getMessage());
                            getView().showRatingBar();
                        }));
    }

    public void inviteAdvisor(String mentorUsername) {
        addDisposableSubscription(dataRepository.inviteAdvisor(mentorUsername)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    getView().hideProgress();
                    getView().hideInviteAdvisorButton();
                    getView().inviteAdvisorSuccess();
                }, error -> {
                    getView().hideProgress();
                    getView().showError(error.getMessage());
                }));

    }
}
