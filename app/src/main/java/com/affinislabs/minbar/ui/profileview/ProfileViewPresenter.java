package com.affinislabs.minbar.ui.profileview;

import com.affinislabs.minbar.data.MinbarDataRepository;
import com.affinislabs.minbar.data.models.Question;
import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.ui.common.base.BasePresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created
 * by User
 * on 2/23/2018.
 */

public class ProfileViewPresenter extends BasePresenter<ProfileView> {
    private MinbarDataRepository repository;

    @Inject
    public ProfileViewPresenter(MinbarDataRepository repository) {
        this.repository = repository;
    }

    void loadConversations() {
        getView().showProgress();
        addDisposableSubscription(repository.getQuestionsUserAsked()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(questionsAsked -> {
                    List<Question> questions = questionsAsked;
                    addDisposableSubscription(repository.getQuestionsUserAnswer()
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(questionsAnswer -> {
                                questions.addAll(questionsAnswer);
                                getView().hideProgress();
                                getView().showConversations(questions);
                            }, error -> {
                                getView().hideProgress();
                                getView().showError(error.getMessage());
                            }));
                }, error -> {
                    getView().hideProgress();
                    getView().showError(error.getMessage());
                }));
    }

    void loadProfile(String secondPartyUsername) {
        getView().showProgress();
        Single<User> getSecondPartyPublicProfileObservable =
                repository.getSecondPartyPublicProfile(secondPartyUsername);
        addDisposableSubscription(getSecondPartyPublicProfileObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(user -> {
                            getView().hideProgress();
                            getView().showSecondPartyPublicProfile(user);
                        },
                        error -> {
                            getView().hideProgress();
                            getView().showError(error.getMessage());
                        }));
    }

    void sendAdvisorInvitation(String advisorUsername) {
        getView().showProgress();
        addDisposableSubscription(repository.inviteAdvisor(advisorUsername)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    getView().hideProgress();
                    getView().finishInvitingAdvisor();
                }, error -> {
                    getView().hideProgress();
                    getView().showError(error.getMessage());
                }));
    }
}
