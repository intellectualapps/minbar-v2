package com.affinislabs.minbar.ui.auth.registration;

import com.affinislabs.minbar.domain.auth.AuthManager;
import com.affinislabs.minbar.ui.common.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * minbar-v2
 * Michael Obi
 * 12 January 2018, 5:13 PM
 */

public class RegistrationPresenter extends BasePresenter<RegistrationContract.View>
        implements AuthManager.AuthListener {

    private AuthManager authManager;

    @Inject
    public RegistrationPresenter(AuthManager authManager) {
        this.authManager = authManager;
    }

    public void onRegisterButtonClicked(String username, String email, String password) {
        if (!isViewAttached()) {
            return;
        }
        getView().showProgress();
        authManager.register(username, email, password, this);
    }

    @Override
    public void onAuthSuccess() {
        getView().hideProgress();
        if (authManager.isUserLoggedIn()) {
            getView().navigateToMainPage();
        }
    }

    @Override
    public void onAuthError(String errorMessage) {
        getView().hideProgress();
        getView().showError(errorMessage);
    }

    public void validateUsername(String username) {
        addDisposableSubscription(authManager.checkUsernameValidity(username)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(status -> getView().handleUsernameValidity(status),
                        throwable -> getView().showError(throwable.getMessage())));
    }
}
