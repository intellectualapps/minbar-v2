package com.affinislabs.minbar.ui.questions.questionstoanswer;

import com.affinislabs.minbar.data.MinbarDataRepository;
import com.affinislabs.minbar.ui.common.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * minbar-v2
 * Michael Obi
 * 05/04/2018, 9:25 PM
 */
public class QuestionsToAnswerPresenter extends BasePresenter<QuestionsToAnswerView> {
    private final MinbarDataRepository repository;

    @Inject
    public QuestionsToAnswerPresenter(MinbarDataRepository repository) {
        this.repository = repository;
    }

    public void loadQuestionsToAnswer() {
        getView().showProgress();
        addDisposableSubscription(repository.getQuestionsUserAnswer()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(questionsAsked -> {
                    getView().hideProgress();
                    getView().showQuestionsToAnswer(questionsAsked);
                }, error -> {
                    getView().hideProgress();
                    getView().showError(error.getMessage());
                }));
    }

    public void deleteMentorQuestion(String questionId) {
        addDisposableSubscription(repository.deleteMentorQuestion(questionId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> getView().showDeclinedQuestion(),
                        error -> getView().showError(error.getMessage())
                ));
    }
}
