package com.affinislabs.minbar.ui.profileedit;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.affinislabs.minbar.MinbarApplication;
import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.models.Category;
import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.databinding.ProfileFragmentBinding;
import com.affinislabs.minbar.ui.becomementor.MentorCategoriesAdapter;
import com.affinislabs.minbar.ui.common.GridSpacingItemDecoration;
import com.affinislabs.minbar.utils.ImageLoader;
import com.affinislabs.minbar.utils.ToastUtil;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import io.reactivex.processors.FlowableProcessor;
import io.reactivex.processors.PublishProcessor;

import static android.app.Activity.RESULT_OK;


public class ProfileEditFragment extends Fragment implements View.OnClickListener, MentorCategoriesAdapter.CategoryItemClickListener, ProfileView {
    private static final String TAG = "ProfileEditFragment";

    @Inject
    ProfilePresenter presenter;

    FlowableProcessor<Set<String>> selectedCategoriesChangeObservable = PublishProcessor.create();

    CallbackManager faceboookCallbackManager;
    ProfileUpdatedListener profileUpdatedListener;
    private ProfileFragmentBinding binding;
    private MentorCategoriesAdapter adapter;
    private ProgressDialog progressDialog;
    private Set<String> selectedCategories = new HashSet<>();

    public ProfileEditFragment() {
    }

    public static Fragment newInstance() {
        return new ProfileEditFragment();
    }

    @Override
    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
        }

        progressDialog.setMessage(getString(R.string.saving_profile));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog == null) return;
        progressDialog.dismiss();
    }

    @Override
    public void showError(String errorMessage) {
        ToastUtil.showToast(getActivity(), errorMessage, Toast.LENGTH_SHORT);
    }

    @Override
    public void showCategories(List<Category> categories, Set<String> userCategoryIds) {
        binding.recyclerviewMentorCategories.setVisibility(View.VISIBLE);
        selectedCategories.addAll(userCategoryIds);
        adapter.setMentorCategoriesList(categories, selectedCategories);
    }

    @Override
    public void showProfileData(User user) {
        binding.fullName.setText(user.getFullName());
        binding.username.setText(String.format("@%s", user.getUsername()));
        binding.emailAddressInput.setText(user.getEmail());
        binding.usernameInput.setText(user.getUsername());
        binding.firstNameInput.setText(user.getFirstName());
        binding.lastNameInput.setText(user.getLastName());
        binding.phoneNumberInput.setText(user.getPhoneNumber());
        binding.locationInput.setText(user.getLocation());
        binding.longBioInput.setText(user.getLongBio());
        ImageLoader.loadImage(binding.profilePhoto, user.getPhotoUrl(), R.drawable.ic_user);
        if (user.getFacebookProfile() != null && !user.getFacebookProfile().isEmpty()) {
            binding.btnConnectFacebook.setText(getString(R.string.disconnect));
            binding.btnConnectFacebook.setOnClickListener(v -> presenter.disconnectFacebook());
        } else {
            binding.btnConnectFacebook.setText(getString(R.string.connect_label));
            binding.btnConnectFacebook.setOnClickListener(v -> loginWithFacebook());
        }

        if (user.getLinkedinProfile() != null && !user.getLinkedinProfile().isEmpty()) {
            binding.btnConnectLinkedin.setText(getString(R.string.disconnect));
        } else {
            binding.btnConnectLinkedin.setText(getString(R.string.connect_label));
        }

        binding.profilePhoto.setOnClickListener(v -> CropImage.activity()
                .setCropShape(CropImageView.CropShape.OVAL)
                .setAspectRatio(1, 1)
                .start(getContext(), this));
    }

    @Override
    public void showCategoryLoadingProgress() {
        binding.categoryLoadingProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideCategoryLoadingProgress() {
        binding.categoryLoadingErrorContainer.setVisibility(View.GONE);
        binding.categoryLoadingProgress.setVisibility(View.GONE);
    }

    @Override
    public void showCategoryLoadingError() {
        binding.categoryLoadingErrorContainer.setVisibility(View.VISIBLE);
        binding.categoryLoadingRetryBtn.setOnClickListener(v -> {
            binding.categoryLoadingErrorContainer.setVisibility(View.GONE);
            presenter.loadMentorCategories();
        });
    }

    @Override
    public void showSaveSuccess() {
        Toast.makeText(getActivity(), getString(R.string.profile_update_success), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showFacebookConnectSuccess() {
        presenter.loadProfile();
        Toast.makeText(getActivity(), R.string.facebook_connect_success, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showFacebookDisconnectSuccess() {
        presenter.loadProfile();
        Toast.makeText(getActivity(), R.string.facebook_disconnect_success, Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    public void showImageUploadSuccess() {
        if (profileUpdatedListener != null) {
            profileUpdatedListener.onProfileUpdated();
        }
        Toast.makeText(requireActivity(), R.string.profile_photo_changed, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showImageUploadProgress() {
        Toast.makeText(getActivity(), R.string.uploading, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCategoryItemClicked(String categoryId, boolean isSelected) {
        if (isSelected) {
            selectedCategories.add(categoryId);
        } else {
            selectedCategories.remove(categoryId);
        }
        selectedCategoriesChangeObservable.onNext(selectedCategories);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save_changes:
                saveProfile();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (faceboookCallbackManager != null) {
            faceboookCallbackManager.onActivityResult(requestCode, resultCode, data);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                presenter.uploadProfilePicture(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Log.d(TAG, error.getMessage(), error);
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((MinbarApplication) requireActivity().getApplication())
                .getAppComponent()
                .profileEditComponent()
                .inject(this);

        if (requireActivity() instanceof ProfileUpdatedListener) {
            profileUpdatedListener = (ProfileUpdatedListener) requireActivity();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_profile, container, false);
        binding.recyclerviewMentorCategories.addItemDecoration(new GridSpacingItemDecoration(2, 30, false));
        binding.recyclerviewMentorCategories.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        adapter = new MentorCategoriesAdapter(this);
        binding.recyclerviewMentorCategories.setAdapter(adapter);
        getActivity().setTitle(getString(R.string.edit_profile));
        binding.btnSaveChanges.setOnClickListener(this);

        binding.longBioInput.setOnTouchListener((view, event) -> {
            view.getParent().requestDisallowInterceptTouchEvent(true);
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_UP:
                    view.getParent().requestDisallowInterceptTouchEvent(false);
                    break;
            }
            return false;
        });
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.attachView(this);
        presenter.loadProfile();
        presenter.loadMentorCategories();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
    }

    private void saveProfile() {
        String firstName = binding.firstNameInput.getText().toString().trim();
        String lastName = binding.lastNameInput.getText().toString().trim();
        String phoneNumber = binding.phoneNumberInput.getText().toString().trim();
        String emailAddress = binding.emailAddressInput.getText().toString().trim();
        String location = binding.locationInput.getText().toString().trim();
        String longBio = binding.longBioInput.getText().toString().trim();

        presenter.saveProfile(firstName, lastName, phoneNumber, emailAddress, location, longBio,
                selectedCategories);
    }

    private void loginWithFacebook() {
        faceboookCallbackManager = CallbackManager.Factory.create();
        final String PERMISSION_EMAIL = "email";
        final String PERMISSION_PUBLIC_PROFILE = "public_profile";

        LoginManager.getInstance().registerCallback(faceboookCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        if (loginResult != null) {
                            final AccessToken accessToken = loginResult.getAccessToken();
                            if (accessToken != null) {
                                Set<String> declinedPermissions = accessToken.getDeclinedPermissions();
                                if (declinedPermissions.isEmpty()) {
                                    GraphRequest req = GraphRequest.newMeRequest(accessToken, (jsonObject, graphResponse) -> {
                                        if (jsonObject != null) {
                                            presenter.connectToFacebook(jsonObject);
                                        }
                                    });

                                    String fields = "id,email,gender,name,picture.type(large),link";
                                    Bundle params = new Bundle();
                                    params.putString("fields", fields);
                                    req.setParameters(params);
                                    req.executeAsync();
                                }
                            }
                        }
                    }

                    @Override
                    public void onCancel() {
                        Log.i(TAG, "Facebook login cancelled");
                    }

                    @Override
                    public void onError(FacebookException e) {
                        Log.e(TAG, "Facebook Login error", e);
                        Toast.makeText(getActivity(), R.string.facebook_login_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        ArrayList<String> permissions = new ArrayList<>();
        permissions.add(PERMISSION_EMAIL);
        permissions.add(PERMISSION_PUBLIC_PROFILE);
        LoginManager.getInstance().logInWithReadPermissions(this, permissions);
    }
}