package com.affinislabs.minbar.ui.common.customviews;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.widget.Checkable;

/**
 * minbar-v2
 * Michael Obi
 * 23/01/2018, 7:20 AM
 */

public class CheckableImageView extends AppCompatImageView implements Checkable {

    private static final int[] CheckedStateSet = {android.R.attr.state_checked};
    private boolean mChecked = false;

    public CheckableImageView(Context context) {
        super(context);
    }

    public CheckableImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public boolean isChecked() {
        return mChecked;
    }

    public void setChecked(boolean isChecked) {
        if (isChecked != mChecked) {
            mChecked = isChecked;
            refreshDrawableState();
        }
    }

    public void toggle() {
        setChecked(!mChecked);
    }

    @Override
    public int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
        if (isChecked()) {
            mergeDrawableStates(drawableState, CheckedStateSet);
        }
        return drawableState;
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        invalidate();
    }

    @Override
    public boolean performClick() {
        toggle();
        return super.performClick();
    }
}