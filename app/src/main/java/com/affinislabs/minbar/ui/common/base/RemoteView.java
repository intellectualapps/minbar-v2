package com.affinislabs.minbar.ui.common.base;

/**
 * Created by michaelobi on 1/11/18.
 */

public interface RemoteView extends View {

    void showProgress();

    void hideProgress();

    void showError(String errorMessage);

}
