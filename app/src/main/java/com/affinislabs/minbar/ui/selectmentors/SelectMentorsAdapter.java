package com.affinislabs.minbar.ui.selectmentors;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.ui.common.customviews.CheckableImageView;
import com.affinislabs.minbar.ui.profileview.ProfileViewActivity;
import com.affinislabs.minbar.utils.Constants;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by felixunlimited on 1/25/18.
 */

public class SelectMentorsAdapter extends RecyclerView.Adapter<SelectMentorsAdapter.ViewHolder> {

    private List<User> mentors;
    private Context context;
    private Set<String> selectedMentors = new HashSet<String>();

    public SelectMentorsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_mentor,
                parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final User user = mentors.get(position);
        viewHolder.userFullName.setText(user.getFullName());
        viewHolder.addUserView.setChecked(selectedMentors.contains(user.getUsername()));

        viewHolder.addUserView.setOnClickListener(v -> {
            if (viewHolder.addUserView.isChecked()) {
                selectedMentors.add(user.getUsername());
            } else {
                selectedMentors.remove(user.getUsername());
            }
        });
        viewHolder.userFullName.setOnClickListener(view -> {
            Intent profileViewIntent = new Intent(context, ProfileViewActivity.class);
            profileViewIntent.putExtra(Constants.SECOND_PARTY_USERNAME, user.getUsername());
            context.startActivity(profileViewIntent);

        });
        viewHolder.userProfilePhoto.setOnClickListener(view -> {
            Intent profileViewIntent = new Intent(context, ProfileViewActivity.class);
            profileViewIntent.putExtra(Constants.SECOND_PARTY_USERNAME, user.getUsername());
            context.startActivity(profileViewIntent);
        });
    }

    @Override
    public int getItemCount() {
        return mentors.size();
    }

    public void setItems(List<User> items) {
        this.mentors = items;
        notifyDataSetChanged();
    }

    public Set<String> getSelectedMentors() {
        return selectedMentors;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        TextView userFullName, userShortBio;
        ImageView userProfilePhoto;
        CheckableImageView addUserView;
        ImageView verifiedIcon;

        ViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            userFullName = itemView.findViewById(R.id.user_full_name);
            userShortBio = itemView.findViewById(R.id.user_short_bio);
            userProfilePhoto = itemView.findViewById(R.id.user_profile_photo);
            verifiedIcon = itemView.findViewById(R.id.verified_icon);
            addUserView = itemView.findViewById(R.id.add_user);
        }
    }
}
