package com.affinislabs.minbar.ui.questions.di;

import com.affinislabs.minbar.di.ScreenScoped;
import com.affinislabs.minbar.ui.questions.QuestionsFragment;
import com.affinislabs.minbar.ui.questions.myquestions.MyQuestionsFragment;
import com.affinislabs.minbar.ui.questions.questionstoanswer.QuestionsToAnswerFragment;

import dagger.Subcomponent;


/**
 * minbar-v2
 * Michael Obi
 * 12/03/2018, 7:27 AM
 */
@ScreenScoped
@Subcomponent
public interface QuestionsComponent {
    void inject(QuestionsFragment fragment);

    void inject(MyQuestionsFragment fragment);

    void inject(QuestionsToAnswerFragment fragment);
}
