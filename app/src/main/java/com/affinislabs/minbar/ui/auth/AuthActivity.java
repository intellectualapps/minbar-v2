package com.affinislabs.minbar.ui.auth;

import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.affinislabs.minbar.MinbarApplication;
import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.remote.MinbarApiService;
import com.affinislabs.minbar.databinding.AuthActivityBinding;
import com.affinislabs.minbar.di.SharedPref;
import com.affinislabs.minbar.domain.auth.AuthManager;
import com.affinislabs.minbar.ui.main.MainActivity;
import com.affinislabs.minbar.ui.becomementor.BecomeMentorActivity;
import com.affinislabs.minbar.utils.Constants;
import com.affinislabs.minbar.utils.PlayServicesUtil;

import javax.inject.Inject;


/**
 * Minbar
 * Michael Obi
 * 04 January 2018, 2:27 PM
 */

public class AuthActivity extends AppCompatActivity {
    @Inject
    MinbarApiService minbarApiService;
    @Inject
    AuthManager authManager;
    @Inject
    @SharedPref(value = SharedPref.META)
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        PlayServicesUtil.checkPlayServices(this);
        ((MinbarApplication) getApplication()).getAppComponent()
                .authenticationComponent()
                .inject(this);

        if (authManager.isUserLoggedIn() && authManager.getUser() != null) {
            navigateToNextPage();
        }

        AuthActivityBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_auth);

        if (savedInstanceState == null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            Fragment fragment = AuthFragment.newInstance();
            transaction.replace(binding.container.getId(), fragment, fragment.getClass().getSimpleName());
            transaction.commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        PlayServicesUtil.checkPlayServices(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(AuthFragment.class.getSimpleName());
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void navigateToNextPage() {
        boolean hasSeenBecomeMentorScreen = sharedPreferences.getBoolean
                (Constants.KEY_HAS_SEEN_BECOME_MENTOR_SCREEN, false);
        Intent i = new Intent(this, MainActivity.class);
        if (!hasSeenBecomeMentorScreen) {
            i = new Intent(this, BecomeMentorActivity.class);
        }
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        finish();
    }
}
