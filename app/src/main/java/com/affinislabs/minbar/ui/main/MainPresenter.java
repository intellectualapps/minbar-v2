package com.affinislabs.minbar.ui.main;

import android.util.Log;

import com.affinislabs.minbar.data.MinbarDataRepository;
import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.ui.common.base.BasePresenter;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * minbar
 * Michael Obi
 * 24/04/2018, 10:29 PM
 */
public class MainPresenter extends BasePresenter<MainView> {

    private static final String TAG = "MainPresenter";
    private MinbarDataRepository repository;

    @Inject
    public MainPresenter(MinbarDataRepository minbarDataRepository) {
        this.repository = minbarDataRepository;
    }

    public void checkLoginStatus() {
        if (!repository.getLoginStatus() || repository.getLoggedInUser() == null) {
            getView().navigateToLogin();
        }
    }

    public void loadProfile() {
        User user = repository.getLoggedInUser();
        getView().showProfileData(user);
    }

    public void logout() {
        repository.clearLoggedInUserData();
        getView().navigateToLogin();
    }

    public void loadOpenQuestionCount() {
        addDisposableSubscription(repository.getOpenQuestionCount()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(count -> getView().showOpenQuestionCount(count),
                        error -> Log.e(TAG, "Question Count Loading Failed")));
    }
}
