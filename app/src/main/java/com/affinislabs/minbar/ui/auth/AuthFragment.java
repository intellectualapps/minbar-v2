package com.affinislabs.minbar.ui.auth;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.affinislabs.minbar.MinbarApplication;
import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.remote.responses.AuthResponse;
import com.affinislabs.minbar.databinding.AuthFragmentBinding;
import com.affinislabs.minbar.domain.auth.AuthManager;
import com.affinislabs.minbar.ui.auth.createUsername.CreateUsernameFragment;
import com.affinislabs.minbar.ui.common.base.BaseFragment;
import com.affinislabs.minbar.ui.common.customviews.CustomTextView;
import com.affinislabs.minbar.ui.meta.AboutFragment;
import com.affinislabs.minbar.utils.Constants;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.linkedin.platform.ApiHelper;
import com.linkedin.platform.LiSessionManager;
import com.linkedin.platform.errors.LiApiError;
import com.linkedin.platform.errors.LiAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Set;

import javax.inject.Inject;



/**
 * Minbar
 * Michael Obi
 * 05 January 2018, 11:17 AM
 */

public class AuthFragment extends BaseFragment implements View.OnClickListener, AuthManager.SocialAuthListener {
    private static final String TAG = "AuthFragment";
    private static final String LINKED_IN_URL =
            "https://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,picture-url)?format=json";
    CallbackManager faceboookCallbackManager;
    @Inject
    AuthManager authManager;
    private AuthFragmentBinding binding;
    private CustomTextView aboutAppButton, privacyPolicyButton;


    public AuthFragment() {
    }

    public static Fragment newInstance() {
        return new AuthFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        faceboookCallbackManager = CallbackManager.Factory.create();
        ((MinbarApplication) getActivity().getApplication())
                .getAppComponent()
                .authenticationComponent()
                .inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_auth, container, false);
        binding.emailAuthButton.setOnClickListener(this);
        binding.facebookAuthButton.setOnClickListener(this);
        binding.linkedinAuthButton.setOnClickListener(this);
        aboutAppButton = binding.getRoot().findViewById(R.id.about_app_button);
        aboutAppButton.setOnClickListener(this);
        privacyPolicyButton = binding.getRoot().findViewById(R.id.privacy_policy_button);
        privacyPolicyButton.setOnClickListener(this);
        return binding.getRoot();
    }

    @Override
    public void onClick(View v) {
        faceboookCallbackManager = null;
        switch (v.getId()) {
            case R.id.email_auth_button:
                startFragment(EmailAuthFragment.newInstance());
                break;
            case R.id.facebook_auth_button:
                loginWithFacebook();
                break;
            case R.id.linkedin_auth_button:
                loginWithLinkedIn();
                break;
            case R.id.about_app_button:
                startMetaFragment(AboutFragment.newInstance());
                break;
            case R.id.privacy_policy_button:
                Intent privacyPolicyIntent = new Intent(Intent.ACTION_VIEW);
                privacyPolicyIntent.setData(Uri.parse(Constants.MA_PRIVACY_POLICY_URL));
                requireActivity().startActivityFromFragment(this, privacyPolicyIntent, 3000);
                break;
            default:
        }
    }

    private void loginWithFacebook() {
        faceboookCallbackManager = CallbackManager.Factory.create();
        final String PERMISSION_EMAIL = "email";
        final String PERMISSION_PUBLIC_PROFILE = "public_profile";

        LoginManager.getInstance().registerCallback(faceboookCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        if (loginResult != null) {
                            final AccessToken accessToken = loginResult.getAccessToken();
                            if (accessToken != null) {
                                Set<String> declinedPermissions = accessToken.getDeclinedPermissions();
                                if (declinedPermissions.isEmpty()) {
                                    GraphRequest req = GraphRequest.newMeRequest(accessToken, (jsonObject, graphResponse) -> {
                                        if (jsonObject != null) {
                                            showLoadingDialog(getString(R.string.login_progress_label));
                                            authManager.loginWithSocialAccount(jsonObject, Constants.AUTH_FACEBOOK,
                                                    AuthFragment.this);
                                        }
                                    });

                                    String fields = "id,email,gender,name,picture.type(large),link";
                                    Bundle params = new Bundle();
                                    params.putString("fields", fields);
                                    req.setParameters(params);
                                    req.executeAsync();
                                }
                            }
                        }
                    }

                    @Override
                    public void onCancel() {
                        Log.e(TAG, "Facebook login cancelled");
                    }

                    @Override
                    public void onError(FacebookException e) {
                        Log.e(TAG, "Facebook Login error", e);
                        Toast.makeText(getActivity(), R.string.facebook_login_error, Toast.LENGTH_SHORT).show();
                    }
                }
        );

        ArrayList<String> permissions = new ArrayList<>();
        permissions.add(PERMISSION_EMAIL);
        permissions.add(PERMISSION_PUBLIC_PROFILE);
        LoginManager.getInstance().logInWithReadPermissions(this, permissions);
    }


    private void loginWithLinkedIn() {
        Scope scope = Scope.build(Scope.R_BASICPROFILE, Scope.R_EMAILADDRESS);
        LiSessionManager.getInstance(getActivity().getApplicationContext())
                .init(getActivity(), scope, new AuthListener() {
                    @Override
                    public void onAuthSuccess() {
                        ApiHelper apiHelper = ApiHelper.getInstance(getActivity());
                        apiHelper.getRequest(getActivity(), LINKED_IN_URL, new ApiListener() {
                            @Override
                            public void onApiSuccess(ApiResponse result) {
                                try {
                                    JSONObject responseData = result.getResponseDataAsJson();
                                    authManager.loginWithSocialAccount(responseData,
                                            Constants.AUTH_LINKEDIN, AuthFragment.this);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onApiError(LiApiError error) {
                                error.printStackTrace();
                            }
                        });
                    }

                    @Override
                    public void onAuthError(LiAuthError error) {
                        Toast.makeText(getContext(), "failed "
                                        + error.toString(),
                                Toast.LENGTH_LONG).show();
                    }
                }, true);
    }


    @Override
    public void onSocialAuthSuccess(String email, AuthResponse response,
                                    @AuthManager.AuthMethod String authMethod,
                                    JSONObject socialProfileData) {
        hideLoadingDialog();
        String username = response.getUsername();
        if (username == null || username.isEmpty()) {
            CreateUsernameFragment fragment = CreateUsernameFragment.newInstance();
            Bundle params = new Bundle();
            params.putString("email", email);
            params.putString("tempKey", response.getTempKey());
            params.putString("socialProfileData", socialProfileData.toString());
            params.putString("authMethod", authMethod);
            fragment.setArguments(params);
            startFragment(fragment);
        } else {
            ((AuthActivity) requireActivity()).navigateToNextPage();
        }
    }

    @Override
    public void onSocialAuthError(String errorMessage) {
        hideLoadingDialog();
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (faceboookCallbackManager != null) {
            faceboookCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
        LiSessionManager.getInstance(getActivity().getApplicationContext())
                .onActivityResult(getActivity(), requestCode, resultCode, data);
    }
}
