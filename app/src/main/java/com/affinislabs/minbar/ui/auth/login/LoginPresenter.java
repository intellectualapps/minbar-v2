package com.affinislabs.minbar.ui.auth.login;

import com.affinislabs.minbar.domain.auth.AuthManager;
import com.affinislabs.minbar.ui.common.base.BasePresenter;

import javax.inject.Inject;

/**
 * minbar-v2
 * Michael Obi
 * 16 January 2018, 6:42 AM
 */

public class LoginPresenter extends BasePresenter<LoginContract.View> implements AuthManager.AuthListener {

    private AuthManager authManager;

    @Inject
    public LoginPresenter(AuthManager authManager) {
        this.authManager = authManager;
    }

    public void onLoginButtonClicked(String username, String password) {
        getView().showProgress();
        authManager.loginWithUsername(username, password, this);
    }

    @Override
    public void onAuthSuccess() {
        getView().hideProgress();
        if (authManager.isUserLoggedIn()) {
            getView().navigateToMainPage();
        }
    }

    @Override
    public void onAuthError(String errorMessage) {
        getView().hideProgress();
        getView().showError(errorMessage);
    }
}
