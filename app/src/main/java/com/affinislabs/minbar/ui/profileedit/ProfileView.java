package com.affinislabs.minbar.ui.profileedit;

import com.affinislabs.minbar.data.models.Category;
import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.ui.common.base.RemoteView;

import java.util.List;
import java.util.Set;

/**
 * Created
 by User
 on 2/16/2018.
 */

interface ProfileView extends RemoteView {
    void showCategories(List<Category> categories, Set<String> userCategoryIds);

    void showProfileData(User user);

    void showCategoryLoadingProgress();

    void hideCategoryLoadingProgress();

    void showCategoryLoadingError();

    void showSaveSuccess();

    void showFacebookConnectSuccess();

    void showFacebookDisconnectSuccess();

    void showImageUploadSuccess();

    void showImageUploadProgress();
}
