package com.affinislabs.minbar.ui.common.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.affinislabs.minbar.R;
import com.affinislabs.minbar.utils.FontUtils;

/**
 * Created by User on 2/16/2018.
 */

public class CustomSearchEditText extends AppCompatEditText {
    private static final String ANDROID_SCHEMA = "xmlns:android=\"http://schemas.android.com/apk/res/android\"";

    private static final int MAX_LENGTH = 10;
    
    public CustomSearchEditText(Context context) {
        this(context, null);
    }


    public CustomSearchEditText(Context context, AttributeSet attrs) {
        this(context, attrs, android.support.v7.appcompat.R.attr.editTextStyle);
    }

    public CustomSearchEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomSearchEditText,
                    defStyle, 0);

            int textStyle;
            if (a.hasValue(R.styleable.CustomSearchEditText_textStyle)) {
                textStyle = a.getInt(R.styleable.CustomSearchEditText_textStyle, 0);
            } else {
                textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", 0);
            }

            a.recycle();

            Typeface typeface = FontUtils.selectTypeface(context, textStyle);
            setTypeface(typeface);

            setOnEditorActionListener(new OnEditorActionListener() {
                @Override
                public synchronized boolean onEditorAction(TextView v,
                                                           int actionId, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN
                            && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                        return true;
                    }
                    return false;
                }
            });

            String value = "";
            final String viewMode = "editing";
            final String viewSide = "right";
            final Drawable mic = getResources().getDrawable(R.drawable.ic_action_mic);

            setHeight(mic.getBounds().height());

            mic.setBounds(0, 0, mic.getIntrinsicWidth(), mic.getIntrinsicHeight());
            Drawable x2 = viewMode.equals("never") ? null : viewMode
                    .equals("always") ? mic : viewMode.equals("editing") ? (value
                    .equals("") ? null : mic)
                    : viewMode.equals("unlessEditing") ? (value.equals("") ? mic
                    : null) : null;
            final Drawable searchIcon = getResources().getDrawable(
                    android.R.drawable.ic_search_category_default);
            searchIcon.setBounds(0, 0, mic.getIntrinsicWidth(),
                    mic.getIntrinsicHeight());

            setCompoundDrawables(searchIcon, null, viewSide.equals("right") ? x2
                    : null, null);

            setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (getCompoundDrawables()[viewSide.equals("left") ? 0 : 2] == null) {
                        return false;
                    }
                    if (event.getAction() != MotionEvent.ACTION_UP) {
                        return false;
                    }
                    if ((viewSide.equals("left") && event.getX() < getPaddingLeft()
                            + mic.getIntrinsicWidth())
                            || (viewSide.equals("right") && event.getX() > getWidth()
                            - getPaddingRight() - mic.getIntrinsicWidth())) {
                        Drawable x3 = viewMode.equals("never") ? null : viewMode
                                .equals("always") ? mic
                                : viewMode.equals("editing") ? null : viewMode
                                .equals("unlessEditing") ? mic : null;
                        setText("");
                        setCompoundDrawables(searchIcon, null,
                                viewSide.equals("right") ? x3 : null, null);
                    }
                    return false;
                }
            });
            addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before,
                                          int count) {
                    Drawable x4 = viewMode.equals("never") ? null : viewMode
                            .equals("always") ? mic
                            : viewMode.equals("editing") ? (getText().toString()
                            .equals("") ? null : mic) : viewMode
                            .equals("unlessEditing") ? (getText()
                            .toString().equals("") ? mic : null) : null;
                    setCompoundDrawables(searchIcon, null,
                            viewSide.equals("right") ? x4 : null, null);
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s != null && s.length() > MAX_LENGTH) {
                        setText(s.subSequence(0, MAX_LENGTH));
                        setSelection(MAX_LENGTH);
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {
                }
            });
        }
    }
}
