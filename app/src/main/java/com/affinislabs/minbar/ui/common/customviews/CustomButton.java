package com.affinislabs.minbar.ui.common.customviews;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.widget.Button;

import com.affinislabs.minbar.R;
import com.affinislabs.minbar.utils.FontUtils;


@SuppressLint("AppCompatCustomView")
public class CustomButton extends Button {
    private static final String DEFAULT_SCHEMA = "xmlns:android=\"http://schemas.android.com/apk/res/android\"";

    public CustomButton(Context context) {
        this(context, null);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode())
            init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        if (attrs != null) {
            TypedArray a = context.obtainStyledAttributes(attrs,
                    R.styleable.CustomButton);

            int textStyle = 0;
            if (a.hasValue(R.styleable.CustomButton_textStyle)) {
                textStyle = a.getInt(R.styleable.CustomButton_textStyle, 0);
            } else {
                //use default schema
                textStyle = attrs.getAttributeIntValue(DEFAULT_SCHEMA, "textStyle", 0);
            }

            a.recycle();
            applyCustomFont(context, textStyle);
        }
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            /*TypedArray array = getContext().obtainStyledAttributes(attrs, R.styleable.TrupprTextView);

			String fontName = array.getString(R.styleable.TrupprTextView_textFont);
			if(fontName != null){
				setFont(fontName);
			}
			else{
				setFont(getContext().getString(R.string.font_regular));
			}

			array.recycle();*/
        }
    }

    public void setFont(String fontName) {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontName);
        setTypeface(tf);
    }

    private void applyCustomFont(Context context, int textStyle) {
        Typeface typeface = FontUtils.selectTypeface(context, textStyle);

        if (typeface != null) {
            setTypeface(typeface);
        }
    }
}
