package com.affinislabs.minbar.ui.selectmentors;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.affinislabs.minbar.MinbarApplication;
import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.models.Question;
import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.databinding.SelectAdvisorsBinding;
import com.affinislabs.minbar.ui.main.MainActivity;
import com.affinislabs.minbar.ui.common.customviews.CustomTextView;
import com.affinislabs.minbar.utils.ToastUtil;

import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import io.reactivex.Observable;

public class SelectMentorsFragment extends Fragment implements SelectMentorsView,
        View.OnClickListener {
    Toolbar toolbar;
    CustomTextView toolbarTitle;
    @Inject
    SelectMentorsPresenter presenter;

    private ProgressDialog progressDialog;
    private SelectMentorsAdapter adapter;
    private SelectAdvisorsBinding binding;
    private Question question;
    private List<User> relevantMentors;

    public SelectMentorsFragment() {
    }

    public static Fragment newInstance(Question question) {
        SelectMentorsFragment selectMentorsFragment = new SelectMentorsFragment();
        Bundle args = new Bundle();
        args.putParcelable("question", question);
        selectMentorsFragment.setArguments(args);
        return selectMentorsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MinbarApplication) requireActivity().getApplication())
                .getAppComponent()
                .selectMentorsComponent()
                .inject(this);
        if (getArguments() != null) {
            question = getArguments().getParcelable("question");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        presenter.attachView(this);
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_select_mentors, container, false);
        binding.sendToSelectedButton.setOnClickListener(this);
        binding.sendToAllButton.setOnClickListener(this);
        toolbar = binding.getRoot().findViewById(R.id.toolbar);
        toolbarTitle = binding.getRoot().findViewById(R.id.toolbar_title);
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new SelectMentorsAdapter(requireContext());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.mentorRecyclerView.setAdapter(adapter);
        binding.mentorRecyclerView.setLayoutManager(linearLayoutManager);
        binding.mentorRecyclerView.addItemDecoration(new DividerItemDecoration(requireContext(),
                DividerItemDecoration.VERTICAL));
        toolbarTitle.setText(R.string.select_mentors_toolbar_title);
        toolbar.setNavigationIcon(R.drawable.ic_action_back);
        toolbar.setNavigationOnClickListener(view1 -> requireActivity().getSupportFragmentManager()
                .popBackStack());
        presenter.loadMentors(question);
    }

    @Override
    public void showProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getContext(), R.style.AppCompatAlertDialogStyle);
        }

        progressDialog.setMessage(getString(R.string.ask_question_progress_label));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog == null) return;
        progressDialog.dismiss();
    }

    @Override
    public void showError(String errorMessage) {
        ToastUtil.showToast(requireActivity(), errorMessage, Toast.LENGTH_SHORT);
    }

    @Override
    public void showMentorsLoadingProgress() {
        binding.mentorsLoadingProgress.setVisibility(View.VISIBLE);
        binding.mentorRecyclerView.setVisibility(View.GONE);
        binding.sendToAllButton.setVisibility(View.GONE);
    }

    @Override
    public void hideMentorsLoadingProgress() {
        binding.mentorsLoadingProgress.setVisibility(View.GONE);
    }

    @Override
    public void showMentors(List<User> relevantMentors) {
        hideMentorsLoadingProgress();
        this.relevantMentors = relevantMentors;
        binding.sendToAllButton.setVisibility(View.VISIBLE);
        binding.sendToAllButton.setText(getString(R.string.send_question_to_all, relevantMentors.size()));
        binding.mentorRecyclerView.setVisibility(View.VISIBLE);
        adapter.setItems(relevantMentors);
    }

    @Override
    public void finishAskQuestionProcess() {
        Toast.makeText(requireActivity(), R.string.question_sent_to_mentors, Toast.LENGTH_LONG).show();
        ((MainActivity) requireActivity()).navigateHome();
    }

    @Override
    public void onClick(View v) {
        Set<String> selectedMentors = adapter.getSelectedMentors();
        switch (v.getId()) {
            case R.id.send_to_selected_button:
                if (selectedMentors.size() > 0) {
                    presenter.saveSelectedMentors(question, selectedMentors);
                } else {
                    ToastUtil.showToast(requireActivity(), getString(R.string.mentor_select_error),
                            Toast.LENGTH_SHORT);
                }
                break;
            case R.id.send_to_all_button:
                Observable.fromIterable(relevantMentors)
                        .forEach(user -> selectedMentors.add(user.getUsername()));
                presenter.saveSelectedMentors(question, selectedMentors);
                break;
            default:
        }
    }
}