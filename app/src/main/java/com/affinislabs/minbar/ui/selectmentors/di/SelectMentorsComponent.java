package com.affinislabs.minbar.ui.selectmentors.di;

import com.affinislabs.minbar.di.ScreenScoped;
import com.affinislabs.minbar.ui.selectmentors.SelectMentorsFragment;

import dagger.Subcomponent;

/**
 * minbar-v2
 * Michael Obi
 * 11/03/2018, 11:25 PM
 */

@ScreenScoped
@Subcomponent
public interface SelectMentorsComponent {
    void inject(SelectMentorsFragment fragment);
}
