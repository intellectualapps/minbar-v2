package com.affinislabs.minbar.ui.auth.login;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.affinislabs.minbar.MinbarApplication;
import com.affinislabs.minbar.R;
import com.affinislabs.minbar.databinding.LoginFragmentBinding;
import com.affinislabs.minbar.ui.auth.AuthActivity;
import com.affinislabs.minbar.ui.auth.forgotpassword.ForgotPasswordFragment;
import com.affinislabs.minbar.ui.common.base.BaseFragment;
import com.affinislabs.minbar.ui.common.base.ViewPagerFragmentLoadedListener;
import com.affinislabs.minbar.utils.ToastUtil;

import javax.inject.Inject;

/**
 * Minbar
 * Michael Obi
 * 06 January 2018, 1:02 AM
 */

public class LoginFragment extends BaseFragment implements View.OnClickListener,
        LoginContract.View {

    @Inject
    LoginPresenter presenter;
    private LoginFragmentBinding binding;

    public LoginFragment() {
    }

    public static LoginFragment newInstance(ViewPagerFragmentLoadedListener fragmentLoadedListener) {
        LoginFragment fragment = new LoginFragment();
        fragment.fragmentLoadedListener = fragmentLoadedListener;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MinbarApplication) getActivity().getApplication())
                .getAppComponent()
                .authenticationComponent()
                .inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        binding.loginButton.setOnClickListener(this);
        binding.forgotPasswordButton.setOnClickListener(this);

        presenter.attachView(this);
        return binding.getRoot();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_button:
                String username = binding.mentorUsername.getText().toString();
                String password = binding.password.getText().toString();
                presenter.onLoginButtonClicked(username, password);
                break;
            case R.id.forgot_password_button:
                startFragment(ForgotPasswordFragment.newInstance());
                break;
            default:
        }
    }

    @Override
    public void showProgress() {
        showLoadingDialog(getString(R.string.login_progress_label));
    }

    @Override
    public void hideProgress() {
        hideLoadingDialog();
    }

    @Override
    public void showError(String errorMessage) {
        ToastUtil.showToast(getActivity(), errorMessage, Toast.LENGTH_LONG);
    }

    @Override
    public void navigateToMainPage() {
        ((AuthActivity) getActivity()).navigateToNextPage();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
    }
}
