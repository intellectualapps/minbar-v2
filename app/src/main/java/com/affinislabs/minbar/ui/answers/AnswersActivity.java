package com.affinislabs.minbar.ui.answers;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;
import android.view.View;

import com.affinislabs.minbar.MinbarApplication;
import com.affinislabs.minbar.R;
import com.affinislabs.minbar.data.models.Answer;
import com.affinislabs.minbar.data.models.Question;
import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.databinding.AnswersActivityBinding;
import com.affinislabs.minbar.domain.auth.AuthManager;
import com.affinislabs.minbar.ui.threads.ThreadActivity;
import com.affinislabs.minbar.utils.Constants;
import com.affinislabs.minbar.utils.DateUtils;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

import static com.affinislabs.minbar.utils.Constants.THREADS_ACTIVITY_REQUEST_CODE;

public class AnswersActivity extends AppCompatActivity implements AnswersView, AnswersAdapter.AnswerClickHandler {

    @Inject
    AnswersPresenter presenter;
    @Inject
    AuthManager authManager;

    AnswersActivityBinding binding;
    private Question question;
    private AnswersAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MinbarApplication) getApplication())
                .getAppComponent()
                .answersComponent()
                .inject(this);
        presenter.attachView(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_answers);
        question = getIntent().getParcelableExtra(Constants.QUESTION);

        setSupportActionBar(binding.toolbarLayout.toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        binding.toolbarLayout.toolbar.setNavigationIcon(getResources().getDrawable(R.drawable
                .ic_action_back));
        binding.toolbarLayout.toolbarTitle.setText(R.string.answers);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adapter = new AnswersAdapter(this);
        binding.recyclerviewAnswers.setAdapter(adapter);
        binding.recyclerviewAnswers.setLayoutManager(linearLayoutManager);
        binding.recyclerviewAnswers.addItemDecoration(new DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL));
        binding.textViewQuestion.setText(question.getBody());

        Date questionDate = question.getDateAsked();
        if (questionDate != null) {
            binding.questionTime.setText(getString(R.string.question_ask_time,
                    DateUtils.getRelativeDateText(questionDate), DateUtils.getShortTime(questionDate)));
        }
        presenter.loadAnswers(question);

        presenter.markAsRead(question.getId());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public void showProgress() {
        binding.answersListContainer.setVisibility(View.GONE);
        binding.answersLoadingProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        binding.answersLoadingProgress.setVisibility(View.GONE);
    }

    @Override
    public void showError(String errorMessage) {
        Snackbar snackbar = Snackbar.make(binding.container,
                R.string.answers_loading_error,
                Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(R.string.retry, v -> {
            snackbar.dismiss();
            presenter.loadAnswers(question);
        });
        snackbar.show();
    }

    @SuppressLint("CheckResult")
    @Override
    public void showAnswers(List<Answer> answers) {
        binding.answersListContainer.setVisibility(View.VISIBLE);
        binding.answersCount.setText(getResources().getQuantityString(R.plurals.numberOfAnswers,
                answers.size(), answers.size()));
        adapter.setAnswers(answers);
        User loggedInUser = authManager.getUser();
        Observable.fromIterable(answers)
                .any(answer -> answer.getMentorUsername().equals(loggedInUser.getUsername()))
                .subscribe(hasAnswered -> {
                    if (hasAnswered || question.wasWrittenBy(authManager.getUser())) {
                        binding.btnSubmitAnswer.setVisibility(View.GONE);
                        return;
                    }
                    binding.btnSubmitAnswer.setVisibility(View.VISIBLE);
                    binding.btnSubmitAnswer.setOnClickListener(v -> {
                        Intent i = new Intent(AnswersActivity.this, ThreadActivity.class);
                        i.putExtra(Constants.QUESTION, question);
                        startActivityForResult(i, THREADS_ACTIVITY_REQUEST_CODE);
                    });
                });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }

    @Override
    public void onAnswerClicked(Answer answer) {
        User user = authManager.getUser();
        if (answer.getMentorUsername().equalsIgnoreCase(user.getUsername()) ||
                question.getAsker().equalsIgnoreCase(user.getUsername())) {
            Intent i = new Intent(this, ThreadActivity.class);
            i.putExtra(Constants.ANSWER, answer);
            startActivityForResult(i, THREADS_ACTIVITY_REQUEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == THREADS_ACTIVITY_REQUEST_CODE) {
            if (intent != null && intent.getData() != null) {
                String returnedResult = intent.getData().toString();
                if (returnedResult.equals(Constants.ACTIVITY_RESULT_DONE_WITH_ANSWER)) {
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        }
    }
}
