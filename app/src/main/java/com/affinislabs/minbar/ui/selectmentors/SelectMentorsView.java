package com.affinislabs.minbar.ui.selectmentors;

import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.ui.common.base.RemoteView;

import java.util.List;

/**
 * Created by felixunlimited on 1/29/18.
 */

interface SelectMentorsView extends RemoteView {
    void showMentorsLoadingProgress();

    void hideMentorsLoadingProgress();

    void showMentors(List<User> relevantMentors);

    void finishAskQuestionProcess();
}
