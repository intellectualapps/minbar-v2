package com.affinislabs.minbar.ui.askquestion;

import com.affinislabs.minbar.data.models.Category;
import com.affinislabs.minbar.data.models.Question;
import com.affinislabs.minbar.ui.common.base.RemoteView;

import java.util.List;

/**
 * minbar-v2
 * Michael Obi
 * 25/01/2018, 12:14 AM
 */

public interface AskQuestionView extends RemoteView {
    void showCategories(List<Category> categories);

    void showCategoryLoadingProgress();

    void hideCategoryLoadingProgress();

    void navigateToNextPage(Question question);

    void showCategoryLoadingError();
}
