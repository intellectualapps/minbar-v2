package com.affinislabs.minbar.di;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.affinislabs.minbar.MinbarApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static com.affinislabs.minbar.utils.Constants.USER_SHARED_PREFERENCES;

/**
 * minbar-v2
 * Michael Obi
 * 11/03/2018, 3:35 PM
 */

@Module
public class AppModule {

    private final MinbarApplication app;

    public AppModule(MinbarApplication app) {
        this.app = app;
    }

    @Provides
    @Singleton
    MinbarApplication provideApplication() {
        return app;
    }

    @Singleton
    @Provides
    Context provideContext() {
        return app.getApplicationContext();
    }

    @Provides
    @SharedPref(SharedPref.META)
    @Singleton
    SharedPreferences provideMetaDataSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Provides
    @SharedPref
    @Singleton
    SharedPreferences provideUserDataSharedPreferences(Context context) {
        return context.getSharedPreferences(USER_SHARED_PREFERENCES, Context.MODE_PRIVATE);
    }
}
