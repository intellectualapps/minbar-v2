package com.affinislabs.minbar.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * minbar-v2
 * Michael Obi
 * 11/03/2018, 4:08 PM
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ScreenScoped {
}
