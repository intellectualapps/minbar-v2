package com.affinislabs.minbar.di;

import com.affinislabs.minbar.data.di.ApiModule;
import com.affinislabs.minbar.domain.auth.di.AuthManagerModule;
import com.affinislabs.minbar.fcm.di.FcmComponent;
import com.affinislabs.minbar.ui.advisors.di.AdvisorsComponent;
import com.affinislabs.minbar.ui.answers.di.AnswersComponent;
import com.affinislabs.minbar.ui.askquestion.di.AskQuestionComponent;
import com.affinislabs.minbar.ui.auth.di.AuthComponent;
import com.affinislabs.minbar.ui.becomementor.di.BecomeMentorComponent;
import com.affinislabs.minbar.ui.main.di.MainComponent;
import com.affinislabs.minbar.ui.profileedit.di.ProfileEditComponent;
import com.affinislabs.minbar.ui.profileview.di.ProfileViewComponent;
import com.affinislabs.minbar.ui.questions.di.QuestionsComponent;
import com.affinislabs.minbar.ui.selectmentors.di.SelectMentorsComponent;
import com.affinislabs.minbar.ui.threads.di.ThreadsComponent;

import javax.inject.Singleton;

import dagger.Component;

/**
 * minbar-v2
 * Michael Obi
 * 11/03/2018, 3:34 PM
 */

@Singleton
@Component(modules = {AppModule.class, AuthManagerModule.class, ApiModule.class})
public interface AppComponent {
    MainComponent mainComponent();

    AskQuestionComponent askQuestionComponent();

    BecomeMentorComponent becomeMentorComponent();

    ProfileEditComponent profileEditComponent();

    SelectMentorsComponent selectMentorsComponent();

    AuthComponent authenticationComponent();

    ThreadsComponent threadsComponent();

    QuestionsComponent questionsComponent();

    AnswersComponent answersComponent();

    AdvisorsComponent advisorsComponent();

    ProfileViewComponent profileViewComponent();

    FcmComponent getFcmComponent();
}