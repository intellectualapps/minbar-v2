package com.affinislabs.minbar.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * minbar-v2
 * Michael Obi
 * 30/03/2018, 3:51 PM
 */

@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface SharedPref {
    String USER = "user";
    String META = "meta";

    String  value() default USER;
}

