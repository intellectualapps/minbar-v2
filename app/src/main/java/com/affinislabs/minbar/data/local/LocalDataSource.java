package com.affinislabs.minbar.data.local;

import android.content.SharedPreferences;
import android.util.Log;

import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.di.SharedPref;
import com.affinislabs.minbar.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import javax.inject.Inject;

/**
 * minbar-v2
 * Michael Obi
 * 15/04/2018, 10:37 PM
 */
public class LocalDataSource {
    private static final String TAG = "LocalDataSource";
    private final SharedPreferences userSharedPreferences;

    @Inject
    public LocalDataSource(@SharedPref SharedPreferences userSharedPreferences) {
        this.userSharedPreferences = userSharedPreferences;
    }

    public void saveUserData(User user) {
        SharedPreferences.Editor editor = userSharedPreferences.edit();
        editor.putString(Constants.PREF_USER_DATA, new Gson().toJson(user));
        editor.apply();
    }

    public void saveAuthToken(String authToken) {
        SharedPreferences.Editor editor = userSharedPreferences.edit();
        editor.putBoolean(Constants.PREF_LOGGED_IN_STATUS, true);
        editor.putString(Constants.PREF_AUTH_TOKEN, authToken);
        editor.apply();
    }

    public void saveUserProfile(User user) {
        SharedPreferences.Editor editor = userSharedPreferences.edit();
        editor.putString(Constants.PREF_USER_DATA, new Gson().toJson(user));
        editor.apply();
    }

    public void clearLoggedInUserData() {
        SharedPreferences.Editor editor = userSharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public boolean getLoginStatus() {
        return userSharedPreferences.getBoolean(Constants.PREF_LOGGED_IN_STATUS, false);
    }

    public User getUser() {
        String sUser = userSharedPreferences.getString(Constants.PREF_USER_DATA, null);
        try {
            Gson gson = new Gson();
            return gson.fromJson(sUser, User.class);
        } catch (JsonSyntaxException e) {
            Log.e(TAG, e.getMessage(), e);
            return null;
        }
    }
}
