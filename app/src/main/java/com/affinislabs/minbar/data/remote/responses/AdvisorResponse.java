package com.affinislabs.minbar.data.remote.responses;

import com.affinislabs.minbar.data.models.Mentor;

import java.util.List;

/**
 * Created
 * by User
 * on 2/19/2018.
 */

public class AdvisorResponse {
    private List<Mentor> advisors;
    private List<Mentor> mentees;

    public List<Mentor> getAdvisors() {
        return advisors;
    }

    public void setAdvisors(List<Mentor> advisors) {
        this.advisors = advisors;
    }

    public List<Mentor> getMentees() {
        return mentees;
    }

    public void setMentees(List<Mentor> mentees) {
        this.mentees = mentees;
    }
}
