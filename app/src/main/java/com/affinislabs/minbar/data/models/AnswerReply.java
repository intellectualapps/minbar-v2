package com.affinislabs.minbar.data.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;

/**
 * minbar-v2
 * Michael Obi
 * 20/02/2018, 11:45 AM
 */

public class AnswerReply {

    private String id;
    private String username;
    private String text;
    private HashMap<String, Object> timestampCreated;

    private User author;

    public AnswerReply() {
        HashMap<String, Object> timestampNow = new HashMap<>();
        timestampNow.put("time", ServerValue.TIMESTAMP);
        timestampCreated = timestampNow;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Exclude
    public long getTime() {
        return (long) timestampCreated.get("time");
    }

    @Exclude
    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public HashMap<String, Object> getTimestampCreated() {
        return timestampCreated;
    }
}
