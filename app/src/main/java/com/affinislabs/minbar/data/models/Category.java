package com.affinislabs.minbar.data.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * minbar-v2
 * Michael Obi
 * 23/01/2018, 11:53 AM
 */

public class Category implements Parcelable {
    public static final Parcelable.Creator<Category> CREATOR = new Parcelable.Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
    private String id;
    private String imageLink;
    private String name;

    public Category() {
    }

    protected Category(Parcel in) {
        this.id = in.readString();
        this.imageLink = in.readString();
        this.name = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.imageLink);
        dest.writeString(this.name);
    }
}
