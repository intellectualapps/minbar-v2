package com.affinislabs.minbar.data.remote;

import com.affinislabs.minbar.data.remote.responses.AdvisorResponse;
import com.affinislabs.minbar.data.remote.responses.AnswersResponse;
import com.affinislabs.minbar.data.remote.responses.AuthResponse;
import com.affinislabs.minbar.data.remote.responses.CategoryResponse;
import com.affinislabs.minbar.data.remote.responses.MenteeDecisionResponse;
import com.affinislabs.minbar.data.remote.responses.MentorOptInResponse;
import com.affinislabs.minbar.data.remote.responses.OpenQuestionsCountResponse;
import com.affinislabs.minbar.data.remote.responses.QuestionResponse;
import com.affinislabs.minbar.data.remote.responses.RelatedUserResponse;
import com.affinislabs.minbar.data.remote.responses.RelevantMentorsResponse;
import com.affinislabs.minbar.data.remote.responses.SaveAnswerResponse;
import com.affinislabs.minbar.data.remote.responses.UserDataResponse;
import com.affinislabs.minbar.data.remote.responses.UsernameCheckResponse;
import com.affinislabs.minbar.domain.auth.AuthManager;

import java.util.Map;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * minbar-v2
 * Michael Obi
 * 10/01/2018, 12:05 PM
 */

public interface MinbarApiService {
    String BASE_URL = "https://minbarapp-android.appspot.com/api/v1/";

    @POST("user")
    @FormUrlEncoded
    Call<AuthResponse> createAccount(@Field("username") String username, @Field("email") String email,
                                     @Field("password") String password);

    @GET("user/verify/{username}")
    Single<UsernameCheckResponse> checkUserNameValidity(@Path("username") String username);

    @POST("user/authenticate")
    @FormUrlEncoded
    Call<AuthResponse> login(@AuthManager.AuthMethod @Field("auth-type") String authMethod,
                             @Field("id") String username, @Field("password") String password);

    @POST("user/authenticate")
    @FormUrlEncoded
    Call<AuthResponse> login(@AuthManager.AuthMethod @Field("auth-type") String authMethod,
                             @Field("id") String username);

    @GET("category/all")
    Single<CategoryResponse> getAllCategories();

    @POST("mentors/opt-in")
    @FormUrlEncoded
    Single<MentorOptInResponse> mentorOptIn(@Field("category-ids") String categoryIds);

    @POST("questions")
    @FormUrlEncoded
    Flowable<QuestionResponse> saveQuestion(@Field("question-body") String question,
                                            @Field("category-ids") String categoryIds);

    @GET("mentors/question/{question-id}")
    Single<RelevantMentorsResponse> getRelevantMentors(@Path("question-id") String questionId);

    @GET("questions/user")
    Single<QuestionResponse> getQuestionsUserAsked();

    @GET("mentors/mentor-questions")
    Single<QuestionResponse> getQuestionsUserAnswer();

    @POST("mentors/question/{question-id}")
    @FormUrlEncoded
    Completable pointQuestionToSelectedMentors(@Path("question-id") String questionId,
                                               @Field("mentor-usernames") String mentorUsernames);

    @GET("advisors")
    Flowable<AdvisorResponse> getAdvisors();

    @GET("advisors/mentees")
    Flowable<AdvisorResponse> getMentees();

    @PUT("advisors/accept/{mentee-username}")
    Call<MenteeDecisionResponse> acceptMenteeRequest(@Path("mentee-username") String menteeUsername);

    @PUT("advisors/decline/{mentee-username}")
    Call<MenteeDecisionResponse> declineMenteeRequest(@Path("mentee-username") String menteeUsername);

    @GET("questions/answer/{question-id}")
    Flowable<AnswersResponse> getAnswersForQuestion(@Path("question-id") String questionId);

    @POST("questions/answer/{question-id}")
    @FormUrlEncoded
    Single<SaveAnswerResponse> saveAnswer(@Path("question-id") String id,
                                          @Field("answer-text") String answer);

    @GET("user/relationships")
    Single<UserDataResponse> getSecondPartyPublicProfile(@Query("second-party-username") String secondPartyUsername);

    @GET("user")
    Single<UserDataResponse> getLoggedInUser();

    @POST("advisors/{advisor-username}")
    Completable inviteAdvisor(@Path("advisor-username") String advisorUsername);

    @FormUrlEncoded
    @PUT("user/profile")
    Single<UserDataResponse> saveProfile(@FieldMap Map<String, String> profileData);

    @FormUrlEncoded
    @POST("user/username")
    Single<AuthResponse> createUsername(@Field("username") String username,
                                        @Field("email") String email,
                                        @Field("temp-key") String tempKey);

    @GET("user/relationships")
    Single<RelatedUserResponse> fetchUserProfile(@Query("second-party-username") String mentorUsername);

    @PUT("questions/user/done/{question-id}")
    Completable markHappyWithAnswer(@Path("question-id") String id);

    @PUT("mentors/mentor-questions/done/{question-id}")
    Completable markQuestionAsDone(@Path("question-id") String id);

    @POST("ratings")
    @FormUrlEncoded
    Completable rateMentor(@Field("username-to-rate") String username, @Field("rating") float rating);

    @DELETE("mentors/mentor-questions/remove/{question-id}")
    Completable deleteMentorQuestion(@Path("question-id") String questionId);

    @DELETE("questions/user/remove/{question-id}")
    Completable deleteMyQuestion(@Path("question-id") String questionId);

    @POST("user/device-token/{username}")
    @FormUrlEncoded
    Completable updateFcmToken(@Path("username") String username, @Field("token") String fcmToken,
                               @Field("uuid") String uuid, @Field("platform") String platform);

    @GET("questions/open")
    Single<OpenQuestionsCountResponse> getOpenQuestionCount();

    @PUT("mentors/mentor-questions/visible/{question-id}")
    Completable markQuestionAsRead(@Path("question-id") String questionId);
}
