package com.affinislabs.minbar.data.remote.responses;

import com.affinislabs.minbar.data.models.Category;
import com.affinislabs.minbar.data.models.User;

import java.util.List;

/**
 * minbar-v2
 * Michael Obi
 * 30/01/2018, 4:17 PM
 */

public class MentorOptInResponse {
    private List<Category> categories;

    private User mentor;

    public List<Category> getCategories() {
        return this.categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public User getMentor() {
        return this.mentor;
    }

    public void setMentor(User mentor) {
        this.mentor = mentor;
    }
}
