package com.affinislabs.minbar.data.remote.responses;

import com.affinislabs.minbar.data.models.Category;

import java.util.List;

/**
 * minbar-v2
 * Michael Obi
 * 23/01/2018, 3:15 PM
 */

public class CategoryResponse extends DefaultResponse {

    List<Category> categories;

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}
