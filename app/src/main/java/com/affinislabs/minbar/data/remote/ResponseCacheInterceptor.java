package com.affinislabs.minbar.data.remote;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * minbar-v2
 * Michael Obi
 * 22/02/2018, 3:51 PM
 */

public final class ResponseCacheInterceptor implements Interceptor {
    private static final String TAG = "ResponseCacheInterceptor";

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();
        if (Boolean.valueOf(request.header("ApplyResponseCache"))) {
            Response originalResponse = chain.proceed(chain.request());
            return originalResponse.newBuilder()
                    .removeHeader("ApplyResponseCache")
                    .header("Cache-Control", "public, max-age=" + 120)
                    .build();
        }

        return chain.proceed(chain.request());
    }
}
