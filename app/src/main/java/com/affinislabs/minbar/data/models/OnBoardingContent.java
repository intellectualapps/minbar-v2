package com.affinislabs.minbar.data.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * minbar-v2
 * Michael Obi
 * 27/01/2018, 10:19 PM
 */

public class OnBoardingContent implements Parcelable {

    public static final Parcelable.Creator<OnBoardingContent> CREATOR = new Parcelable.Creator<OnBoardingContent>() {
        @Override
        public OnBoardingContent createFromParcel(Parcel source) {
            return new OnBoardingContent(source);
        }

        @Override
        public OnBoardingContent[] newArray(int size) {
            return new OnBoardingContent[size];
        }
    };
    private String heading;
    private String description;
    private int iconResource;

    public OnBoardingContent(String heading, String description, int iconResource) {
        this.heading = heading;
        this.description = description;
        this.iconResource = iconResource;
    }

    protected OnBoardingContent(Parcel in) {
        this.heading = in.readString();
        this.description = in.readString();
        this.iconResource = in.readInt();
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIconResource() {
        return iconResource;
    }

    public void setIconResource(int iconResource) {
        this.iconResource = iconResource;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.heading);
        dest.writeString(this.description);
        dest.writeInt(this.iconResource);
    }
}
