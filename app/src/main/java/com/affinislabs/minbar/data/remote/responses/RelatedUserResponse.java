package com.affinislabs.minbar.data.remote.responses;

/**
 * minbar-v2
 * Michael Obi
 * 06/03/2018, 5:09 PM
 */

public class RelatedUserResponse extends DefaultResponse {

    private String linkedinProfile;

    private String lastName;

    private String shortBio;

    private String facebookProfile;

    private String relationship;

    private String username;

    private String phoneNumber;

    private String email;

    private String firstName;

    private String photoUrl;

    private String longBio;

    public String getLinkedinProfile() {
        return this.linkedinProfile;
    }

    public void setLinkedinProfile(String linkedinProfile) {
        this.linkedinProfile = linkedinProfile;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getShortBio() {
        return this.shortBio;
    }

    public void setShortBio(String shortBio) {
        this.shortBio = shortBio;
    }

    public String getFacebookProfile() {
        return this.facebookProfile;
    }

    public void setFacebookProfile(String facebookProfile) {
        this.facebookProfile = facebookProfile;
    }

    public String getRelationship() {
        return this.relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhotoUrl() {
        return this.photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getLongBio() {
        return this.longBio;
    }

    public void setLongBio(String longBio) {
        this.longBio = longBio;
    }
}
