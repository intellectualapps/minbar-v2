package com.affinislabs.minbar.data.remote.responses;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Set;

public class UserDataResponse extends DefaultResponse {

    @SerializedName("creationDate")
    @Expose
    private String creationDate;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("photoUrl")
    @Expose
    private String photoUrl;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("invited")
    @Expose
    private boolean invited;
    @SerializedName("tempKey")
    @Expose
    private String tempKey;
    @SerializedName("linkedInProfile")
    @Expose
    private String linkedinProfile;
    @SerializedName("facebookProfile")
    @Expose
    private String facebookProfile;

    @SerializedName("categories")
    private Set<String> categoryIds;

    @SerializedName("shortBio")
    private String shortBio;

    @SerializedName("longBio")
    private String longBio;

    @SerializedName("relationship")
    @Expose
    private String relationshipToCaller;

    public String getRelationshipToCaller() {
        return relationshipToCaller;
    }

    public void setRelationshipToCaller(String relationshipToCaller) {
        this.relationshipToCaller = relationshipToCaller;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean getInvited() {
        return invited;
    }

    public void setInvited(boolean invited) {
        this.invited = invited;
    }

    public String getTempKey() {
        return tempKey;
    }

    public void setTempKey(String tempKey) {
        this.tempKey = tempKey;
    }

    public String getLinkedinProfile() {
        return linkedinProfile;
    }

    public void setLinkedinProfile(String linkedinProfile) {
        this.linkedinProfile = linkedinProfile;
    }

    public String getFacebookProfile() {
        return facebookProfile;
    }

    public void setFacebookProfile(String facebookProfile) {
        this.facebookProfile = facebookProfile;
    }

    public String getLongBio() {
        return longBio;
    }

    public void setLongBio(String longBio) {
        this.longBio = longBio;
    }

    public String getShortBio() {
        return shortBio;
    }

    public void setShortBio(String shortBio) {
        this.shortBio = shortBio;
    }

    public String getFullName() {
        String fullName;
        if (!TextUtils.isEmpty(getFirstName()) && TextUtils.isEmpty(getLastName()))
            fullName = getFirstName();
        else if (TextUtils.isEmpty(getFirstName()) && !TextUtils.isEmpty(getLastName()))
            fullName = getLastName();
        else if (!TextUtils.isEmpty(getFirstName()) && !TextUtils.isEmpty(getLastName()))
            fullName = getFirstName() + " " + getLastName();
        else
            fullName = "";

        return fullName;
    }

    public Set<String> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(Set<String> categoryIds) {
        this.categoryIds = categoryIds;
    }
}
