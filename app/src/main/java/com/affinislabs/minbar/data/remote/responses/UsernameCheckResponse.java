package com.affinislabs.minbar.data.remote.responses;

/**
 * minbar-v2
 * Michael Obi
 * 12 January 2018, 4:43 PM
 */


public class UsernameCheckResponse extends DefaultResponse {
    private boolean status;

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
