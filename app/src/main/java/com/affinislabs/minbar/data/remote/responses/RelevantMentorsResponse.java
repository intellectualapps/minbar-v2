package com.affinislabs.minbar.data.remote.responses;

import com.affinislabs.minbar.data.models.User;

import java.util.List;

/**
 * minbar-v2
 * Michael Obi
 * 01/02/2018, 6:11 PM
 */

public class RelevantMentorsResponse extends DefaultResponse {

    private List<List<User>> relevantMentors;

    public List<List<User>> getRelevantMentors() {
        return relevantMentors;
    }

    public void setRelevantMentors(List<List<User>> relevantMentors) {
        this.relevantMentors = relevantMentors;
    }
}
