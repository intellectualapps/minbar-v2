package com.affinislabs.minbar.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Mentor implements Parcelable{
    @SerializedName("creationDate")
    @Expose
    private String creationDate;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName(value = "advisorPhotoUrl", alternate = "menteePhotoUrl")
    @Expose
    private String photoUrl;
    @SerializedName(value = "advisorFullname", alternate = "menteeFullname")
    @Expose
    private String fullName;
    @SerializedName(value = "advisorUsername", alternate = "menteeUsername")
    @Expose
    private String username;

    public Mentor() {
    }

    protected Mentor(Parcel in) {
        this.creationDate = in.readString();
        this.status = in.readString();
        this.photoUrl = in.readString();
        this.fullName = in.readString();
        this.username = in.readString();
    }

    public static final Creator<Mentor> CREATOR = new Creator<Mentor>() {
        @Override
        public Mentor createFromParcel(Parcel in) {
            return new Mentor(in);
        }

        @Override
        public Mentor[] newArray(int size) {
            return new Mentor[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flag) {
        dest.writeString(this.creationDate);
        dest.writeString(this.status);
        dest.writeString(this.fullName);
        dest.writeString(this.username);
        dest.writeString(this.photoUrl);
    }
}
