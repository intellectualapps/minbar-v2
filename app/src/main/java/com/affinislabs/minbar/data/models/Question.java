package com.affinislabs.minbar.data.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import java.util.Date;
import java.util.List;

/**
 * minbar-v2
 * Michael Obi
 * 25/01/2018, 1:01 PM
 */

public class Question implements Parcelable {

    private String asker;
    private String body;
    private Date dateAsked;
    private String id;
    private String fullName;
    private String photoUrl;
    private List<Category> categories;
    public static final Creator<Question> CREATOR = new Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel source) {
            return new Question(source);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };

    public Question() {
    }

    public String getAsker() {
        return this.asker;
    }

    public void setAsker(String asker) {
        this.asker = asker;
    }

    public String getBody() {
        return this.body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Nullable
    public Date getDateAsked() {
        return this.dateAsked;
    }

    public void setDateAsked(Date dateAsked) {
        this.dateAsked = dateAsked;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    private User writer;

    protected Question(Parcel in) {
        this.asker = in.readString();
        this.body = in.readString();
        long tmpDateAsked = in.readLong();
        this.dateAsked = tmpDateAsked == -1 ? null : new Date(tmpDateAsked);
        this.id = in.readString();
        this.fullName = in.readString();
        this.photoUrl = in.readString();
        this.categories = in.createTypedArrayList(Category.CREATOR);
        this.writer = in.readParcelable(User.class.getClassLoader());
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public boolean wasWrittenBy(String username) {
        return getAsker().equals(username);
    }

    public boolean wasWrittenBy(User user) {
        return wasWrittenBy(user.getUsername());
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public User getWriter() {
        return writer;
    }

    public void setWriter(User writer) {
        this.writer = writer;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.asker);
        dest.writeString(this.body);
        dest.writeLong(this.dateAsked != null ? this.dateAsked.getTime() : -1);
        dest.writeString(this.id);
        dest.writeString(this.fullName);
        dest.writeString(this.photoUrl);
        dest.writeTypedList(this.categories);
        dest.writeParcelable(this.writer, flags);
    }
}