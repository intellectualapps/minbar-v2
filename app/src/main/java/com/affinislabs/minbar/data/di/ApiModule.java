package com.affinislabs.minbar.data.di;

import android.content.SharedPreferences;

import com.affinislabs.minbar.BuildConfig;
import com.affinislabs.minbar.data.remote.DateDeserializer;
import com.affinislabs.minbar.data.remote.MinbarApiService;
import com.affinislabs.minbar.di.SharedPref;
import com.affinislabs.minbar.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * minbar-v2
 * Michael Obi
 * 11/03/2018, 4:01 PM
 */

@Module
public class ApiModule {

    @Provides
    HttpLoggingInterceptor provideLoggingInterceptor() {
        return new HttpLoggingInterceptor()
                .setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
    }

    @Provides
    OkHttpClient provideAuthenticatedOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor,
                                                  @SharedPref final SharedPreferences prefs) {
        return new OkHttpClient.Builder().addInterceptor(chain -> {
            String authToken = prefs.getString(Constants.PREF_AUTH_TOKEN, "");
            if (authToken.isEmpty()) {
                return chain.proceed(chain.request());
            }
            Request original = chain.request();
            Request.Builder requestBuilder = original.newBuilder()
                    .header("Authorization", "Bearer: " + authToken);
            Request request = requestBuilder.build();
            return chain.proceed(request);
        }).addInterceptor(httpLoggingInterceptor).build();
    }

    @Provides
    MinbarApiService provideApiService(OkHttpClient okHttpClient) {
        return createApiService(okHttpClient);
    }

    private MinbarApiService createApiService(OkHttpClient okHttpClient) {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(MinbarApiService.BASE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        return retrofit.create(MinbarApiService.class);
    }

}

