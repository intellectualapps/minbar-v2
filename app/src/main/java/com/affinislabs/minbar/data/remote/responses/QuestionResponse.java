package com.affinislabs.minbar.data.remote.responses;


import com.affinislabs.minbar.data.models.Category;
import com.affinislabs.minbar.data.models.Question;

import java.util.List;

/**
 * minbar-v2
 * Michael Obi
 * 25/01/2018, 12:09 PM
 */

public class QuestionResponse extends DefaultResponse {
    private List<Category> categories;
    private Question question;
    private List<Question> questions;

    public List<Category> getCategories() {
        return this.categories;
    }

    public void setCategories(List<Category> Category) {
        this.categories = Category;
    }

    public Question getQuestion() {
        return this.question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}
