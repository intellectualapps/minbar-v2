package com.affinislabs.minbar.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.affinislabs.minbar.data.remote.responses.SaveAnswerResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * minbar-v2
 * Michael Obi
 * 09/02/2018, 5:03 AM
 */

public class Answer implements Parcelable {

    @SerializedName("answerId")
    private String id;

    @SerializedName("questionId")
    private String questionId;

    @SerializedName("mentorUsername")
    private String mentorUsername;

    @SerializedName("mentorFullname")
    private String mentorFullname;

    @SerializedName("answerText")
    private String body;

    @SerializedName("dateAnswered")
    private Date dateAnswered;

    private String photoUrl;

    @Expose(deserialize = false, serialize = false)
    private Question question;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getMentorFullname() {
        return mentorFullname;
    }

    public void setMentorFullname(String mentorFullname) {
        this.mentorFullname = mentorFullname;
    }

    public Date getDateAnswered() {
        return dateAnswered;
    }

    public void setDateAnswered(Date dateAnswered) {
        this.dateAnswered = dateAnswered;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getMentorUsername() {
        return mentorUsername;
    }

    public void setMentorUsername(String mentorUsername) {
        this.mentorUsername = mentorUsername;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Answer() {
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
        questionId = question.getId();
    }

    public boolean wasWrittenBy(String username) {
        return getMentorUsername().equals(username);
    }

    public boolean wasWrittenBy(User user) {
        return wasWrittenBy(user.getUsername());
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.questionId);
        dest.writeString(this.mentorUsername);
        dest.writeString(this.mentorFullname);
        dest.writeString(this.body);
        dest.writeLong(this.dateAnswered != null ? this.dateAnswered.getTime() : -1);
        dest.writeString(this.photoUrl);
        dest.writeParcelable(this.question, flags);
    }

    protected Answer(Parcel in) {
        this.id = in.readString();
        this.questionId = in.readString();
        this.mentorUsername = in.readString();
        this.mentorFullname = in.readString();
        this.body = in.readString();
        long tmpDateAnswered = in.readLong();
        this.dateAnswered = tmpDateAnswered == -1 ? null : new Date(tmpDateAnswered);
        this.photoUrl = in.readString();
        this.question = in.readParcelable(Question.class.getClassLoader());
    }

    public static final Creator<Answer> CREATOR = new Creator<Answer>() {
        @Override
        public Answer createFromParcel(Parcel source) {
            return new Answer(source);
        }

        @Override
        public Answer[] newArray(int size) {
            return new Answer[size];
        }
    };

    public static Answer fromSaveAnswerResponse(SaveAnswerResponse saveAnswerResponse) {
        Answer answer = new Answer();
        answer.setId(saveAnswerResponse.getAnswerId());
        answer.setBody(saveAnswerResponse.getAnswerText());
        answer.setDateAnswered(saveAnswerResponse.getDateAnswered());
        answer.setMentorUsername(saveAnswerResponse.getMentorUsername());
        return answer;
    }
}
