package com.affinislabs.minbar.data.remote.responses;

/**
 * minbar
 * Michael Obi
 * 24/04/2018, 10:16 PM
 */
public class OpenQuestionsCountResponse extends DefaultResponse {
    private int numberOfOpenQuestions;

    public int getNumberOfOpenQuestions() {
        return numberOfOpenQuestions;
    }

    public void setNumberOfOpenQuestions(int numberOfOpenQuestions) {
        this.numberOfOpenQuestions = numberOfOpenQuestions;
    }
}
