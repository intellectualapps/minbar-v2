package com.affinislabs.minbar.data.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.affinislabs.minbar.data.remote.responses.RelatedUserResponse;
import com.affinislabs.minbar.data.remote.responses.UserDataResponse;
import com.google.gson.annotations.Expose;

import java.util.Set;

/**
 * minbar-v2
 * Michael Obi
 * 13 January 2018, 10:08 PM
 */

public class User implements Parcelable {
    private String email;
    private String username;

    @Expose(deserialize = false)
    private String authToken;
    private String creationDate;
    private String firstName;
    private String lastName;
    private String location;
    private String phoneNumber;
    private String photoUrl;
    private String tempKey;
    private String shortBio;
    private String linkedinProfile;
    private String facebookProfile;
    private String longBio;
    private boolean invited;
    private String relationshipToCaller;

    public User() {}

    public String getRelationshipToCaller() {
        return relationshipToCaller;
    }

    public void setRelationshipToCaller(String relationshipToCaller) {
        this.relationshipToCaller = relationshipToCaller;
    }

    @Expose(deserialize = false)
    private Set<String> mentorCategoryIds;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        String fullName;
        if (!TextUtils.isEmpty(getFirstName()) && TextUtils.isEmpty(getLastName()))
            fullName = getFirstName();
        else if (TextUtils.isEmpty(getFirstName()) && !TextUtils.isEmpty(getLastName()))
            fullName = getLastName();
        else if (!TextUtils.isEmpty(getFirstName()) && !TextUtils.isEmpty(getLastName()))
            fullName = getFirstName() + " " + getLastName();
        else
            fullName = "";

        return fullName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getTempKey() {
        return tempKey;
    }

    public void setTempKey(String tempKey) {
        this.tempKey = tempKey;
    }

    public boolean isInvited() {
        return invited;
    }

    public void setInvited(boolean invited) {
        this.invited = invited;
    }

    public String getShortBio() {
        return shortBio;
    }

    public void setShortBio(String shortBio) {
        this.shortBio = shortBio;
    }

    public String getLinkedinProfile() {
        return linkedinProfile;
    }

    public void setLinkedinProfile(String linkedinProfile) {
        this.linkedinProfile = linkedinProfile;
    }

    public String getFacebookProfile() {
        return facebookProfile;
    }

    public void setFacebookProfile(String facebookProfile) {
        this.facebookProfile = facebookProfile;
    }

    public static User fromUserDataResponse(UserDataResponse userDataResponse) {
        User user = new User();
        user.setEmail(userDataResponse.getEmail());
        user.setUsername(userDataResponse.getUsername());
        user.setCreationDate(userDataResponse.getCreationDate());
        user.setFirstName(userDataResponse.getFirstName());
        user.setLastName(userDataResponse.getLastName());
        user.setPhoneNumber(userDataResponse.getPhoneNumber());
        user.setLocation(userDataResponse.getLocation());
        user.setPhotoUrl(userDataResponse.getPhotoUrl());
        user.setInvited(userDataResponse.getInvited());
        user.setMentorCategoryIds(userDataResponse.getCategoryIds());
        user.setLongBio(userDataResponse.getLongBio());
        user.setShortBio(userDataResponse.getShortBio());
        user.setRelationshipToCaller(userDataResponse.getRelationshipToCaller());
        user.setFacebookProfile(userDataResponse.getFacebookProfile());
        user.setLinkedinProfile(userDataResponse.getLinkedinProfile());
        return user;
    }

    public static User fromRelatedUserResponse(RelatedUserResponse response) {
        User user = new User();
        user.setEmail(response.getEmail());
        user.setUsername(response.getUsername());
        user.setFirstName(response.getFirstName());
        user.setLastName(response.getLastName());
        user.setPhoneNumber(response.getPhoneNumber());
        user.setPhotoUrl(response.getPhotoUrl());
        user.setLongBio(response.getLongBio());
        user.setShortBio(response.getShortBio());
        return user;
    }

    public Set<String> getMentorCategoryIds() {
        return mentorCategoryIds;
    }

    public void setMentorCategoryIds(Set<String> mentorCategoryIds) {
        this.mentorCategoryIds = mentorCategoryIds;
    }

    public String getLongBio() {
        return longBio;
    }

    public void setLongBio(String longBio) {
        this.longBio = longBio;
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    protected User(Parcel in) {
        this.email = in.readString();
        this.username = in.readString();
        this.authToken = in.readString();
        this.creationDate = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.location = in.readString();
        this.phoneNumber = in.readString();
        this.photoUrl = in.readString();
        this.tempKey = in.readString();
        this.shortBio = in.readString();
        this.linkedinProfile = in.readString();
        this.facebookProfile = in.readString();
        this.longBio = in.readString();
        this.invited = in.readByte() != 0;
        this.relationshipToCaller = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.email);
        dest.writeString(this.username);
        dest.writeString(this.authToken);
        dest.writeString(this.creationDate);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.location);
        dest.writeString(this.phoneNumber);
        dest.writeString(this.photoUrl);
        dest.writeString(this.tempKey);
        dest.writeString(this.shortBio);
        dest.writeString(this.linkedinProfile);
        dest.writeString(this.facebookProfile);
        dest.writeString(this.longBio);
        dest.writeByte(this.invited ? (byte) 1 : (byte) 0);
        dest.writeString(this.relationshipToCaller);
    }
}
