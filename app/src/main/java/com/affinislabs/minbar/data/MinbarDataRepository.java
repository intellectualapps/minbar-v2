package com.affinislabs.minbar.data;

import android.net.Uri;

import com.affinislabs.minbar.data.local.LocalDataSource;
import com.affinislabs.minbar.data.models.Answer;
import com.affinislabs.minbar.data.models.AnswerReply;
import com.affinislabs.minbar.data.models.Category;
import com.affinislabs.minbar.data.models.Mentor;
import com.affinislabs.minbar.data.models.Question;
import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.data.remote.MinbarApiService;
import com.affinislabs.minbar.data.remote.RemoteCallback;
import com.affinislabs.minbar.data.remote.responses.AdvisorResponse;
import com.affinislabs.minbar.data.remote.responses.AnswersResponse;
import com.affinislabs.minbar.data.remote.responses.CategoryResponse;
import com.affinislabs.minbar.data.remote.responses.MenteeDecisionResponse;
import com.affinislabs.minbar.data.remote.responses.MentorOptInResponse;
import com.affinislabs.minbar.data.remote.responses.OpenQuestionsCountResponse;
import com.affinislabs.minbar.utils.Constants;
import com.affinislabs.minbar.utils.ImageUploader;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.apache.commons.lang3.StringUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import durdinapps.rxfirebase2.DataSnapshotMapper;
import durdinapps.rxfirebase2.RxFirebaseDatabase;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

/**
 * minbar-v2
 * Michael Obi
 * 23/01/2018, 10:41 AM
 */

public class MinbarDataRepository {

    private MinbarApiService apiService;
    private LocalDataSource localDataSource;
    private DatabaseReference database = FirebaseDatabase.getInstance().getReference();

    @Inject
    public MinbarDataRepository(MinbarApiService apiService, LocalDataSource localDataSource) {
        this.apiService = apiService;
        this.localDataSource = localDataSource;
    }

    public Single<List<Category>> getAllCategories() {
        return apiService.getAllCategories()
                .map(CategoryResponse::getCategories);
    }

    public Single<List<Category>> saveMentorCategories(Collection<String> categories) {
        String categoryIds = StringUtils.join(categories, ",");
        return apiService.mentorOptIn(categoryIds)
                .map(MentorOptInResponse::getCategories);
    }

    public Flowable<Question> saveQuestion(String question, Collection<String> categories) {
        String categoryIds = StringUtils.join(categories, ",");
        return apiService.saveQuestion(question, categoryIds)
                .map(questionResponse -> {
                    Question questionObject = questionResponse.getQuestion();
                    questionObject.setCategories(questionResponse.getCategories());
                    return questionObject;
                });
    }

    public Single<List<Question>> getQuestionsUserAsked() {
        return apiService.getQuestionsUserAsked()
                .flatMapObservable(questionResponse -> Observable.fromIterable(questionResponse.getQuestions()))
                .flatMapSingle(this::mergeQuestionWithWriter).toList();
    }

    private Single<Question> mergeQuestionWithWriter(Question question) {
        return apiService.fetchUserProfile(question.getAsker())
                .flatMap(relatedUserResponse -> Single.just(User.fromRelatedUserResponse(relatedUserResponse)))
                .map(user -> {
                    question.setWriter(user);
                    return question;
                });
    }

    public Single<List<Question>> getQuestionsUserAnswer() {
        return apiService.getQuestionsUserAnswer()
                .flatMapObservable(questionResponse -> Observable.fromIterable(questionResponse.getQuestions()))
                .flatMapSingle(this::mergeQuestionWithWriter).toList();
    }

    public Single<List<User>> getMentorsForCategories(Question question) {
        return apiService.getRelevantMentors(question.getId())
                .map(relevantMentorsResponses -> relevantMentorsResponses.getRelevantMentors()
                        .get(0));
    }

    public Completable saveSelectedQuestionMentors(String questionId, Collection<String> selectedMentorIds) {
        String mentorIds = StringUtils.join(selectedMentorIds, ",");
        return apiService.pointQuestionToSelectedMentors(questionId, mentorIds);
    }

    public Flowable<List<Mentor>> getAdvisors() {
        return apiService.getAdvisors()
                .map(AdvisorResponse::getAdvisors);
    }

    public Flowable<List<Mentor>> getMentees() {
        return apiService.getMentees().map(AdvisorResponse::getMentees);
    }

    public void acceptMenteeRequest(String menteeUsername, MentorDecisionListener mentorDecisionListener) {
        apiService.acceptMenteeRequest(menteeUsername)
                .enqueue(new RemoteCallback<MenteeDecisionResponse>() {
                    @Override
                    public void onSuccess(MenteeDecisionResponse response) {
                        mentorDecisionListener.onMentorDecisionMade(response.getState());
                    }

                    @Override
                    public void onFailed(Throwable throwable) {
                    }
                });
    }

    public void declineMenteeRequest(String menteeUsername, MentorDecisionListener mentorDecisionListener) {
        apiService.declineMenteeRequest(menteeUsername)
                .enqueue(new RemoteCallback<MenteeDecisionResponse>() {
                    @Override
                    public void onSuccess(MenteeDecisionResponse response) {
                        mentorDecisionListener.onMentorDecisionMade(response.getState());
                    }

                    @Override
                    public void onFailed(Throwable throwable) {
                    }
                });
    }

    public Single<User> getSecondPartyPublicProfile(String secondPartyeUsername) {
        return apiService.getSecondPartyPublicProfile(secondPartyeUsername)
                .map(User::fromUserDataResponse);
    }

    public Flowable<List<Answer>> getAnswersForQuestion(Question question) {
        return getAnswersForQuestion(question.getId())
                .flatMapIterable(answers -> answers)
                .doOnNext(answer -> answer.setQuestion(question))
                .toList().toFlowable();
    }

    public Flowable<List<Answer>> getAnswersForQuestion(String questionId) {
        return apiService.getAnswersForQuestion(questionId)
                .map(AnswersResponse::getAnswers)
                .flatMapIterable(answers -> answers)
                .toList().toFlowable();
    }

    public Single<Answer> saveQuestionAnswer(String answerText, Question question) {
        return apiService.saveAnswer(question.getId(), answerText)
                .flatMap(saveAnswerResponse -> Single.just(Answer.fromSaveAnswerResponse(saveAnswerResponse)))
                .flatMap(answer -> apiService.fetchUserProfile(answer.getMentorUsername())
                        .flatMap(relatedUserResponse -> Single.just(User.fromRelatedUserResponse(relatedUserResponse)))
                        .map(user -> {
                            answer.setMentorFullname(user.getFullName());
                            answer.setQuestion(question);
                            return answer;
                        }));
    }

    public Flowable<List<AnswerReply>> observeAnswerThread(String answerId) {
        DatabaseReference threadReference = database.child("answersReplies").child(answerId);
        return RxFirebaseDatabase.observeValueEvent(threadReference,
                DataSnapshotMapper.listOf(AnswerReply.class));
    }

    public Completable addAnswerReply(String answerId, String text, String authorId) {
        DatabaseReference reference = database.child("answersReplies").child(answerId).push();
        AnswerReply answerReply = new AnswerReply();
        answerReply.setText(text);
        answerReply.setUsername(authorId);
        return RxFirebaseDatabase.setValue(reference, answerReply);
    }

    public Single<User> loadLoggedInUser() {
        return apiService.getLoggedInUser()
                .map(User::fromUserDataResponse);
    }

    public Single<User> saveProfile(String firstName, String lastName, String phoneNumber, String emailAddress,
                                    String location, String longBio, Set<String> selectedCategoryIds) {
        Map<String, String> profileData = new HashMap<>(7);
        profileData.put(Constants.KEY_EDIT_PROFILE_FIRST_NAME, firstName);
        profileData.put(Constants.KEY_EDIT_PROFILE_LAST_NAME, lastName);
        profileData.put(Constants.KEY_EDIT_PROFILE_PHONE, phoneNumber);
        profileData.put(Constants.KEY_EDIT_PROFILE_EMAIL, emailAddress);
        profileData.put(Constants.KEY_EDIT_PROFILE_LOCATION, location);
        profileData.put(Constants.KEY_EDIT_PROFILE_LONG_BIO, longBio);
        String categoryIds = StringUtils.join(selectedCategoryIds, ",");
        profileData.put(Constants.KEY_EDIT_PROFILE_MENTOR_CATEGORY_IDS, categoryIds);
        return saveProfile(profileData);
    }

    public Single<User> saveProfile(Map<String, String> profileData) {
        return saveProfile(profileData, false);
    }

    public Single<User> saveProfile(Map<String, String> profileData, boolean sendEmptyData) {
        if (!sendEmptyData) {
            Map<String, String> data = new HashMap<>();
            for (Map.Entry<String, String> entry : profileData.entrySet()) {
                if (entry.getValue() != null && !entry.getValue().isEmpty()) {
                    data.put(entry.getKey(), entry.getValue());
                }
            }
            return apiService.saveProfile(data).map(User::fromUserDataResponse)
                    .doOnSuccess(user -> localDataSource.saveUserData(user));
        }
        return apiService.saveProfile(profileData).map(User::fromUserDataResponse)
                .doOnSuccess(user -> localDataSource.saveUserData(user));
    }

    public Single<User> createUsername(String username, String email, String tempKey) {
        return apiService.createUsername(username, email, tempKey)
                .map(userDataResponse -> {
                    User user = User.fromUserDataResponse(userDataResponse);
                    user.setAuthToken(userDataResponse.getAuthToken());
                    return user;
                });
    }

    public Completable doneWithAnswer(Answer answer) {
        return apiService.markQuestionAsDone(answer.getQuestionId());
    }

    public Completable happyWithAnswer(Answer answer) {
        return apiService.markHappyWithAnswer(answer.getQuestionId());
    }

    public Completable inviteAdvisor(String advisorUsername) {
        return apiService.inviteAdvisor(advisorUsername);
    }


    public Single<User> connectFacebookProfile(String email, String profileUrl, String photoUrl) {
        Map<String, String> profileData = new HashMap<>();
        profileData.put(Constants.KEY_FACEBOOK_EMAIL, email);
        profileData.put(Constants.KEY_FACEBOOK_PROFILE, profileUrl);
        profileData.put(Constants.KEY_PHOTO_URL, photoUrl);
        return saveProfile(profileData);
    }

    public Single<User> disconnectFacebook() {
        Map<String, String> profileData = new HashMap<>();
        profileData.put(Constants.KEY_FACEBOOK_EMAIL, "");
        profileData.put(Constants.KEY_FACEBOOK_PROFILE, "");
        return saveProfile(profileData, true);
    }

    public Completable rateMentor(String username, float rating) {
        return apiService.rateMentor(username, rating);
    }

    public Completable deleteMentorQuestion(String questionId) {
        return apiService.deleteMentorQuestion(questionId);
    }

    public Completable deleteMyQuestion(String questionId) {
        return apiService.deleteMyQuestion(questionId);
    }

    public Completable updateFcmToken(String username, String fcmToken, String deviceUuid) {
        return apiService.updateFcmToken(username, fcmToken, deviceUuid, Constants.FCM_PLATFORM);
    }

    public Single<User> uploadProfilePicture(Uri resultUri) {
        return ImageUploader.upload(resultUri)
                .map(resultMap -> resultMap.get("url"))
                .flatMap(url -> {
                    Map<String, String> data = new HashMap<>(1);
                    data.put(Constants.KEY_PHOTO_URL, url);
                    return saveProfile(data).subscribeOn(Schedulers.io());
                });
    }

    public void saveAuthToken(String authToken) {
        localDataSource.saveAuthToken(authToken);
    }

    public void clearLoggedInUserData() {
        localDataSource.clearLoggedInUserData();
    }

    public boolean getLoginStatus() {
        return localDataSource.getLoginStatus();
    }

    public void saveUserData(User user) {
        localDataSource.saveUserData(user);
    }

    public User getLoggedInUser() {
        return localDataSource.getUser();
    }

    public void saveUserProfile(User user) {
        localDataSource.saveUserProfile(user);
    }

    public Completable markQuestionAsRead(String questionId) {
        return apiService.markQuestionAsRead(questionId);
    }

    public Single<Integer> getOpenQuestionCount() {
        return apiService.getOpenQuestionCount()
                .map(OpenQuestionsCountResponse::getNumberOfOpenQuestions);
    }

    public interface MentorDecisionListener {
        void onMentorDecisionMade(boolean decision);
    }
}
