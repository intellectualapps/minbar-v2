package com.affinislabs.minbar.data.remote.responses;

import java.util.Date;

/**
 * minbar-v2
 * Michael Obi
 * 06/03/2018, 4:57 PM
 */

public class SaveAnswerResponse extends DefaultResponse {
    private String answerId;

    private String answerText;

    private Date dateAnswered;

    private String mentorUsername;

    private String questionId;

    public String getAnswerId() {
        return this.answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getAnswerText() {
        return this.answerText;
    }

    public void setAnswerText(String answerText) {
        this.answerText = answerText;
    }

    public Date getDateAnswered() {
        return this.dateAnswered;
    }

    public void setDateAnswered(Date dateAnswered) {
        this.dateAnswered = dateAnswered;
    }

    public String getMentorUsername() {
        return this.mentorUsername;
    }

    public void setMentorUsername(String mentorUsername) {
        this.mentorUsername = mentorUsername;
    }

    public String getQuestionId() {
        return this.questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }
}
