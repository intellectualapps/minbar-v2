package com.affinislabs.minbar.data.remote.responses;

import com.affinislabs.minbar.data.models.Answer;

import java.util.List;

/**
 * minbar-v2
 * Michael Obi
 * 14/02/2018, 8:48 AM
 */

public class AnswersResponse extends DefaultResponse {
    private List<Answer> answers;

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }
}
