package com.affinislabs.minbar.data.remote;

import com.affinislabs.minbar.data.remote.responses.DefaultResponse;

import java.net.HttpURLConnection;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * minbar-v2
 * Michael Obi
 * 11 January 2018, 3:40 PM
 */

public abstract class RemoteCallback<T extends DefaultResponse> implements Callback<T> {
    @Override
    public final void onResponse(Call<T> call, Response<T> response) {
        switch (response.code()) {
            case HttpsURLConnection.HTTP_OK:
            case HttpsURLConnection.HTTP_CREATED:
            case HttpsURLConnection.HTTP_ACCEPTED:
            case HttpsURLConnection.HTTP_NOT_AUTHORITATIVE:
                handleSuccess(response);
                break;
            default:
                onFailed(new Throwable("Default " + response.code() + " " + response.message()));
        }
    }

    private void handleSuccess(Response<T> response) {
        T body = response.body();
        if (body == null) {
            onFailed(new Throwable("Response contains no body"));
            return;
        }

        if (body.getCode() != null && !body.getCode().isEmpty()) {
            onFailed(new Throwable(body.getMessage()));
            return;
        }
        onSuccess(response.body());
    }

    @Override
    public final void onFailure(Call<T> call, Throwable t) {
        onFailed(t);
    }

    public abstract void onSuccess(T response);

    public abstract void onFailed(Throwable throwable);
}