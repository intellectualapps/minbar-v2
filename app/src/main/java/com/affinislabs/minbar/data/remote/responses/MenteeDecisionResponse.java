package com.affinislabs.minbar.data.remote.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created
 * by User
 * on 2/19/2018.
 */

public class MenteeDecisionResponse extends DefaultResponse {
    @SerializedName("state")
    @Expose
    private boolean state;

    public boolean getState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}
