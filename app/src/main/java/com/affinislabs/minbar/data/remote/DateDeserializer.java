package com.affinislabs.minbar.data.remote;

import android.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;



/**
 * minbar-v2
 * Michael Obi
 * 30/03/2018, 10:43 AM
 */
public  class DateDeserializer implements JsonDeserializer<Date> {
    private static final String TAG = "DateDeserializer";

    @Override
    public Date deserialize(JsonElement element, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        String dateString = element.getAsString();
        String dateFormat = "yyyy-MM-dd'T'HH:mm:ss";
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        TimeZone serverTimeZone = TimeZone.getTimeZone("GMT");
        formatter.setTimeZone(serverTimeZone);
        try {
            return formatter.parse(dateString);
        } catch (ParseException e) {
            Log.e(TAG, e.getMessage(), e);
            return null;
        }
    }
}

