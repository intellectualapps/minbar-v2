package com.affinislabs.minbar.utils;

/**
 * Created
 * by User
 * on 3/1/2018.
 */

public class Constants {
    public static final String SECOND_PARTY_USERNAME = "second_party_username";
    public static final String USER_SHARED_PREFERENCES = "USER_SHARED_PREFERENCES";
    public static final String PREF_LOGGED_IN_STATUS = "logged_in_status";
    public static final String PREF_AUTH_TOKEN = "auth_token";
    public static final String PREF_USER_DATA = "user_data";
    public static final String AUTH_EMAIL = "email";
    public static final String AUTH_FACEBOOK = "facebook";
    public static final String AUTH_LINKEDIN = "linkedin";
    public static final int REQUEST_CODE_INTRO = 101;
    public static final String KEY_HAS_SEEN_BECOME_MENTOR_SCREEN =
            "KEY_HAS_SEEN_BECOME_MENTOR_SCREEN";
    public static final String KEY_DEVICE_ID =
            "KEY_DEVICE_ID";

    public static final String ADVISORS = "advisors";
    public static final String MENTEES = "mentees";
    public static final String NOT_CONNECTED = "NOT_CONNECTED";
    public static final String ACCEPT = "accept";
    public static final String DECLINE = "decline";
    public static final String NULL_RESPONSE = "Null returned by server";
    public static final String ACCEPT_ERROR = "Accept advisor processing error";
    public static final String DECLINE_ERROR = "Decline advisor processing error";
    public static final String INVITATION_SENT = "Invitation sent";
    public static final String TODAY = "Today";

    public static final String KEY_EDIT_PROFILE_EMAIL = "email";
    public static final String KEY_EDIT_PROFILE_FIRST_NAME = "first-name";
    public static final String KEY_EDIT_PROFILE_LAST_NAME = "last-name";
    public static final String KEY_EDIT_PROFILE_PHONE = "phone-number";
    public static final String KEY_EDIT_PROFILE_LOCATION = "location";
    public static final String KEY_EDIT_PROFILE_GENDER = "gender";
    public static final String KEY_EDIT_PROFILE_LONG_BIO = "long-bio";
    public static final String KEY_EDIT_PROFILE_SHORT_BIO = "short-bio";
    public static final String KEY_EDIT_PROFILE_MENTOR_CATEGORY_IDS = "category-ids";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_FACEBOOK_EMAIL = "facebook-email";
    public static final String KEY_FACEBOOK_PROFILE= "facebook-profile";
    public static final String KEY_LINKED_IN_EMAIL = "linkedin-email";

    public static final String KEY_PHOTO_URL = "photo-url";

    public static final String ACTIVITY_RESULT_DONE_WITH_ANSWER = "DONE_WITH_ANSWER";
    public static final int THREADS_ACTIVITY_REQUEST_CODE = 560;
    public static final int ANSWERS_ACTIVITY_REQUEST_CODE = 560;

    public static final String QUESTION = "question";
    public static final String ANSWER = "answer";

    public static final String FCM_PLATFORM = "1";

    public static final String MA_TERMS_URL="https://minbarapp-android.appspot.com/ma-tos.html";
    public static final String MA_PRIVACY_POLICY_URL="https://minbarapp-android.appspot.com/ma-pp.html";
    public static final String NEW_ADVISOR_ACTION = "com.affinislabs.minbar.NEW_ADVISOR_ACTION";
}
