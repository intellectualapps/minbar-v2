package com.affinislabs.minbar.utils;

import android.app.Activity;
import android.util.Log;

import com.google.android.gms.common.GoogleApiAvailability;

/**
 * minbar-v2
 * Michael Obi
 * 09/04/2018, 5:12 PM
 */
public class PlayServicesUtil {
    private static final String TAG = "PlayServicesUtil";

    public static void checkPlayServices(Activity activity) {

        GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(activity)
                .addOnCompleteListener(activity, task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "onComplete: Play Services OKAY");
                    }
                });
    }
}
