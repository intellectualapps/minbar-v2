package com.affinislabs.minbar.utils;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

public class FontUtils {
    public static int STYLE_LIGHT = 3;
    public static int STYLE_REGULAR = 0;
    public static int STYLE_BOLD = 2;
    public static int STYLE_ITALIC = 4;
    public static int STYLE_MEDIUM = 1;
    private static String FONT_LIGHT = "AvenirLTStd-Light.otf";
    private static String FONT_REGULAR = "AvenirLTStd-Book.otf";
    private static String FONT_BOLD = "AvenirLTStd-Heavy.otf";
    private static String FONT_MEDIUM = "AvenirLTStd-Medium.otf";
    private static String FONT_ITALIC = "AvenirLTStd-Oblique.otf";
    private static Map<String, Typeface> sCachedFonts = new HashMap<String, Typeface>();

    public static Typeface getTypeface(Context context, String assetPath) {
        if (!sCachedFonts.containsKey(assetPath)) {
            Typeface tf = Typeface.createFromAsset(context.getAssets(), assetPath);
            sCachedFonts.put(assetPath, tf);
        }
        return sCachedFonts.get(assetPath);
    }

    public static Typeface selectTypeface(Context context, int textStyle) {
        String fontDirectory = "Avenir/";
        String font;
        switch (textStyle) {
            default:
            case 0:
                font = FontUtils.FONT_REGULAR;
                break;
            case 1:
                font = FontUtils.FONT_MEDIUM;
                break;
            case 2:
                font = FontUtils.FONT_BOLD;
                break;
            case 3:
                font = FontUtils.FONT_LIGHT;
                break;
            case 4:
                font = FontUtils.FONT_ITALIC;
                break;
        }

        return FontUtils.getTypeface(context, fontDirectory + font);
    }
}
