package com.affinislabs.minbar.utils;

import android.app.Activity;
import android.support.annotation.IntDef;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.affinislabs.minbar.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.LENGTH_SHORT;

/**
 * Minbar
 * Michael Obi
 * 07 January 2018, 12:39 PM
 */

public class ToastUtil {

    @IntDef({LENGTH_SHORT, LENGTH_LONG})
    @Retention(RetentionPolicy.SOURCE)
    @interface Duration {}

    /**
     * @param activity
     * @param text
     * @param duration
     */
    public static void showToast(Activity activity, String text, @Duration int duration) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(R.layout.notification_popup,
                activity.findViewById(R.id.notification_view_container));
        TextView toastMessage = layout.findViewById(R.id.notification_message);
        toastMessage.setText(text);
        Toast toast = new Toast(activity);
        toast.setGravity(Gravity.TOP, 0, 0);
        toast.setDuration(duration);
        toast.setView(layout);
        toast.show();
    }
}
