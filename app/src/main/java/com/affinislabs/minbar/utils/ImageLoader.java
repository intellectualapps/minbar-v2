package com.affinislabs.minbar.utils;

import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * minbar-v2
 * Michael Obi
 * 14/02/2018, 10:48 AM
 */

public class ImageLoader {

    public static void loadImage(ImageView imageView, String url,
                                 @DrawableRes int defaultImage) {
        if (url != null && !url.isEmpty()) {
            Picasso.with(imageView.getContext()).load(url).error(defaultImage).into(imageView);
        }
    }

    public static void loadImage(ImageView imageView, String url, Drawable defaultImage) {
        if (url != null && !url.isEmpty()) {
            Picasso.with(imageView.getContext()).load(url).error(defaultImage).into(imageView);
        }
    }
}
