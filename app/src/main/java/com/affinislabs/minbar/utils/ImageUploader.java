package com.affinislabs.minbar.utils;

import android.net.Uri;
import android.util.Log;

import com.cloudinary.android.MediaManager;
import com.cloudinary.android.callback.ErrorInfo;
import com.cloudinary.android.callback.UploadCallback;

import java.util.Locale;
import java.util.Map;

import io.reactivex.Single;

/**
 * minbar-v2
 * Michael Obi
 * 15/04/2018, 7:19 PM
 */
public class ImageUploader {
    private static final String TAG = "ImageUploadCallback";

    public static Single<Map<String, String>> upload(Uri uri) {
        return Single.create(emitter -> {
            UploadCallback uploadCallback = new UploadCallback() {
                @Override
                public void onSuccess(String requestId, Map resultData) {
                    emitter.onSuccess(resultData);
                }

                @Override
                public void onError(String requestId, ErrorInfo error) {
                    emitter.onError(new Exception(String.format(Locale.getDefault(), "%d: %s", error.getCode(),
                            error.getDescription())));
                }

                @Override
                public void onReschedule(String requestId, ErrorInfo error) {
                    emitter.onError(new Exception(String.format(Locale.getDefault(), "%d: %s", error.getCode(),
                            error.getDescription())));
                }

                @Override
                public void onStart(String requestId) {
                    Log.d(TAG, String.format("Image upload started: requestId = %s", requestId));
                }

                @Override
                public void onProgress(String requestId, long bytes, long totalBytes) {
                    Log.d(TAG, String.format("Image Upload Progress: requestId = %s, bytes = %d, totalBytes = %d", requestId, bytes, totalBytes));
                }

            };

            MediaManager.get().upload(uri).callback(uploadCallback).dispatch();
        });
    }

}
