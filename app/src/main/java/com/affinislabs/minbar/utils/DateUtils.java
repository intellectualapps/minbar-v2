package com.affinislabs.minbar.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import static android.text.format.DateUtils.DAY_IN_MILLIS;
import static android.text.format.DateUtils.WEEK_IN_MILLIS;
import static android.text.format.DateUtils.YEAR_IN_MILLIS;
import static android.text.format.DateUtils.getRelativeTimeSpanString;

/**
 * Created
 * by User
 * on 2/21/2018.
 */

public class DateUtils {

    public static String getRelativeDate(long timestamp) {
        String relativeDate = "";
        long timeDiff = System.currentTimeMillis() - timestamp;
        if (timeDiff >= YEAR_IN_MILLIS) {
            relativeDate = getRelativeTimeSpanString(timestamp, GregorianCalendar.getInstance().getTimeInMillis(), YEAR_IN_MILLIS).toString();
        } else if (timeDiff >= (DAY_IN_MILLIS * 30)) {
            int month = (int) (timeDiff / (DAY_IN_MILLIS * 30));
            relativeDate = (month > 1) ? month + "months ago" : month + " month ago";
        } else if (timeDiff >= WEEK_IN_MILLIS) {
            relativeDate = getRelativeTimeSpanString(timestamp, GregorianCalendar.getInstance().getTimeInMillis(), WEEK_IN_MILLIS).toString();
        } else if (timeDiff >= DAY_IN_MILLIS) {
            relativeDate = getRelativeTimeSpanString(timestamp, GregorianCalendar.getInstance().getTimeInMillis(), DAY_IN_MILLIS).toString();
        } else {
            relativeDate = Constants.TODAY;
        }

        return relativeDate;
    }

    public static String getRelativeDateText(Date date) {
        return android.text.format.DateUtils
                .getRelativeTimeSpanString(date.getTime(), System.currentTimeMillis(),
                        android.text.format.DateUtils.MINUTE_IN_MILLIS, SimpleDateFormat.SHORT).toString();

    }

    public static String getShortTime(Date date) {
        return new SimpleDateFormat("h:mm a", Locale.getDefault()).format(date).toLowerCase();
    }

}
