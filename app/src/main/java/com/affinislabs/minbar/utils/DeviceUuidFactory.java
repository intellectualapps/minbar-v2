package com.affinislabs.minbar.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

/**
 *
 */
public class DeviceUuidFactory {
    protected volatile static UUID uuid;

    public DeviceUuidFactory(Context context) {
        if (uuid == null) {
            synchronized (DeviceUuidFactory.class) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

                if (uuid == null) {
                    final String id = preferences.getString(Constants.KEY_DEVICE_ID, null);
                    if (id != null) {
                        uuid = UUID.fromString(id);
                    } else {
                        final String androidID = Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
                        try {
                            uuid = UUID.nameUUIDFromBytes(androidID.getBytes("utf-8"));
                        } catch (UnsupportedEncodingException e) {
                            throw new RuntimeException(e);
                        }

                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString(Constants.KEY_DEVICE_ID, uuid.toString());
                        editor.apply();
                    }
                }
            }
        }
    }

    public UUID getUUID() {
        return uuid;
    }

    public String getStringUUID() {
        return uuid.toString();
    }

}
