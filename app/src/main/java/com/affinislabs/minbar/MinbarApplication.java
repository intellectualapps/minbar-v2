package com.affinislabs.minbar;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.affinislabs.minbar.di.AppComponent;
import com.affinislabs.minbar.di.AppModule;
import com.affinislabs.minbar.di.DaggerAppComponent;
import com.affinislabs.minbar.utils.DeviceUuidFactory;
import com.cloudinary.android.MediaManager;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.HashMap;
import java.util.Map;


public class MinbarApplication extends MultiDexApplication {
    private static String deviceUuid;
    private static AppComponent appComponent;
    private static final String TAG = "MinbarApplication";
    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
        MediaManager.init(this);
        DeviceUuidFactory deviceUuidFactory = new DeviceUuidFactory(this);
        deviceUuid = deviceUuidFactory.getStringUUID();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    protected void attachBaseContext(Context context) {
        MultiDex.install(context);
        super.attachBaseContext(context);
    }

    public static String getDeviceUuid() {
        return deviceUuid;
    }
}
