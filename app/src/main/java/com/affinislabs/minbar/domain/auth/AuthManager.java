package com.affinislabs.minbar.domain.auth;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;
import android.util.Log;

import com.affinislabs.minbar.MinbarApplication;
import com.affinislabs.minbar.data.MinbarDataRepository;
import com.affinislabs.minbar.data.models.User;
import com.affinislabs.minbar.data.remote.MinbarApiService;
import com.affinislabs.minbar.data.remote.RemoteCallback;
import com.affinislabs.minbar.data.remote.responses.AuthResponse;
import com.affinislabs.minbar.data.remote.responses.UsernameCheckResponse;
import com.affinislabs.minbar.ui.auth.AuthActivity;
import com.affinislabs.minbar.utils.Constants;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;


/**
 * minbar-v2
 * Michael Obi
 * 16 January 2018, 7:20 AM
 */

@SuppressWarnings("WeakerAccess")
public class AuthManager {

    private static final String TAG = "AuthManager";

    private MinbarApiService minbarApiService;

    private MinbarDataRepository repository;

    @Inject
    public AuthManager(MinbarApiService minbarApiService,
                       MinbarDataRepository repository) {
        this.minbarApiService = minbarApiService;
        this.repository = repository;
    }

    public Single<Boolean> checkUsernameValidity(String username) {
        return minbarApiService.checkUserNameValidity(username)
                .map(UsernameCheckResponse::getStatus);
    }

    public void register(String username, String email, String password, AuthListener listener) {
        minbarApiService.createAccount(username, email, password).enqueue(new RemoteCallback<AuthResponse>() {
            @Override
            public void onSuccess(AuthResponse response) {
                saveUserData(response);
                listener.onAuthSuccess();
            }

            @Override
            public void onFailed(Throwable throwable) {
                listener.onAuthError(throwable.getMessage());
            }
        });
    }

    public void saveUserData(AuthResponse response) {
        repository.saveAuthToken(response.getAuthToken());
        User user = User.fromUserDataResponse(response);
        user.setAuthToken(response.getAuthToken());
        repository.saveUserData(user);
    }

    public User getUser() {
        return repository.getLoggedInUser();
    }

    public boolean isUserLoggedIn() {
        return repository.getLoginStatus();
    }

    public void loginWithUsername(String username, String password, AuthListener authListener) {
        minbarApiService.login(Constants.AUTH_EMAIL, username, password).enqueue(new RemoteCallback<AuthResponse>() {
            @Override
            public void onSuccess(AuthResponse response) {
                saveUserData(response);
                updateFcmToken(response.getUsername());
                authListener.onAuthSuccess();
            }

            @Override
            public void onFailed(Throwable throwable) {
                authListener.onAuthError(throwable.getMessage());
            }
        });
    }

    public void logout() {
        repository.clearLoggedInUserData();
    }

    public void loginWithSocialAccount(@NonNull JSONObject socialProfileData,
                                       @AuthMethod String authMethod,
                                       SocialAuthListener authListener) {
        String email = socialProfileData.optString("email");
        minbarApiService.login(authMethod, email).enqueue(new RemoteCallback<AuthResponse>() {
            @Override
            public void onSuccess(AuthResponse response) {
                repository.saveAuthToken(response.getAuthToken());
                if (response.getUsername() != null && !response.getUsername().isEmpty()) {
                    saveUserData(response);
                    updateFcmToken(response.getUsername());
                }
                authListener.onSocialAuthSuccess(email, response, authMethod, socialProfileData);
            }

            @Override
            public void onFailed(Throwable throwable) {
                authListener.onSocialAuthError(throwable.getMessage());
            }
        });
    }

    @SuppressLint("CheckResult")
    public void updateFcmToken(String username) {
        String fcmToken = FirebaseInstanceId.getInstance().getToken();
        if (fcmToken != null) {
            repository.updateFcmToken(username, fcmToken, MinbarApplication.getDeviceUuid())
                    .subscribeOn(Schedulers.io())
                    .subscribe(() -> Log.d(TAG, "Firebase Token updated"));
        }
    }


    public Single<User> updateProfileFromFacebook(String username, JSONObject facebookProfileData) {
        String name = facebookProfileData.optString("name");
        String email = facebookProfileData.optString("email");
        String profileUrl = facebookProfileData.optString("link");

        String firstName, lastName;
        if (name.contains(" ")) {
            String[] split_names = name.split(" ");
            firstName = split_names[0];
            lastName = split_names[1];
        } else {
            firstName = name;
            lastName = "";
        }

        String photoUrl = null;
        try {
            if (facebookProfileData.has("picture")) {
                photoUrl = facebookProfileData.getJSONObject("picture").getJSONObject("data").getString("url");
            }
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage());
        }

        Map<String, String> profileData = new HashMap<>();
        profileData.put(Constants.KEY_EDIT_PROFILE_FIRST_NAME, firstName);
        profileData.put(Constants.KEY_EDIT_PROFILE_LAST_NAME, lastName);
        profileData.put(Constants.KEY_USERNAME, username);
        profileData.put(Constants.KEY_FACEBOOK_EMAIL, email);
        profileData.put(Constants.KEY_FACEBOOK_PROFILE, profileUrl);
        profileData.put(Constants.KEY_PHOTO_URL, photoUrl);
        return repository.saveProfile(profileData);
    }

    public Single<User> updateProfileFromLinkedIn(String username, JSONObject linkedInProfileData) {
        String email = linkedInProfileData.optString("emailAddress");
        String firstName = linkedInProfileData.optString("firstName");
        String lastName = linkedInProfileData.optString("lastName");
        String photoUrl = linkedInProfileData.optString("pictureUrl");
        Map<String, String> profileData = new HashMap<>();
        profileData.put(Constants.KEY_EDIT_PROFILE_FIRST_NAME, firstName);
        profileData.put(Constants.KEY_EDIT_PROFILE_LAST_NAME, lastName);
        profileData.put(Constants.KEY_USERNAME, username);
        profileData.put(Constants.KEY_LINKED_IN_EMAIL, email);
        profileData.put(Constants.KEY_PHOTO_URL, photoUrl);
        return repository.saveProfile(profileData);
    }

    public interface AuthListener {

        void onAuthSuccess();

        void onAuthError(String errorMessage);
    }

    public interface SocialAuthListener {

        void onSocialAuthSuccess(String email, AuthResponse response,
                                 @AuthMethod String authMethod, JSONObject socialProfileData);

        void onSocialAuthError(String errorMessage);
    }

    @StringDef({Constants.AUTH_EMAIL, Constants.AUTH_FACEBOOK, Constants.AUTH_LINKEDIN})
    @Retention(RetentionPolicy.SOURCE)
    public @interface AuthMethod {
    }
}
