package com.affinislabs.minbar.domain.auth.di;

import android.content.SharedPreferences;

import com.affinislabs.minbar.data.MinbarDataRepository;
import com.affinislabs.minbar.data.remote.MinbarApiService;
import com.affinislabs.minbar.di.SharedPref;
import com.affinislabs.minbar.domain.auth.AuthManager;

import dagger.Module;
import dagger.Provides;

/**
 * minbar-v2
 * Michael Obi
 * 11/03/2018, 3:40 PM
 */

@Module
public class AuthManagerModule {

    @Provides
    AuthManager provideAuthManager(MinbarApiService apiService,
                                   MinbarDataRepository minbarDataRepository) {
        return new AuthManager(apiService, minbarDataRepository);
    }
}