package com.linkedin.platform.errors;

import java.util.HashMap;
import java.util.Map;

public enum LiAppErrorCode {
    NONE("none"),
    INVALID_REQUEST("Invalid request"),
    NETWORK_UNAVAILABLE("Unavailable network connection"),
    USER_CANCELLED("User canceled action"),
    UNKNOWN_ERROR("Unknown or not defined error"),
    SERVER_ERROR("Server side error"),
    LINKEDIN_APP_NOT_FOUND("LinkedIn application not found"),
    NOT_AUTHENTICATED("User is not authenticated in LinkedIn app"),
    ;

    private static Map<String, LiAppErrorCode> liAuthErrorCodeHashMap = buildMap();

    private static Map<String, LiAppErrorCode> buildMap() {
        HashMap<String, LiAppErrorCode> map = new HashMap<String, LiAppErrorCode>();
        for (LiAppErrorCode code : LiAppErrorCode.values()) {
            map.put(code.name(), code);
        }
        return map;
    }

    private String description;

    LiAppErrorCode(String name) {
        this.description = name;
    }

    public String getDescription() {
        return description;
    }

    public static LiAppErrorCode findErrorCode(String errorCode) {
        LiAppErrorCode liAuthErrorCode = liAuthErrorCodeHashMap.get(errorCode);
        return liAuthErrorCode == null ? UNKNOWN_ERROR : liAuthErrorCode;
    }
}
