package com.linkedin.platform.errors;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class LiAuthError {

    private static final String TAG = LiAuthError.class.getName();

    private LiAppErrorCode errorCode;
    private String errorMsg;

    public LiAuthError(String errorInfo, String errorMsg) {
        LiAppErrorCode liAuthErrorCode = LiAppErrorCode.findErrorCode(errorInfo);
        errorCode = liAuthErrorCode;
        this.errorMsg = errorMsg;
    }

    public LiAuthError(LiAppErrorCode errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    @Override
    public String toString() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("errorCode", errorCode.name());
            jsonObject.put("errorMessage", errorMsg);
            return jsonObject.toString(2);
        } catch (JSONException e) {
            Log.d(TAG, e.getMessage());
        }
        return null;
    }


}
